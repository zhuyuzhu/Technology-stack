"use strict";
/**
 * 浅copy,没有Boolean值；深copy，第一个值为true；
 * 传一个对象，给this对象(jQuery或jQuery.fn)上添加一个对象(深、浅)
 * 传多个对象，是将后面的对象赋值到第一个对象上(深、浅)
 */
jQuery.extend = jQuery.fn.extend = function(){
  var target = arguments[0];
  var length = arguments.length;
  var i = 1;
  var deep, options, src;
  //(1)先深浅copy的判断处理
  if(typeof target === "boolean" && target){
    deep = target;
    target = arguments[i] || {};
    i++;
  }
  //(2)一个对象的处理
  if(length === i && typeof target === "object"){
    target = this;
    i--; //第三步的for循环要执行一次
  }
  //(3)开始处理
  for(;i < length; i ++){
    if((options = arguments[i]) !== null){
      for(prop in options){
        copy = options[prop];
        if(deep && copy && typeof copy === "object"){//copy是对象或数组
          src = target[prop];//目标上的该属性
          if(1){}
        }else if(copy !== undefined){
          target[prop] = copy;
        }
      }
    }
  }
  //(4)返回target
  return target;
}