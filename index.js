var gitData = {
  id: "git",
  img: "./src/images/git/git.jpg",
  title: "Git",
  describe: "关于Git的学习知识"
}
var htmlData = {
  id: "git",
  img: "./src/images/git/git.jpg",
  title: "Git",
  describe: "关于Git的学习知识"
}
var detailData = [
  {
    name: "git",
    data: {
      id: "git",
      img: "./src/images/git/git.jpg",
      title: "Git",
      describe: "关于Git的学习知识"
    }
  },
  {
    name: "html",
    data: {
      id: "html",
      img: "./src/images/html/html.jpg",
      title: "HTML",
      describe: "关于HTML的学习知识"
    }
  },
  {
    name: "css",
    data: {
      id: "css",
      img: "./src/images/css/css.jpg",
      title: "css",
      describe: "关于css的学习知识"
    }
  },
  {
    name: "gulp",
    data: {
      id: "gulp",
      img: "./src/images/gulp/gulp.jpg",
      title: "gulp",
      describe: "关于gulp的学习知识"
    }
  },
  {
    name: "java",
    data: {
      id: "java",
      img: "./src/images/java/java.jpg",
      title: "java",
      describe: "关于java的学习知识"
    }
  },
  {
    name: "JavaScript",
    data: {
      id: "JavaScript",
      img: "./src/images/JavaScript/JavaScript.jpg",
      title: "JavaScript",
      describe: "关于JavaScript的学习知识"
    }
  },
  {
    name: "jquery",
    data: {
      id: "jquery",
      img: "./src/images/jquery/jquery.jpg",
      title: "jquery",
      describe: "关于jquery的学习知识"
    }
  },
  {
    name: "less",
    data: {
      id: "less",
      img: "./src/images/less/less.jpg",
      title: "less",
      describe: "关于less的学习知识"
    }
  },
  {
    name: "network",
    data: {
      id: "network",
      img: "./src/images/network/network.jpg",
      title: "network",
      describe: "关于network的学习知识"
    }
  },
  {
    name: "Nodejs",
    data: {
      id: "Nodejs",
      img: "./src/images/Nodejs/Nodejs.jpg",
      title: "Nodejs",
      describe: "关于Nodejs的学习知识"
    }
  },
  {
    name: "React",
    data: {
      id: "React",
      img: "./src/images/React/React.jpg",
      title: "React",
      describe: "关于React的学习知识"
    }
  },
  {
    name: "Vue",
    data: {
      id: "Vue",
      img: "./src/images/Vue/Vue.jpg",
      title: "Vue",
      describe: "关于Vue的学习知识"
    }
  },
  {
    name: "webpack",
    data: {
      id: "webpack",
      img: "./src/images/webpack/webpack.jpg",
      title: "webpack",
      describe: "关于webpack的学习知识"
    }
  },
  {
    name: "es6",
    data: {
      id: "es6",
      img: "./src/images/es6/es6.jpg",
      title: "ES6",
      describe: "关于es6的学习知识"
    }
  }

]
for(var i = 0; i < detailData.length; i ++){
  $(".detail-"+ detailData[i].name).tempaleInterface(detailData[i].data);
}
$(".header").templateHeader([
  {
    flag: "home",
    text: "首页",
    linkSrc: ""
  },
  {
    flag: "github",
    text: "Github",
    linkSrc: "https://github.com/zhuyuzhu"
  },{
    flag: "csdn",
    text: "CSDN",
    linkSrc: "https://blog.csdn.net/zyz00000000"
  },{
    flag: "sifou",
    text: "思否",
    linkSrc: "https://segmentfault.com/"
  },{
    flag: "jianshu",
    text: "简书",
    linkSrc: "https://www.jianshu.com/"
  },
  {
    flag: "juejin",
    text: "掘金",
    linkSrc: "https://juejin.im/"
  },
  {
    flag: "infoq",
    text: "极客帮",
    linkSrc: "https://www.infoq.cn/"
  },
  {
    flag: "about",
    text: "关于",
    linkSrc: ""
  }
])
