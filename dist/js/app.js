/**
 * 命名空间
 */
$.extend({
  namespace: function(str){
    var arr = str.split('.');
    var obj = window;
    for(var i = 0; i < arr.length; i++){
      obj[arr[i]] = obj[arr[i]] || {};
      obj = obj[arr[i]];
    }
  }
})