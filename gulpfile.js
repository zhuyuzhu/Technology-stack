var gulp = require('gulp');
var gulpLess = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var babel =require("gulp-babel");
gulp.task('less', function(done){
  gulp.src('./src/less/**/*.less')
      .pipe(gulpLess())
      .pipe(gulp.dest('./dist/css'))
      done()
})
gulp.task('uglify', function(done){
    gulp.src('./src/js/**/*.js')
        .pipe(babel())
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
    done()
})
gulp.task('build', gulp.parallel('less', 'uglify'));