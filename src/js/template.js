/**
 * Usage：标签调用该组件，传入数据，构建一个界面
 * 默认数据
 * 
 */
;(function(){
  /**
   * 创建每个item的入口的HTML
   * @param {*} data 
   */
  var createTemplate = function(data){
    var templateHtml = '<div id="wrapper_'+data.id+'">'+
    '<div class="conent-img"><img src="'+ data.img+'"></img></div>'+
    '<div class="content-title">'+data.title +'</div>'+
    '<div calss="content-describe">'+data.describe +'</div>'
    '</div>';
    return templateHtml;
  }
  /**
   * 创建header的HTML
   * @param {*} data 
   */
  var createHeader = function(data){
    var headerHtml = '';
    var liHtml = '';
    if(Object.prototype.toString.call(data) === "[object Array]"){
      for(var i = 0; i < data.length; i++){
        liHtml += '<li tab='+data[i].flag +'>' + data[i].text +'</li>';
      }
    }
    headerHtml = '<ul class="wrapper">'+ liHtml + '</ul>';
    return headerHtml;
  }
  $.fn.extend({
    /**
     * item的入口方法
     * @param {*} options 
     */
    tempaleInterface : function(options){
      var localhost = "http://localhost:8080";
      var defualts = {
        id: "error",
        img: "",
        title: "标题名称",
        describe: "描述"
      }
      var data = null;
      var templateHtml = "";
      if(typeof options == "object"){
        data = $.extend({},defualts,options);
        templateHtml = createTemplate(data);
        $(this).append(templateHtml).data("params",data);
      }
      $(this).on('click', function(){
        var params = $(this).data("params");
        location.href = localhost+'/dist/detail/'+params.id+'.html';
      })
    },
    /**
     * header的入口函数
     * @param {*} options 
     */
    templateHeader: function(options) {
      if(Object.prototype.toString.call(options) != "[object Array]"){
        return;
      }
      var headerHtml = createHeader(options);
      $(this).append(headerHtml);
      $(this).find("li").on("click", options, function(){
        //this 是li
        var index = $(this).index();
        var flag = $(this).attr("tab");
        
        var localhost = "http://localhost:8080";
        if(index == 0){//跳转到home页
          location.href = localhost +"/index.html"
        }else if(flag == "about"){
          location.href = localhost + "/about.html"
        }
        else if(index > 0){//跳转到其他页面
          window.open(options[index].linkSrc);
        }
      })
    }
  })

}())