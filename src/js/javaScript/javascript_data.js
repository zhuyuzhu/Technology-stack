$.namespace("Zhu.javascriptData");
Zhu.javascriptData = {
    data: {
        //基础知识
        js_basic: {
            "article_id": "106322980",
            "title": "JavaScript基本概念",
            "description": "1、区分大小写\n\n2、标识符：\n\n变量、函数、属性名、函数参数等，字母、$、下划线开头，可以包含数字。不能把关键词、保留字作为标识符。\n\n3、分号：\n\n代码行结尾处没有分号，可能会导致压缩错误；加上分号某些情况下会增进代码性能，因为这样解析器就不会再花时间推测在哪里插入分号。\n\n4、变量\n\n变量是松散类型的，每个变量仅仅是一个保存值的占位符。可以修改变量值的同时，修改值的类型。\n\n在函数中，用var申明一个变量，在函数执行完成，该变量就会销毁。即局部变量。\n\n省略了var 操作符，变量会默认为全局变量。\n\n",
            "content": "<p>1、区分大小写</p>\n\n<p><strong>2、标识符：</strong></p>\n\n<p>变量、函数、属性名、函数参数等，字母、$、下划线开头，可以包含数字。不能把关键词、保留字作为标识符。</p>\n\n<p><strong>3、分号：</strong></p>\n\n<p>代码行结尾处没有分号，可能会导致压缩错误；加上分号某些情况下会增进代码<strong>性能，</strong>因为这样解析器就不会再花时间推测在哪里插入分号。</p>\n\n<p><strong>4、变量</strong></p>\n\n<p>变量是松散类型的，每个变量仅仅是一个保存值的占位符。可以修改变量值的同时，修改值的类型。</p>\n\n<p>在函数中，用var申明一个变量，在函数执行完成，该变量就会销毁。即局部变量。</p>\n\n<p>省略了var 操作符，变量会默认为全局变量。</p>\n\n<p><strong>5、if语句</strong></p>\n\n<pre>\n<code class=\"language-javascript\">//推荐使用\nif(test){\n\n    console.log(test);\n}\n//有效，但不推荐使用\nif(test) console.log(test);</code></pre>\n\n<p><strong>6、do while语句</strong></p>\n\n<p>后测循环语句，先执行后判断。do-while语句大多用于代码至少执行一次的情形。</p>\n\n<pre>\n<code class=\"language-javascript\">do {\n    console.log(i);\n)while(i &lt; 10)</code></pre>\n\n<p><strong>7、while语句</strong></p>\n\n<p>前测语句，先判断，再执行。</p>\n\n<pre>\n<code class=\"language-javascript\">while(i &lt; 10){\n    console.log(i);\n}</code></pre>\n\n<p><strong>8、for in语句</strong></p>\n\n<p>用来遍历对象或者数组，</p>\n\n<p>js对象的属性没有顺序，因此for in循环输出的属性名是不可预测的，先后顺序因浏览器而异。</p>\n\n<p>也会遍历原型上的属性。<strong>obj.hasOwnProperty(prop)来判断prop属性是不是obj对象的</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {\n            a: 1,\n            b: 2,\n            c: 3\n        }\n        Object.prototype.d = 4;\n        console.log(obj)\n        for (prop in obj){\n            if(obj.hasOwnProperty(prop)){//判断prop属性是不是obj对象的\n                console.log(obj[prop])\n            }\n        }</code></pre>\n\n<p><strong>9.break和continue</strong></p>\n\n<p>break：退出循环；continue：退出本次循环。</p>\n\n<p><strong>10、swith case default</strong></p>\n\n<p>case：<strong>如果case的表达式结果 === swith的表达式的执行结果，执行后面的语句</strong></p>\n\n<p>用到break;来终止往下判断。</p>\n\n<p>default:在上面的判断都不成立的时候，执行</p>\n\n<p>swith内的传值要与case对应的一项，才会执行对应case的代码。但是如果swith传true，那么每个case会按顺序判断，直到找到匹配的值，或者遇到default为止。因为每个case都会返回一个Boolean值，与swith中的判断条件做 === 对比。</p>\n\n<pre>\n<code class=\"language-javascript\">        var i = 4;\n        switch(true){\n            case i &lt; 10: \n                console.log(1);\n                break;\n            case i &gt; 10:\n                console.log(2)\n                break;\n            default: \n                console.log(\"default\")\n        }</code></pre>\n\n<p><strong>11、alert方法</strong></p>\n\n<p>当代码执行到alert方法时，js处于断点状态！</p>\n\n<p><strong>12、进制表示法</strong></p>\n\n<p>二进制：0b0 或0b1</p>\n\n<p>八进制：0o+数字</p>\n\n<p>十六进制：0x+数字</p>\n\n<pre>\n<code class=\"language-javascript\">        var n1 = 0b1; // 二进制\n        var n2 = 0o12; //八进制\n        var n3 = 0x14; //十六进制\n        console.log(n1, n2, n3); //1 10 20</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "js",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1
        },
        js_keyCode: {
            "article_id": "88551188",
            "title": "JavaScript中keyCode对应的编码值",
            "description": "keyCode对应的编码值列表：\n\n\nkeycode 0 =\n\nkeycode 1 =\nkeycode 2 =\nkeycode 3 =\nkeycode 4 =\nkeycode 5 =\nkeycode 6 =\nkeycode 7 =\nkeycode 8 = BackSpace\nkeycode 9 = Tab\nkeycode 10 =\nkeycode 11 =\nkeycode 12 = Clear\n...",
            "content": "<p>&nbsp;keyCode对应的编码值列表：</p>\n\n<pre class=\"has\">\n<code>keycode 0 =\n\nkeycode 1 =\nkeycode 2 =\nkeycode 3 =\nkeycode 4 =\nkeycode 5 =\nkeycode 6 =\nkeycode 7 =\nkeycode 8 = BackSpace\nkeycode 9 = Tab\nkeycode 10 =\nkeycode 11 =\nkeycode 12 = Clear\nkeycode 13 = Enter\nkeycode 14 =\nkeycode 15 =\nkeycode 16 = Shift_L\nkeycode 17 = Control_L\nkeycode 18 = Alt_L\nkeycode 19 = Pause\nkeycode 20 = Caps_Lock\nkeycode 21 =\nkeycode 22 =\nkeycode 23 =\nkeycode 24 =\nkeycode 25 =\nkeycode 26 =\nkeycode 27 = Esc Escape\nkeycode 28 =\nkeycode 29 =\nkeycode 30 =\nkeycode 31 =\nkeycode 32 = Space\nkeycode 33 = Page Up\nkeycode 34 = Page Down\nkeycode 35 = End\nkeycode 36 = Home\nkeycode 37 = Left Arrow\nkeycode 38 = Up Arrow\nkeycode 39 = Right Arrow\nkeycode 40 = Down Arrow\nkeycode 41 = Select\nkeycode 42 = Print\nkeycode 43 = Execute\nkeycode 44 =\nkeycode 45 = Insert\nkeycode 46 = Delete\nkeycode 47 = Help\nkeycode 48 = 0 )\nkeycode 49 = 1 !\nkeycode 50 = 2 @\nkeycode 51 = 3 #\nkeycode 52 = 4 $\nkeycode 53 = 5 %\nkeycode 54 = 6 ^\nkeycode 55 = 7 &amp;\nkeycode 56 = 8 *\nkeycode 57 = 9 (\nkeycode 58 =\nkeycode 59 =\nkeycode 60 =\nkeycode 61 =\nkeycode 62 =\nkeycode 63 =\nkeycode 64 =\nkeycode 65 = a A\nkeycode 66 = b B\nkeycode 67 = c C\nkeycode 68 = d D\nkeycode 69 = e E\nkeycode 70 = f F\nkeycode 71 = g G\nkeycode 72 = h H\nkeycode 73 = i I\nkeycode 74 = j J\nkeycode 75 = k K\nkeycode 76 = l L\nkeycode 77 = m M\nkeycode 78 = n N\nkeycode 79 = o O\nkeycode 80 = p P\nkeycode 81 = q Q\nkeycode 82 = r R\nkeycode 83 = s S\nkeycode 84 = t T\nkeycode 85 = u U\nkeycode 86 = v V\nkeycode 87 = w W\nkeycode 88 = x X\nkeycode 89 = y Y\nkeycode 90 = z Z\nkeycode 91 =\nkeycode 92 =\nkeycode 93 =\nkeycode 94 =\nkeycode 95 =\nkeycode 96 = KP_0\nkeycode 97 = KP_1\nkeycode 98 = KP_2\nkeycode 99 = KP_3\nkeycode 100 = KP_4\nkeycode 101 = KP_5\nkeycode 102 = KP_6\nkeycode 103 = KP_7\nkeycode 104 = KP_8\nkeycode 105 = KP_9\nkeycode 106 = KP_* KP_Multiply\nkeycode 107 = KP_+ KP_Add\nkeycode 108 = KP_Enter KP_Separator\nkeycode 109 = KP_- KP_Subtract\nkeycode 110 = KP_. KP_Decimal\nkeycode 111 = KP_/ KP_Divide\nkeycode 112 = F1\nkeycode 113 = F2\nkeycode 114 = F3\nkeycode 115 = F4\nkeycode 116 = F5\nkeycode 117 = F6\nkeycode 118 = F7\nkeycode 119 = F8\nkeycode 120 = F9\nkeycode 121 = F10\nkeycode 122 = F11\nkeycode 123 = F12\nkeycode 124 = F13\nkeycode 125 = F14\nkeycode 126 = F15\nkeycode 127 = F16\nkeycode 128 = F17\nkeycode 129 = F18\nkeycode 130 = F19\nkeycode 131 = F20\nkeycode 132 = F21\nkeycode 133 = F22\nkeycode 134 = F23\nkeycode 135 = F24\nkeycode 136 = Num_Lock\nkeycode 137 = Scroll_Lock\nkeycode 138 =\nkeycode 139 =\nkeycode 140 =\nkeycode 141 =\nkeycode 142 =\nkeycode 143 =\nkeycode 144 =\nkeycode 145 =\nkeycode 146 =\nkeycode 147 =\nkeycode 148 =\nkeycode 149 =\nkeycode 150 =\nkeycode 151 =\nkeycode 152 =\nkeycode 153 =\nkeycode 154 =\nkeycode 155 =\nkeycode 156 =\nkeycode 157 =\nkeycode 158 =\nkeycode 159 =\nkeycode 160 =\nkeycode 161 =\nkeycode 162 =\nkeycode 163 =\nkeycode 164 =\nkeycode 165 =\nkeycode 166 =\nkeycode 167 =\nkeycode 168 =\nkeycode 169 =\nkeycode 170 =\nkeycode 171 =\nkeycode 172 =\nkeycode 173 =\nkeycode 174 =\nkeycode 175 =\nkeycode 176 =\nkeycode 177 =\nkeycode 178 =\nkeycode 179 =\nkeycode 180 =\nkeycode 181 =\nkeycode 182 =\nkeycode 183 =\nkeycode 184 =\nkeycode 185 =\nkeycode 186 =\nkeycode 187 = =+\nkeycode 188 = ,&lt;\nkeycode 189 = -_\nkeycode 190 = .&gt;\nkeycode 191 = /?\nkeycode 192 = `~\nkeycode 193 =\nkeycode 194 =\nkeycode 195 =\nkeycode 196 =\nkeycode 197 =\nkeycode 198 =\nkeycode 199 =\nkeycode 200 =\nkeycode 201 =\nkeycode 202 =\nkeycode 203 =\nkeycode 204 =\nkeycode 205 =\nkeycode 206 =\nkeycode 207 =\nkeycode 208 =\nkeycode 209 =\nkeycode 210 = plusminus hyphen macron\nkeycode 211 =\nkeycode 212 = copyright registered\nkeycode 213 = guillemotleft guillemotright\nkeycode 214 = masculine ordfeminine\nkeycode 215 = ae AE\nkeycode 216 = cent yen\nkeycode 217 = questiondown exclamdown\nkeycode 218 = onequarter onehalf threequarters\nkeycode 219 = [{\nkeycode 220 = \\|\nkeycode 221 = ]}\nkeycode 222 = '\"\n\nkeycode 223 =\nkeycode 224 =\nkeycode 225 =\nkeycode 226 =\nkeycode 227 = multiply division\nkeycode 228 = acircumflex Acircumflex\nkeycode 229 = ecircumflex Ecircumflex\nkeycode 230 = icircumflex Icircumflex\nkeycode 231 = ocircumflex Ocircumflex\nkeycode 232 = ucircumflex Ucircumflex\nkeycode 233 = ntilde Ntilde\nkeycode 234 = yacute Yacute\nkeycode 235 = oslash Ooblique\nkeycode 236 = aring Aring\nkeycode 237 = ccedilla Ccedilla\nkeycode 238 = thorn THORN\nkeycode 239 = eth ETH\nkeycode 240 = diaeresis cedilla currency\nkeycode 241 = agrave Agrave atilde Atilde\nkeycode 242 = egrave Egrave\nkeycode 243 = igrave Igrave\nkeycode 244 = ograve Ograve otilde Otilde\nkeycode 245 = ugrave Ugrave\nkeycode 246 = adiaeresis Adiaeresis\nkeycode 247 = ediaeresis Ediaeresis\nkeycode 248 = idiaeresis Idiaeresis\nkeycode 249 = odiaeresis Odiaeresis\nkeycode 250 = udiaeresis Udiaeresis\nkeycode 251 = ssharp question backslash\nkeycode 252 = asciicircum degree\nkeycode 253 = 3 sterling\nkeycode 254 = Mode_switch\n\n</code></pre>\n\n<h3>组合键</h3>\n\n<pre>\n<code class=\"language-html\">if ((window.event.ctrlKey)&amp;&amp;(window.event.keyCode==49))\n\nalert(\"您按下了CTRL＋1键\");</code></pre>\n\n<p>从以上例子可以看出，shift、Ctrl、Alt 在JS中也可用&ldquo;window.event.shiftKey&rdquo;、&ldquo;window.event.ctrlKey&rdquo;、&ldquo;window.event.altKey&rdquo; 代替。</p>\n\n<p>Internet Explorer/Chrome 浏览器使用 event.keyCode 获取被按下的字符，而 Netscape/Firefox/Opera 等浏览器使用 event.which。</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "JavaScript中keyCode对应的编码值",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/88551188"
        },
        js_toString: `<p>判断数据类型的方式：</p>

<p><strong>1.&nbsp;Object.prototype.toString</strong></p>

<p>对象原型上的toString方法，在对象的this属性上有可以判断数据类型的隐藏属性[[class]]，其他的数据类型都有自己的toString方法，但是<strong>他们的toString方法被重写了</strong>跟Object.prototype.toString不一样。</p>

<p>比如：</p>

<p>（1）定义一个变量a是一个数字：var&nbsp;a&nbsp;=&nbsp;1;</p>

<p>（2）a的类型：typeof&nbsp;a&nbsp;==&nbsp;&quot;number&quot;;</p>

<p>（3）a调用toString方法：a.toString()&nbsp;==&nbsp;&quot;1&quot;;</p>

<p>（4）为什么a是一个数字也可以调用toString方法呢？因为：a.constructor&nbsp;==&nbsp;&nbsp;&fnof;&nbsp;Number()&nbsp;{&nbsp;[native&nbsp;code]&nbsp;}; a 是由构造函数Number()new出来的；既然是构造函数构造出来，其实也可以看做是一个特殊的对象</p>

<p>（5）a的原型上是有toString方法，不过这个方法是重写的，跟Object.prototype.toString的方法不一样了。同样的，数组、对象、布尔值等都重写了toString方法</p>

<p><img alt="" class="has" height="193" src="https://img-blog.csdnimg.cn/20191206114847469.png" width="620" /></p>

<p><strong>原型上包含了所有的方法，可以通过__proto__来查看这些方法，从而使用这些方法。</strong></p>

<p><strong>数组原型上</strong>也是：</p>

<p><img alt="" class="has" height="657" src="https://img-blog.csdnimg.cn/20191206143610364.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70" width="641" /></p>

<p><strong>注意</strong>：length: 0是原型的length属性的值，arr有自己的length属性，有自己的值。&nbsp;</p>

<p>&nbsp;（6）分别来看一下，不同类型的数据调用toString方法后的结果：</p>

<p>对象：</p>

<p><img alt="" class="has" height="134" src="https://img-blog.csdnimg.cn/20191206130602333.png" width="233" /></p>

<p>数组：</p>

<p><img alt="" class="has" height="121" src="https://img-blog.csdnimg.cn/20191206130642629.png" width="159" /></p>

<p>布尔值：</p>

<p><img alt="" class="has" height="119" src="https://img-blog.csdnimg.cn/20191206130719513.png" width="189" /></p>

<p>（7）除了对象调用toString方法得到的值判断数据类型，其他的都是转换为字符串，当然，每个类型的toString实现的方式也是不同，比如：数组就是每一项都会调用toString方法，而得到的字符串。</p>

<p>（8）根据上面的变量a，可以调用toString方法，因为构造函数内的<strong>this对象有一个&nbsp;[[Class]] 隐藏的属性（internal property）</strong>，该属性的值就是类型：</p>

<table summary="Internal Properties">
	<thead>
		<tr>
			<th>内部属性</th>
			<th>描述</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><a id="_Class_" name="_Class_"></a>[[Class]]</td>
			<td>一个字符串值,表明了该对象的类型.</td>
		</tr>
	</tbody>
</table>

<p>只有最原始的对象的原型上的toString方法具有[[Class]]属性，而该属性正是来区分数据类型的，其他toString方法都被重写了，失去了判断类型的功能；正因为如此，我们通过call来改变this指向，进而使得Object.prototype.toString.call()可以避开自己的toString方法，而去调用原型上的toString方法，才能确定数据类型。</p>

<p>（9）在toString方法被调用时,会执行下面的操作步骤:</p>

<p>&nbsp;获取this对象的[[Class]]属性的值.</p>

<p>计算出三个字符串&quot;[object &quot;, [[Class]]属性的值, 以及 &quot;]&quot;连接后的新字符串.</p>

<p>&nbsp;返回计算的的结果</p>

<p>（10）<strong>在ES3中</strong>,规范文档并没有总结出[[class]]内部属性一共有几种,不过我们可以自己统计一下,原生对象的[[class]]内部属性的值一共有10种.分别是:<strong>&quot;Array&quot;, &quot;Boolean&quot;, &quot;Date&quot;, &quot;Error&quot;, &quot;Function&quot;, &quot;Math&quot;, &quot;Number&quot;, &quot;Object&quot;, &quot;RegExp&quot;, &quot;String&quot;.</strong></p>

<p>（11）<strong>ECMAScript 5，</strong>对象的[[Class]]属性的值可以是除了&quot;<strong>Arguments</strong>&quot;, &quot;Array&quot;, &quot;Boolean&quot;, &quot;Date&quot;, &quot;Error&quot;, &quot;Function&quot;, &quot;JSON&quot;, &quot;Math&quot;, &quot;Number&quot;, &quot;Object&quot;, &quot;RegExp&quot;, &quot;String&quot;之外的的<strong>任何字符串.</strong>&nbsp;</p>

<p><strong>ES5的Object.prototype.toString ( )</strong></p>

<p>在toString方法被调用时,会执行下面的操作步骤:</p>

<p>如果this的值为undefined,则返回&quot;[object Undefined]&quot;.</p>

<p>如果this的值为null,则返回&quot;[object Null]&quot;.</p>

<p>让O成为调用ToObject(this)的结果.</p>

<p>让class成为O的内部属性[[Class]]的值.</p>

<p>返回三个字符串&quot;[object &quot;, class, 以及 &quot;]&quot;连接后的新字符串.</p>

<p>（12）<strong>Arguments类型</strong></p>

<pre class="has">
<code class="language-javascript">function func(a,b,c){
            console.log(arguments);
            console.log(Object.prototype.toString.call(arguments));
        }
        func(1,2,3);</code></pre>

<p>结果：Arguments(3)&nbsp;[1, 2, 3, callee: &fnof;, Symbol(Symbol.iterator): &fnof;]<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;[object Arguments]&nbsp;</p>

<p>&nbsp;</p>

<p>2.&nbsp;如果能明白toString方法的原理，还可以这么写：</p>

<pre>
<code class="language-javascript">var obj = {};
obj.toString.call([])
//结果
"[object Array]"</code></pre>

<p>参考：</p>

<p><a href="https://www.cnblogs.com/libin-1/p/5902070.html">JavaScript中Object.prototype.toString方法的原理</a></p>

<p><a href="https://api.jquery.com/jQuery.type/">jQuery.type()</a></p>`
        ,
        js_typeOf: `"<p>typeOf查看字符串类型</p>

        <pre>
        <code class="language-javascript">
        typeof undefined
        "undefined"
        
        typeof null
        "object"
        
        typeof {}
        "object"
        
        typeof []
        "object"
        
        typeof function(){}
        "function"
        
        typeof "aa"
        "string"
        
        typeof 11
        "number"
        
        typeof true
        "boolean</code></pre>
        
        <p>需要注意的：</p>
        
        <pre>
        <code class="language-javascript">
        typeof Infinity === 'number';
        typeof NaN === 'number';
        typeof (typeof 1) === 'string';
        typeof [1, 2, 4] === 'object';
        typeof Math.sin === 'function';
        typeof Number(1) === 'number'; 
        
        typeof Symbol() === 'symbol';
        typeof Symbol('foo') === 'symbol';
        typeof Symbol.iterator === 'symbol';
        
        typeof new Boolean(true) === 'object';
        typeof new Number(1) === 'object';
        typeof new String("abc") === 'object';
        
        typeof new Function() === 'function';</code></pre>
        
        <p>&nbsp;</p>
        "`
        ,
        js_instanceof: `"<p><strong><code>instanceof</code></strong>&nbsp;<strong>运算符</strong>用于检测<strong>构造函数</strong>的&nbsp;<code>prototype</code>&nbsp;属性（构造函数原型）是否出现在<strong>某个实例对象</strong>的原型链上。</p>

        <p><code>instanceof</code>&nbsp;运算符用来检测&nbsp;<code>constructor.prototype&nbsp;</code>是否存在于参数&nbsp;<code>object</code>&nbsp;的原型链上。</p>
        
        <pre>
        <code>// 定义构造函数
        function C(){} 
        function D(){} 
        
        var o = new C();
        
        
        o instanceof C; // true，因为 Object.getPrototypeOf(o) === C.prototype
        
        
        o instanceof D; // false，因为 D.prototype 不在 o 的原型链上
        
        o instanceof Object; // true，因为 Object.prototype.isPrototypeOf(o) 返回 true
        C.prototype instanceof Object // true，同上
        
        C.prototype = {};
        var o2 = new C();
        
        o2 instanceof C; // true
        
        o instanceof C; // false，C.prototype 指向了一个空对象,这个空对象不在 o 的原型链上.
        
        D.prototype = new C(); // 继承
        var o3 = new D();
        o3 instanceof D; // true
        o3 instanceof C; // true 因为 C.prototype 现在在 o3 的原型链上
        </code></pre>
        
        <p>需要注意的是，如果表达式&nbsp;<code>obj instanceof Foo</code>&nbsp;返回&nbsp;<code>true</code>，则并不意味着该表达式会永远返回&nbsp;<code>true</code>，因为&nbsp;<code>Foo.prototype</code>&nbsp;属性的值有可能会改变，改变之后的值很有可能不存在于&nbsp;<code>obj</code>&nbsp;的原型链上，这时原表达式的值就会成为&nbsp;<code>false</code>。另外一种情况下，原表达式的值也会改变，就是改变对象&nbsp;<code>obj</code>&nbsp;的原型链的情况，虽然在目前的ES规范中，我们只能读取对象的原型而不能改变它，但借助于非标准的&nbsp;<code>__proto__</code>&nbsp;伪属性，是可以实现的。比如执行&nbsp;<code>obj.__proto__ = {}</code>&nbsp;之后，<code>obj instanceof Foo</code>&nbsp;就会返回&nbsp;<code>false</code>&nbsp;了。</p>
        
        <p>包装类对象：</p>
        
        <pre>
        <code class="language-javascript">
                var str = new String();
                console.log(str instanceof String &amp;&amp; str instanceof Object); true
                var bool = new Boolean();
                console.log(bool instanceof Boolean &amp;&amp; bool instanceof Object); true
                var num = new Number();
                console.log(num instanceof Number &amp;&amp; num instanceof Object); true
         </code></pre>
        
        <p>函数和数组：</p>
        
        <pre>
        <code class="language-javascript">
                function func(){};
                console.log(func instanceof Function &amp;&amp; func instanceof Object);
                var arr = [];
                console.log(arr instanceof Array &amp;&amp; arr instanceof Object);</code></pre>
        
        <p>&nbsp;</p>
        "`
        ,
        js_dataTypeTransform: `"<p><strong>一、运算符</strong></p>

        <p><strong>1. 隐式的转换为字符串</strong></p>
        
        <pre>
        <code class="language-javascript">
        var variate = ["a","b","c"];
        转换为字符串
        
        variate + ""</code></pre>
        
        <p><strong>2. 隐式的转换为布尔类型</strong></p>
        
        <pre>
        <code class="language-javascript">
        var variate = ["a","b","c"];
        转换为Boolean类型
        
        !!variate</code></pre>
        
        <p><strong>3. 隐式的转换为数字</strong></p>
        
        <p><strong>（1）一元+</strong></p>
        
        <p>在对非数值应用一元加运算符时，会调用Number()转型函数对这个值进行转换</p>
        
        <pre>
        <code class="language-javascript">
        var variate = "123";
        转为数字
        
        +variate</code></pre>
        
        <p>在new Date()前面使用一元加符号，可以把日期字符串，转换为日期毫秒数&nbsp;</p>
        
        <pre>
        <code class="language-javascript">
        new Date()
        Thu May 28 2020 15:07:16 GMT+0800 (中国标准时间)
        +new Date()
        1590649644859</code></pre>
        
        <p><strong>&nbsp;（2）一元-</strong></p>
        
        <p>会对该值使用Number()转型函数进行转换，再将得到的数值转换成负数&nbsp;</p>
        
        <pre>
        <code class="language-javascript">
        -variate</code></pre>
        
        <p><strong>（3）自增++ 和 自减 --</strong></p>
        
        <p>会调用Number()转型函数对这个值进行加减</p>
        
        <p><strong>4、加法运算</strong></p>
        
        <p><strong>（1）数字进行的加法</strong></p>
        
        <p>特别注意，数字和引用值进行加法运算，其实是拼接</p>
        
        <pre>
        <code class="language-javascript">
        console.log(1 + {});//'1[object Object]'
        console.log(1 + [1,2]);//'11,2'
        console.log(1 + new Date());//'1Thu Jun 16 2016 10:27:13 GMT+0800 (中国标准时间)'
        console.log(1 + /0/);//'1/0/'</code></pre>
        
        <p><strong>（2）字符串进行的拼接</strong></p>
        
        <pre>
        <code class="language-javascript">
        console.log('' + undefined);//'undefined'
        console.log('' + null);//'null'
        console.log('' + false);//'false'
        console.log('' + true);//'true'</code></pre>
        
        <p><strong>5、减法运算</strong></p>
        
        <p>减法运算都会进行数字运行，注意new Data()会转换为毫秒数</p>
        
        <pre>
        <code class="language-javascript">
        console.log(1 - {});//NaN
        console.log(1 - [1,2]);//NaN
        console.log(1 - /0/);//NaN
        console.log(1 - []);//1
        
        console.log(new Date() + 1);//'Thu Jun 16 2016 11:11:49 GMT+0800 (中国标准时间)1'
        console.log(new Date() - 1);//1466046941641
        
        console.log(1 - undefined);//NaN
        console.log(1 - null);//1
        console.log(1 - false);//1
        console.log(1 - true);//0</code></pre>
        
        <p><strong>6、乘除取余（* / %）</strong></p>
        
        <p>都会进过Number()的转换，转换为数字后进行运行。</p>
        
        <p>需要注意的是：%的符号有左边的数字决定</p>
        
        <pre>
        <code class="language-javascript">
        console.log(5 % 2);//1
        console.log(5 % -2);//1
        console.log(-5 % 2);//-1
        console.log(-5 % -2);//-1</code></pre>
        
        <p>二、关系运算符</p>
        
        <p>关系运算符用于测试两个值之间的关系，根据关系是否存在而返回true或false，关系表达式总是返回一个布尔值，通常在if、while或for语句中使用关系表达式，用以控制程序的执行流程。</p>
        
        <p>javascript提供了===、!==、==、!=、&lt;、&lt;=、&gt;、&gt;=8个关系运算符，分为4类介绍关系运算符。</p>
        
        <p>【1】=== 全等</p>
        
        <p>不经过任何的类型转换，进行值的比较。</p>
        
        <p>如果是<strong>数字比较</strong>，都转换为10进制后进行比较</p>
        
        <p>10 === 0xa&nbsp; &nbsp; （true）</p>
        
        <p>&nbsp;如果是<strong>字符串</strong>，比较每一位的值</p>
        
        <p>&quot;abc&quot; === &quot;abc&quot;&nbsp; （true）</p>
        
        <p><strong>undefined的比较</strong>：</p>
        
        <p>undefined === undefined（true）</p>
        
        <p><strong>null值的比较：</strong></p>
        
        <p>null === null（true）</p>
        
        <p><strong>布尔值的比较：</strong></p>
        
        <p>true === true（true）</p>
        
        <p><strong>引用值的比较</strong>，比较的是同一地址：</p>
        
        <pre>
        <code class="language-javascript">
        var obj1 = {a:1,b:2,o:{age:24}}
        undefined
        var obj = obj1
        undefined
        obj === obj1
        true</code></pre>
        
        <pre>
        <code class="language-javascript">
        {a:1} === {a:1}
        false</code></pre>
        
        <p>&nbsp;<strong>注意 NaN</strong>与任何值都不相等，更不全等。</p>
        
        <p>【2】== 相等</p>
        
        <p>相等运算符&#39;==&#39;和恒等运算符相似，但相等运算符的比较并不严格，如果两个操作数不是同一类型，相等运算符会尝试进行一些类型转换，然后再进行比较</p>
        
        <p>当两个操作数类型相同时，比较规则和恒等运算符规则相同</p>
        
        <pre>
        <code class="language-javascript">
        console.log(true == 1);//true
        console.log(true == 0);//false
        console.log(false == '1');//false
        console.log(false == '0');//true
        console.log(true == 'true');//false，相当于1 == NaN
        
        console.log([1] == 1);//true，相当于1 == 1
        console.log([1] == '1');//true，相当于'1' == '1'
        console.log([] == 0);//true，相当于0 == 0
        console.log([] == '0');//false，相当于'' == '0'
        
        console.log([] == true);//false，相当于0 == 1
        console.log([1] == true);//true，相当于1 == 1
        
        console.log(new Date() == 'Sat Jun 25 2016 11:07:20 GMT+0800 (中国标准时间)');//true</code></pre>
        
        <p>[注意]如果一个值是null，另一个值是undefined，则返回true。</p>
        
        <p><strong>虽然Number(null)是0，但null和0并不相等。虽然Boolean(null)是false，但null和false也不相等。</strong></p>
        
        <pre>
        <code class="language-javascript">
        console.log(null == undefined);//true
        console.log(null == 0);//false
        
        null == false
        false</code></pre>
        
        <p>&nbsp;实际上，null == null 或 undefined 外，null和其他都不相等。</p>
        
        <p id="sf-article_title"><strong>参考文章：<a href="https://segmentfault.com/a/1190000015651284">javascript &mdash;&mdash; 运算符</a></strong></p>
        "`
        ,
        js_primitiveObject: `"<p><strong>字符串、布尔值、数字都是可以调用方法的，尤其是字符串，有很多的方法可以调用，那么他们的原理是什么呢？</strong></p>

        <h3><strong>1、字符串的原型上的方法</strong></h3>
        
        <pre>
        <code class="language-javascript">
        var str = new String();  空字符串实例</code></pre>
        
        <p><strong>&nbsp;字符串的方法都不会改变原字符串。</strong>该实例的原型上的方法有：</p>
        
        <p><img alt="" height="848" src="https://img-blog.csdnimg.cn/20200623151626180.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70" width="365" /></p>
        
        <p><strong>（1）charAt方法：返回指定位置的字符</strong></p>
        
        <p>请注意，JavaScript 并没有一种有别于字符串类型的字符数据类型，所以返回的字符是长度为 1 的字符串。</p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcde";
                console.log(str.charAt(0),str); a abcde</code></pre>
        
        <p><strong>&nbsp;charCodeAt() 方法：返回指定位置的字符的 Unicode 编码</strong></p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcde";
                console.log(str.charCodeAt(0),str);  97 "abcde"</code></pre>
        
        <p><strong>（2） 转大小写</strong></p>
        
        <p><strong>toLowerCase() 方法和toLocaleLowerCase() 方法：</strong>用于把字符串转换为小写。所有大写字符全部被转换为小写字符</p>
        
        <p>与 toLowerCase() 不同的是，toLocaleLowerCase() 方法按照本地方式把字符串转换为小写。只有几种语言（如土耳其语）具有地方特有的大小写映射，所有该方法的返回值通常与 toLowerCase() 一样。</p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcDeaBabA";
                console.log(str.toLocaleLowerCase()); abcdeababa</code></pre>
        
        <p><strong>&nbsp;toUpperCase()方法&nbsp;和&nbsp;toLocaleUpperCase()方法，将字符串的每个字符转换为大写。</strong></p>
        
        <p><strong>（3）去空字符</strong></p>
        
        <p>trim()方法：用于删除字符串的头尾空格。</p>
        
        <p><strong>（4）split() 方法：返回一个按参数拆分字符串后的数组</strong></p>
        
        <p><strong>不传值：将字符串作为一个整项，作为数组的第一个项。</strong></p>
        
        <p><strong>传空字符串：把字符串每一项拆开。</strong></p>
        
        <p><strong>传指定字符：按指定字符拆开。</strong></p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcdefabab";
                console.log(str.split());   ["abcdefabab"]
                console.log(str.split("")); ["a", "b", "c", "d", "e", "f", "a", "b", "a", "b"]
                console.log(str.split("a"));["", "bcdef", "b", "b"]</code></pre>
        
        <p><strong>concat() 方法：用于连接两个或多个字符串，返回拼接后的新字符串。</strong></p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcde";
                console.log(str.concat("mn","xyz")); abcdemnxyz</code></pre>
        
        <p><strong>slice(start, end) 方法：</strong>可提取字符串的某个部分，并以新的字符串返回被提取的部分。</p>
        
        <p>使用 start（包含） 和 end（不包含） 参数来指定字符串提取的部分。</p>
        
        <p>字符串中第一个字符位置为 0, 第二个字符位置为 1, 以此类推。</p>
        
        <p><strong>提示：</strong>&nbsp;如果是负数，则该参数规定的是从字符串的尾部开始算起的位置。也就是说，-1 指字符串的最后一个字符，-2 指倒数第二个字符，以此类推。</p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcdefabab";
                console.log(str.slice(0,3));   abc</code></pre>
        
        <p>&nbsp;<strong>substring() 方法：用于提取字符串中介于两个指定下标之间的字符</strong></p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcdeababa";
                console.log(str.substring(0,3)); abc</code></pre>
        
        <p><strong>（5）</strong>&nbsp;<strong>indexOf方法</strong>：字符串原型上也有这个方法，所以字符串也可以用这个方法来查找字符是否存在。同样还有<strong>lastIndexOf方法。</strong></p>
        
        <p><strong>（6）localeCompare() 方法：用本地特定的顺序来比较两个字符串。结合数组sort方法可以排序。</strong></p>
        
        <p>提供的比较字符串的方法，考虑了默认的本地排序规则。ECMAscript 标准并没有规定如何进行本地特定的比较操作，它只规定该函数采用底层操作系统提供的排序规则。</p>
        
        <pre>
        <code class="language-javascript">
                console.log("a".localeCompare("b"));  -1  升序
                console.log("b".localeCompare("a"));  1   降序</code></pre>
        
        <p>给数组中的每一项排序，按本地规则排序。如果是数字，按数字大小排序，如果是字符串，按本地规则的字符串排序。</p>
        
        <pre>
        <code class="language-javascript">
        var arr = ["我","你","他",12,3,5,14,"abc","b","ac"];
        arr.sort((a,b) =&gt; {
        return typeof a === "number" &amp;&amp; typeof b === "number" ? a - b : (a+"").localeCompare(b+"");
        })
        console.log(arr); [3, 5, 12, 14, "你", "他", "我", "abc", "ac", "b"]</code></pre>
        
        <p>&nbsp;<strong>（7）与正则表达式相关的方法</strong></p>
        
        <p><strong>match() 方法：</strong>可在字符串内检索指定的值，或找到一个或多个<strong>正则表达式的匹配</strong>。</p>
        
        <p>该方法类似 indexOf() 和 lastIndexOf()，但是它返回指定的值，而不是字符串的位置。</p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcdeababa";
                var reg = /a/g;
                console.log(str.match(reg))  ["a", "a", "a", "a"]</code></pre>
        
        <p>match() 方法将检索字符串，以找到一个或多个与 regexp 匹配的文本。这个方法的行为在很大程度上有赖于 regexp 是否具有标志 g。如果 regexp 没有标志 g，那么 match() 方法就只能在 stringObject 中执行一次匹配。如果没有找到任何匹配的文本， match() 将返回 null。否则，它将返回一个数组，其中存放了与它找到的匹配文本有关的信息。&nbsp;</p>
        
        <p><strong>匹配不到返回null ：</strong></p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcdefabab";
                var reg = /\d/g
                console.log(str.match(reg)); null</code></pre>
        
        <p>全局匹配和非全局匹配的结果是不同的。</p>
        
        <p>全局匹配：返回所以匹配到的组成的数组。</p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcdefabab";
                var reg = /b/g;
                console.log(str.match(reg)); ["b", "b", "b"]</code></pre>
        
        <p>&nbsp;非全局匹配：返回匹配项信息组成的数组。</p>
        
        <pre>
        <code class="language-javascript">
        var str = "abcdefabab";
        var reg = /b/;
        console.log(str.match(reg));  ["b", index: 1, input: "abcdefabab", groups: undefined]</code></pre>
        
        <p>&nbsp;<strong>replace() 方法</strong>：用于在字符串中用一些字符替换另一些字符，或替换一个与正则表达式匹配的子串。不修改原字符串。</p>
        
        <p>字符串替换字符串：只能匹配到第一个字符，然后将其替换：&nbsp;</p>
        
        <pre>
        <code class="language-javascript">
        var str = "abcdefabab";
        console.log(str.replace("a","1"),str); 1bcdefabab abcdefabab</code></pre>
        
        <p>&nbsp;正则表达式非全局匹配：</p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcdefabab";
                var reg = /a/;
                console.log(str.replace(reg,"1"));  1bcdefabab</code></pre>
        
        <p>正则表达式全局匹配字符：</p>
        
        <pre>
        <code class="language-javascript">
                var str = "abcdefabab";
                var reg = /a/g;
                console.log(str.replace(reg,"1"));  1bcdef1b1b</code></pre>
        
        <p>正则的子表达式：第二个参数中$1 $2 $n将代表对应的子表达式</p>
        
        <pre>
        <code class="language-javascript">
                var str = "a1bcdefa2ba3b";
                var reg = /(a)(\d)/g;
                console.log(str.replace(reg,"$2$1")); 1abcdef2ab3ab</code></pre>
        
        <p>正则与 正则表达式相匹配的文本：$&amp;</p>
        
        <pre>
        <code class="language-javascript">
                var str = "a1bcdefa2ba3b";
                var reg = /(a)(\d)/g;
                console.log(str.replace(reg,"$&amp;$&amp;")); a1a1bcdefa2a2ba3a3b</code></pre>
        
        <p>&nbsp;正则表达式匹配内容左侧的文本：$\`</p>
        
        <p>下例中第一个a的左侧文本替换a，第二个a的左侧文本替换a</p>
        
        <pre>
        <code class="language-javascript">
                var str = "12a34a56";
                var reg = /a/g;
                console.log(str.replace(reg,"$\`")); 12123412a3456</code></pre>
        
        <p>&nbsp;正则表达式匹配子串右侧的内容：$&#39;</p>
        
        <p>第二个参数是函数：</p>
        
        <pre>
        <code class="language-javascript">
                var str = "12a34a56";
                var reg = /(a)(\d)/g;
                console.log(str.replace(reg,function(){
                    console.log(arguments);
                    return "b";
                }));</code></pre>
        
        <p>匹配到几次，函数就执行几次。每次打印的arguments如下：</p>
        
        <p>该处理函数接收的实参包括：</p>
        
        <p>param1：匹配到的文本，</p>
        
        <p>param2子表达式1，param3子表达式2，。。 <strong>子表达式是从第二个参数开始的</strong></p>
        
        <p>倒数第二个：是匹配到的文本的位置，</p>
        
        <p>倒数第一个：字符串本身。</p>
        
        <p><img alt="" height="104" src="https://img-blog.csdnimg.cn/20200624100507301.png" width="314" /></p>
        
        <p><img alt="" height="100" src="https://img-blog.csdnimg.cn/20200624100521242.png" width="315" /></p>
        
        <p><strong>search() 方法：</strong>用于检索字符串中<strong>指定的子字符串</strong>，<strong>或检索与正则表达式相匹配的子字符串</strong>。</p>
        
        <p>返回检索到的第一个符合的位置，如果没有找到任何匹配的子串，则返回 -1。</p>
        
        <pre>
        <code class="language-javascript">
                var str = "12a34a56";
                var reg = /(a)(\d)/g;
                console.log(str.search(reg)); 2</code></pre>
        
        <p><strong>（8）还有一些方法可以修改样式，参考以下图片</strong></p>
        
        <p><img alt="" height="578" src="https://img-blog.csdnimg.cn/20200623164657674.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70" width="1042" /></p>
        
        <h3>2、布尔值的原型上的方法</h3>
        
        <pre>
        <code class="language-javascript">
        var flag = new Boolean(); 值为false的布尔类型</code></pre>
        
        <p><img alt="" height="130" src="https://img-blog.csdnimg.cn/20200623152426525.png" width="273" /></p>
        
        <p>我们知道<strong>布尔类型也重写了toString方法</strong></p>
        
        <pre>
        <code class="language-javascript">
               var flag = true;
               console.log(flag.toString(), typeof flag.toString()); true string
               console.log(flag, typeof flag);   true "boolean"</code></pre>
        
        <h3>3、数字的原型上的方法</h3>
        
        <pre>
        <code class="language-javascript">
        var num = new Number(); 值为0的数字类型 </code></pre>
        
        <p><img alt="" height="198" src="https://img-blog.csdnimg.cn/2020062315250735.png" width="274" /></p>
        
        <p><strong>Number原型上的这三个方法，都是返回字符串 ，且不改变原数字。</strong></p>
        
        <p><strong>&nbsp;（1）toString方法</strong>：返回字符串类型，不改变数字原本的类型。</p>
        
        <pre>
        <code class="language-javascript">
                var num1 = 123456;
                console.log(num1.toString());  字符串123456
                console.log(typeof num1.toString()); 类型string
                console.log(typeof num1); 类型number</code></pre>
        
        <p><strong>（2）toFixed方法</strong>：保留几位小数且会进行四舍五入，返回值是四舍五入后的结果，不改变原本的数字。不够时用0代替。</p>
        
        <p>注意参数 在：&nbsp;0 ~ 20 之间的值，包括 0 和 20</p>
        
        <pre>
        <code class="language-javascript">
                var num1 = 1.23456;
                console.log(num1.toFixed(3),typeof num1.toFixed(3)); 1.235 string
                console.log(num1); 数字1.23456</code></pre>
        
        <p>小数部分不够时，用0代替。</p>
        
        <pre>
        <code class="language-javascript">
                var num1 = 123456;
                console.log(num1.toFixed(3));    123456.000</code></pre>
        
        <p><strong>（3）toExponential方法</strong>：转为科学计数法，参数为保留的小数个数，会进行四舍五入。</p>
        
        <pre>
        <code class="language-javascript">
                var num1 = 123456;
                console.log(num1.toExponential(3),typeof num1.toExponential(3)); 1.235e+5 string
                console.log(num1); 数字123456</code></pre>
        
        <p>再比如：科学计数法保留七位小数：</p>
        
        <pre>
        <code class="language-javascript">
        var num1 = 123456;
        console.log(num1.toExponential(7),typeof num1.toExponential(7)); 1.2345600e+5 string</code></pre>
        
        <p>&nbsp;</p>
        "`
        ,
        js_arguments: `"<h2>一、MDN的arguments详解</h2>

        <p><strong><code>arguments</code></strong>&nbsp;是一个对应于传递给函数的参数的类数组对象。</p>
        
        <h2 id="语法">语法</h2>
        
        <pre>
        <code class="language-html">arguments</code></pre>
        
        <h2 id="Description">描述</h2>
        
        <p><code>arguments</code>对象是所有（非箭头）函数中都可用的<strong>局部变量</strong>。你可以使用<code>arguments</code>对象在函数中引用函数的参数。此对象包含传递给函数的每个参数，第一个参数在索引0处。例如，如果一个函数传递了三个参数，你可以以如下方式引用他们：</p>
        
        <pre>
        <code>arguments[0]
        arguments[1]
        arguments[2]
        </code></pre>
        
        <p>参数也可以被设置：</p>
        
        <pre>
        <code>arguments[1] = 'new value';</code></pre>
        
        <p><strong><code>arguments</code>对象不是一个&nbsp;<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Array"><code>Array</code></a>&nbsp;</strong>。它类似于<code>Array</code>，但除了length属性和索引元素之外没有任何<code>Array</code>属性。例如，它没有&nbsp;<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/pop">pop</a>&nbsp;方法。</p>
        
        <p>比如以下实例，对arguments对象的使用：</p>
        
        <p><strong>method方法打印的this为obj对象；fn方法打印的this为arguments对象。</strong></p>
        
        <pre>
        <code class="language-javascript">
        var length = 10;
        function fn() {
            console.log(this);
        };
        var obj = {
            length: 5, 
            method: function (fn) {
                console.log(this);
                arguments[0]();
            }
        };
        obj.method(fn, 1);</code></pre>
        
        <p><strong>但是它可以被转换为一个真正的<code>Array</code>：</strong></p>
        
        <pre>
        <code>var args = Array.prototype.slice.call(arguments);
        var args = [].slice.call(arguments);
        
        // ES2015
        const args = Array.from(arguments);
        const args = [...arguments];
        </code></pre>
        
        <p>对参数使用slice会阻止某些JavaScript引擎中的优化 (比如 V8&nbsp;-&nbsp;<a href="https://github.com/petkaantonov/bluebird/wiki/Optimization-killers#3-managing-arguments">更多信息</a>)。如果你关心性能，尝试通过遍历arguments对象来构造一个新的数组。另一种方法是使用被忽视的<code>Array</code>构造函数作为一个函数：</p>
        
        <pre>
        <code>var args = (arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments));
        </code></pre>
        
        <p>如果调用的参数多于正式声明接受的参数，则可以使用<code>arguments</code>对象。这种技术对于可以传递可变数量的参数的函数很有用。使用&nbsp;<code><a href="https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Functions_and_function_scope/arguments/length">arguments.length</a></code>来确定传递给函数参数的个数，然后使用<code>arguments</code>对象来处理每个参数。要确定函数<a href="https://developer.mozilla.org/zh-CN/docs/Glossary/Signature/Function">签名</a>中（输入）参数的数量，请使用<code><a href="https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/length">Function.length</a></code>属性。</p>
        
        <h3 id="对参数使用_typeof">对参数使用&nbsp;<code>typeof</code></h3>
        
        <p>typeof参数返回 &#39;object&#39;。</p>
        
        <pre>
        <code>console.log(typeof arguments);    // 'object'
        // arguments 对象只能在函数内使用
        function test(a){
            console.log(a,Object.prototype.toString.call(arguments));
            console.log(arguments[0],arguments[1]);
            console.log(typeof arguments[0]);
        }
        test(1);
        /*
        1 "[object Arguments]"
        1 undefined
        number
        */</code></pre>
        
        <p>可以使用索引确定单个参数的类型。</p>
        
        <pre>
        <code>console.log(typeof arguments[0]); //this will return the typeof individual arguments.</code></pre>
        
        <h3 id="对参数使用扩展语法">对参数使用扩展语法</h3>
        
        <p>您还可以使用<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/from"><code>Array.from()</code></a>方法或<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Spread_operator">扩展运算符</a>将参数转换为真实数组：</p>
        
        <pre>
        <code>var args = Array.from(arguments);
        var args = [...arguments];</code></pre>
        
        <h2 id="Properties">属性</h2>
        
        <p><code><a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/arguments/callee">arguments.callee</a></code></p>
        
        <p>指向当前执行的函数。</p>
        
        <p><code><a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/arguments/caller">arguments.caller</a></code>&nbsp;</p>
        
        <p>指向调用当前函数的函数。</p>
        
        <p><code><a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/arguments/length">arguments.length</a></code></p>
        
        <p>指向传递给当前函数的参数数量。</p>
        
        <p><code><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments/@@iterator">arguments[@@iterator]</a></code></p>
        
        <p>返回一个新的Array迭代器对象，该对象包含参数中每个索引的值。</p>
        
        <p>注意:现在在严格模式下，<code>arguments</code>对象已与过往不同。<code><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments/@@iterator">arguments[@@iterator]</a></code>不再与函数的实际形参之间共享，同时caller属性也被移除。</p>
        
        <h3 id="剩余参数、默认参数和解构赋值参数">ES6的剩余参数、默认参数和解构赋值参数</h3>
        
        <p>剩余参数：用剩余形参y来接收过多的实参。</p>
        
        <p>例如：x值1，y值为数组[2,3,4]</p>
        
        <pre>
        <code class="language-javascript">
                function func(x,...y){
                    console.log(x);
                    console.log(y);
                }
                func(1,2,3,4);</code></pre>
        
        <p>默认参数：如果函数没有传入给形参值（对应形参为实参），那么该形参就会取默认值。<strong>（切记：arguments只与实参对应）</strong></p>
        
        <p>例如：x的值为1，y的值为5</p>
        
        <pre>
        <code class="language-javascript">
        function func(x,y=5){
                    console.log(x);
                    console.log(y);
                }
                func(1);</code></pre>
        
        <p>结构赋值：</p>
        
        <p>列如：x的值为1，y值为2</p>
        
        <pre>
        <code class="language-javascript">
                function func(x,y){
                    console.log(x);
                    console.log(y);
                }
                var arr = [1,2]
                func(...arr);</code></pre>
        
        <p><code>arguments</code>对象可以与<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Rest_parameters">剩余参数</a>、<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Default_parameters">默认参数</a>和<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment">解构赋值</a>参数结合使用。</p>
        
        <pre>
        <code>function foo(...args) {
          return args;
        }
        foo(1, 2, 3);  // [1,2,3]
        </code></pre>
        
        <p>&nbsp;</p>
        
        <p>当非严格模式中的函数<strong>没有</strong>包含<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Rest_parameters">剩余参数</a>、<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Default_parameters">默认参数</a>和<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment">解构赋值</a>，那么<code>arguments</code>对象中的值<strong>会</strong>跟踪参数的值（反之亦然）。看下面的代码：</p>
        
        <pre>
        <code>function func(a) { 
          arguments[0] = 99;   // 更新了arguments[0] 同样更新了a
          console.log(a);
        }
        func(10); // 99
        </code></pre>
        
        <p>并且</p>
        
        <pre>
        <code>function func(a) { 
          a = 99;              // 更新了a 同样更新了arguments[0] 
          console.log(arguments[0]);
        }
        func(10); // 99
        </code></pre>
        
        <p>当非严格模式中的函数<strong>有</strong>包含<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Rest_parameters">剩余参数</a>、<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Default_parameters">默认参数</a>和<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment">解构赋值</a>，那么<code>arguments</code>对象中的值<strong>不会</strong>跟踪参数的值（反之亦然）。相反,&nbsp;<code>arguments</code>反映了调用时提供的参数：</p>
        
        <pre>
        <code>function func(a = 55) { 
          arguments[0] = 99; // updating arguments[0] does not also update a
          console.log(a);
        }
        func(10); // 10</code></pre>
        
        <p>并且</p>
        
        <pre>
        <code>function func(a = 55) { 
          a = 99; // updating a does not also update arguments[0]
          console.log(arguments[0]);
        }
        func(10); // 10</code></pre>
        
        <p>并且</p>
        
        <pre>
        <code>function func(a = 55) { 
          console.log(arguments[0]);
        }
        func(); // undefined
        </code></pre>
        
        <h1>【总结】</h1>
        
        <h3 id="规范">一、非严格模式下的特性</h3>
        
        <p><strong>1、arguments对象与实参映射，通过arguments可以修改实参的值，反之亦然。</strong></p>
        
        <p><strong>2、arguments.callee执行函数体；</strong></p>
        
        <p><strong>3、函数的caller指向该函数的父级函数，arguments.callee.caller。需要注意的是，如果在全局作用域中调用当前函数，它的值null。</strong></p>
        
        <pre>
        <code>        function fatherFunc() {
                    console.log(arguments.callee.caller);//fatherFunc在全局中调用，该值为null
                    function func(x) {
                        console.log(arguments.callee.caller)//打印fatherFunc
                    }
                    func(1);
                }
        
                fatherFunc();</code></pre>
        
        <p><strong>4、必须有实参的传入，arguments只与实参建立联系。</strong></p>
        
        <p><strong>实例1：x与arguments[0]无联系</strong></p>
        
        <pre>
        <code>function fn(x){
           x = 10;
           arguments[0] = 20;
           console.log(x,arguments[0]); //x = 10; arguments[0] = 20;
        }
         fn();
         console.log(window.x);// undefined
         console.log(x);// x is not defined</code></pre>
        
        <p><strong>实例2：x与arguments[0]无联系</strong></p>
        
        <pre>
        <code> function fn(x){
                arguments[0] = 20;
                console.log(x,arguments[0])
            }
            fn();
            // undefined  和 20</code></pre>
        
        <p><strong>&nbsp;实例3：建立联系</strong></p>
        
        <pre>
        <code>function fn(x){
                x = 10;                      
                arguments[0] = 20;
                console.log(x,arguments[0]); // 20 20
            }
            fn(1);</code></pre>
        
        <h3>二、严格模式下的特性</h3>
        
        <p>1、arguments对象和实参的修改，已经互不影响，即，在实参传入到函数中时，arguments的值与其相等，后续对arguments或对实参的修改，两者互不影响。</p>
        
        <p>2、禁止使用arguments.callee和arguments.callee.caller函数的caller值。</p>
        
        <p>&#39;caller&#39;, &#39;callee&#39;, and &#39;arguments&#39; properties may not be accessed on strict mode functions</p>
        
        <h3>三、ES6下的特性</h3>
        
        <p>默认参数、剩余参数、结构赋值，会使函数的arguments也不再与实参相互影响。</p>
        "`
        ,
        js_garbageColletion: `"<p>在固定时间内，找出那些不再使用的变量，然后释放其占用的内存。</p>

        <p>比如函数中的局部变量，在函数执行的过程中，存在于内存中，当函数执行完毕后，局部变量就没有必要存在了。并不是所有的情况都这么容易判断变量是否需要回收。垃圾回收器，必须跟踪哪个变量有用，哪个变量没用，对于不再有用的变量打上标记，等待垃圾回收器的回收。</p>
        
        <p>垃圾回收策略：</p>
        
        <p>标记清除：</p>
        
        <p>引用计数</p>
        "`
        ,
        js_EventLoop: {
            "article_id": "107390035",
            "title": "事件循环和任务队列",
            "description": "终于理解了事件循环和事件队列，怎么说呢，有了ES5的Promise后，需要我重新思考事件的执行顺序以及机制。\n\n\n    console.log(1);\n    const p = new Promise((resolve, reject) =&gt; { // 执行器函数  同步回调\n      console.log(Promise.resolve().then(function () {\n        console.log(5);\n      }));\n      $.get(\"https://.",
            "content": "<p>&nbsp;终于理解了事件循环和任务队列，怎么说呢，有了ES5的Promise后，需要我重新思考事件的执行顺序以及机制。</p>\n\n<pre>\n<code class=\"language-javascript\">    console.log(1);\n    const p = new Promise((resolve, reject) =&gt; { // 执行器函数  同步回调\n      console.log(Promise.resolve().then(function () {\n        console.log(5);\n      }));\n      $.get(\"https://api.apiopen.top/getSingleJoke?sid=28654780\", function (t) {\n        console.log(\"网络请求get1速度与定时器竞争：\");\n      })\n      let tiemr1 = setTimeout(() =&gt; {\n        let timer2 = setTimeout(() =&gt; {\n          console.log(\"虽然get1请求先开启，但是没有该定时器tiemr2先添加到任务队列中\")\n        }, 5);\n        console.log(\"定时器timer1与网络请求get1竞争：\");\n        const time = Date.now();\n        if (time % 2 == 0) {\n          resolve('resolve' + time)\n        } else {\n          reject('reject' + time)\n        }\n        console.log(7);\n      }, 10);\n      console.log(2)\n    })\n    console.log(3)\n    p.then(\n      value =&gt; { // 接收得到resolve传递的数据 \n        console.log(value)\n      },\n      reason =&gt; { // 接收得到reject传递的数据\n        console.log(reason)\n      }\n    )\n\n    Promise.resolve().then(() =&gt; {\n      console.log(6)\n    });\n    console.log(4);</code></pre>\n\n<p><strong>结果：</strong></p>\n\n<p><img alt=\"\" height=\"245\" src=\"https://img-blog.csdnimg.cn/20200716194137416.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"399\" /></p>\n\n<p><strong>js代码是单线程的，执行的机制是：</strong></p>\n\n<p>（1）js是单线程的，自上而下逐行执行</p>\n\n<p>（2）当遇到Promise的构造函数的时候，它的处理函数是立即执行的，所以该处理函数内部也是直接执行的</p>\n\n<p>（3）当遇到定时器或ajax网络请求的时候，等到达条件时，将他们的回到函数，<strong>添加到任务队列中。比如：定时器是由浏览器帮忙计时的，当到达计时的条件后，将定时器内的处理函数添加到任务队列中（宏任务还在执行，浏览器会帮忙计时，到达实践后将处理函数添加到任务队列中，好比宏任务执行的总时间是100ms，而定时器的时间是10ms，那么在定时器开启后的10ms，会将事件添加到任务队列中，当100ms的宏任务执行完后，才会依次执行任务队列中的每个事件）。ajax网络请求的时候，通过网络获取到数据的时候，将处理函数添加到任务队列中。所以这个事件是要竞争的，谁先达到条件谁先添加到任务队列中。</strong></p>\n\n<p>（4）当遇到Promise微任务的时候，比如Promise.resolve().then()方法或者Promise对象的then方法，这是微任务，添加到微任务队列，当然也是按顺序添加到微任务队列的。</p>\n\n<p>（5）当宏任务执行结束后，一次性按顺序将微任务队列中的事件执行</p>\n\n<p>（6）然后开始执行任务队列中的任务，先执行任务队列中的第一个任务，该任务的执行也是按上述的方式，微任务添加到微任务队列中，定时器和ajax事件添加到之前说的那个任务队列中，继续执行下去</p>\n\n<p>注意：所有的定时器和ajax这种异步的事件，当达到条件后添加任务队列中，进行事件执行先后的排序。所以对于异步事件，我们不能看他什么时候开启的，而是什么时候<strong>达到添加条件</strong>的，将其添加到任务队列中。</p>\n\n<p><strong>上面代码执行的思维导图：</strong></p>\n\n<p><img alt=\"上面代码思维导图\" src=\"https://img-blog.csdnimg.cn/20200716202143929.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" /></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "js",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1
        },
        js_dataType: `"<p><strong>原始值</strong></p>

        <p>除 Object 以外的所有类型都是不可变的（<strong>值本身无法被改变</strong>）如，JavaScript 中对字符串的操作一定返回了一个新字符串，原始字符串并没有被改变&mdash;&mdash;原始值</p>
        
        <p><strong>1、Undefined类型</strong></p>
        
        <p>所有变量在未赋值之前，都默认是undefined。</p>
        
        <pre>
        <code class="language-javascript">
        undefined === undefined （true）</code></pre>
        
        <p>JavaScript 中&nbsp;<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null"><code>null</code></a>&nbsp;和&nbsp;<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/undefined"><code>undefined</code></a>&nbsp;是不同的，前者表示一个空值（non-value），必须使用null关键字才能访问，后者是&ldquo;undefined（未定义）&rdquo;类型的对象，表示一个未初始化的值，也就是还没有被分配的值。</p>
        
        <p>需要注意的是：undefined值派生于null值，所以 undefined == null。即所有的未赋值的变量，都 == null。</p>
        
        <p><strong>2、Null类型</strong></p>
        
        <p>如果定义变量为了保存对象，最好初始化的时候为为null。</p>
        
        <pre>
        <code class="language-javascript">
        null === null  （true）</code></pre>
        
        <p>以下的原因是为什么呢？</p>
        
        <pre>
        <code class="language-javascript">
        typeof null === 'object';
        
        null instanceof Object === false
        
        null instanceof null  抛出异常</code></pre>
        
        <p>因为：</p>
        
        <p>在 javascript 的最初版本中，使用的 32 位系统，为了性能考虑使用<strong>低位存储变量类型的信息</strong>：</p>
        
        <ul>
            <li><strong>000：对象</strong></li>
            <li>1：整数</li>
            <li>010：浮点数</li>
            <li>100：字符串</li>
            <li>110：布尔</li>
        </ul>
        
        <p>有 2 个值比较特殊：</p>
        
        <ul>
            <li>undefined：用 - （&minus;2^30）表示。</li>
            <li>null：对应机器码的 NULL 指针，一般是全零。</li>
        </ul>
        
        <p>这样一来，<code>null</code>&nbsp;就出了一个 bug。根据 type tags 信息，低位是&nbsp;<code>000</code>，因此&nbsp;<code>null</code>&nbsp;被判断成了一个对象。这就是为什么&nbsp;<code>typeof null</code>&nbsp;的返回值是&nbsp;<code>object</code>。</p>
        
        <p>&nbsp;</p>
        
        <p><strong>3、Boolean类型</strong></p>
        
        <p>对任何值调用Boolean()函数，可以得到一个Boolean类型的值。</p>
        
        <p>0、false、null、undefined、&quot;&quot;、NaN 这六个值，都会转换为false。</p>
        
        <p><strong>4、Number类型</strong></p>
        
        <p>JavaScript 中只有一种数字类型：基于 IEEE 754 标准的<strong>双精度 64 位</strong>二进制格式的值<code>-9007199254740991 (-(2^53-1))</code>&nbsp;和<code>9007199254740991(2^53-1)</code>之间的整数。</p>
        
        <p>除了能够表示整数浮点数外，还有一些带符号的值：<code>+Infinity</code>，<code>-Infinity</code>&nbsp;和&nbsp;<code>NaN</code>&nbsp;(非数值，Not-a-Number)。</p>
        
        <p>&nbsp;0 可表示为 -0 和 +0（&quot;0&quot; 是 +0 的简写）。例如&nbsp;<code>+0 === -0</code>&nbsp;为真，但是，你可能要注意除以0的时候：</p>
        
        <pre>
        <code>42 / +0; // Infinity
        42 / -0; // -Infinity</code></pre>
        
        <p>在 ECMAScript 6 中，也可以通过&nbsp;<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger"><code>Number.isSafeInteger()</code></a>&nbsp;方法有&nbsp;<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number/MAX_SAFE_INTEGER"><code>Number.MAX_SAFE_INTEGER</code></a>&nbsp;和&nbsp;<a href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number/MIN_SAFE_INTEGER"><code>Number.MIN_SAFE_INTEGER</code></a>&nbsp;来检查值是否在双精度浮点数的取值范围内。 超出这个范围，JavaScript 中的数字不再安全了 。</p>
        
        <p>【1】为什么同样作为包装类的数字，不能直接调用方法，在调用方法的时候会报错？</p>
        
        <p>并不是因为数字不能直接调用toString... 是因为它不知道那个点是小数点还是要调用方法... 可以用下面这两种写法：</p>
        
        <pre>
        <code class="language-javascript">
        
        1['toString']()
        
        (1).toString()    "1"</code></pre>
        
        <p>【2】&nbsp;写一段JS代码，按下面的方式实现plus和minus方法： 输出6。 既实现：(7).plus(2).minus(1);结果为6的代码;</p>
        
        <p>var a = (7).minus(2).plus(1); console.log(a)</p>
        
        <pre>
        <code class="language-javascript">
        Number.prototype.plus = function(n) {
            return this - n;
        };
        Number.prototype.minus = function(n) {
            return this + n;
        }
        var a = (7).plus(2).minus(1);
        alert(a);</code></pre>
        
        <p>知识点：<br />
        （1) 为number原型增加方法。<br />
        （2) this指向调用者。 主要是考察对包装类是否能用prototype（原型）属性，包装类上也会有原型方法，只不过是Number包装类的原型方法用的少而已。String类上用的要多 别外，number类型的值，如果调用原型方法，需要把数学用括号括起来，要不然就是非法的浮点数了。&nbsp;</p>
        
        <p><strong>由此可见：在包装类原型上写的方法，实例对象会继承，就像toString方法，在包装类原型上。而给实例添加的属性和方法，会在创建完成后直接销毁。</strong></p>
        
        <p><strong>【3】包装类：还可以这么创建</strong></p>
        
        <pre>
        <code class="language-javascript">
        var b = Boolean(true);
        var n = Number(10);
        var s = String('hello');</code></pre>
        
        <p>&nbsp;没有new操作符，并不是创建对象，是简单的调用函数。因为在这三个构造函数都在window对象上。调用函数后返回结果就是一个基本数据。</p>
        
        <p>【4】new操作符做了什么？<br />
        &nbsp;</p>
        
        <p>5、BigInt类型</p>
        
        <p><code>BigInt</code>数据类型的目的是比<code>Number</code>数据类型支持的范围更大的整数值。在对大整数执行数学运算时，以任意精度表示整数的能力尤为重要。使用<code>BigInt</code>，整数溢出将不再是问题。</p>
        
        <p>&nbsp;<code>BigInt</code>目前是第3阶段提案， <strong>一旦添加到规范中</strong>，它就是JS 第二个数字数据类型，也将是 JS 第8种基本数据类型&nbsp;。</p>
        
        <p>JS 提供<code>Number.MAX_SAFE_INTEGER</code>常量来表示 最大安全整数，<code>Number.MIN_SAFE_INTEGER</code>常量表示最小安全整数：&nbsp;</p>
        
        <pre>
        <code class="language-javascript">
        const minInt = Number.MIN_SAFE_INTEGER;
        
        console.log(minInt);          → -9007199254740991
        
        console.log(minInt - 5);      → -9007199254740996
        
         超出了安全范围的运算，异常
        console.log(minInt - 4);      → -9007199254740996</code></pre>
        
        <p>具体的解释可以看这篇文章：<a href="https://segmentfault.com/a/1190000019912017">JS最新基本数据类型:BigInt</a></p>
        
        <p><strong>6、String类型</strong></p>
        
        <p>7、Object类型</p>
        
        <p>8、Symbol类型</p>
        
        <p>&nbsp;</p>
        
        <p>&nbsp;</p>
        "`
        ,
        js_constructor: `"<p>我们可以通过constructor来判断数据的类型，但是<strong>除了null、undefined</strong>，因为他们不是由对象构建。</p>

        <p>数字、布尔值、字符串是包装类对象，所以有constructor</p>
        
        <pre>
        <code class="language-javascript">
        数字
        var num = 1;
        num.constructor
        ƒ Number() { [native code] }
        
        布尔值
        true.constructor
        ƒ Boolean() { [native code] }
        
        字符串
        "".constructor
        ƒ String() { [native code] }
        
        函数
        var func = function(){}
        func.constructor
        ƒ Function() { [native code] }
        
        数组
        [].constructor
        ƒ Array() { [native code] }
        
        对象
        var obj = {}
        obj.constructor
        ƒ Object() { [native code] }</code></pre>
        
        <p>&nbsp;</p>
        "`
        ,
        js_prototype: {
            "article_id": "106559385",
            "title": "内置构造函数的原型prototype",
            "description": "我们需要关注内置构造函数的prototype，\n\n1、三个包装类对象\n\n\nNumber.prototype\nNumber{0, constructor: ƒ, toExponential: ƒ, toFixed: ƒ, toPrecision: ƒ,…}\n\nString.prototype\nString{\"\", constructor: ƒ, anchor: ƒ, big: ƒ, blink: ƒ,…}\n\nBoolean.prototype\nBoolean{false, constructor: ...",
            "content": "<p>我们需要关注内置构造函数的prototype，</p>\n\n<p>1、三个包装类对象</p>\n\n<pre>\n<code class=\"language-javascript\">Number.prototype\nNumber {0, constructor: ƒ, toExponential: ƒ, toFixed: ƒ, toPrecision: ƒ, …}\n\nString.prototype\nString {\"\", constructor: ƒ, anchor: ƒ, big: ƒ, blink: ƒ, …}\n\nBoolean.prototype\nBoolean {false, constructor: ƒ, toString: ƒ, valueOf: ƒ}</code></pre>\n\n<p>2、数组</p>\n\n<pre>\n<code class=\"language-javascript\">Array.prototype\n[constructor: ƒ, concat: ƒ, copyWithin: ƒ, fill: ƒ, find: ƒ, …]</code></pre>\n\n<p>3、对象</p>\n\n<pre>\n<code class=\"language-javascript\">Object.prototype\n{constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}</code></pre>\n\n<p>4、函数</p>\n\n<pre>\n<code class=\"language-javascript\">Function.prototype\nƒ () { [native code] }</code></pre>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "js",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106559385"
        },
        js_proto__:{
            "article_id": "106425207",
            "title": "函数原型",
            "description": "1、构造函数：\n\n【1】构造函数和普通函数的唯一区别，在于调用方式不同。任何函数只要通过new操作符来调用，那就是构造函数；任何函数只要不通过new操作符来调用，那就是普通函数。\n\n【2】任何函数都有prototype属性（函数也是对象），该属性指向函数原型。\n\n原型上有一个不可枚举的constructor属性：指向该函数；还有一个[[Prototype]]指针，指向原型链的上一级。没有标准的方式访问[[Prototype]]，但Firefox、Safari、Chrome都用__proto__来表示。\n\n\n",
            "content": "<p>1、构造函数：</p>\n\n<p>【1】构造函数和普通函数的唯一区别，在于调用方式不同。任何函数只要通过new操作符来调用，那就是构造函数；任何函数只要不通过new操作符来调用，那就是普通函数。</p>\n\n<p>【2】任何函数都有prototype属性（函数也是对象），该属性指向函数原型。</p>\n\n<p>原型上有一个不可枚举的constructor属性：指向该函数；还有一个[[Prototype]]指针，指向原型链的上一级。没有标准的方式访问[[Prototype]]，但Firefox、Safari、Chrome都用__proto__来表示。</p>\n\n<pre>\n<code class=\"language-javascript\">constructor: ƒ Person()\n__proto__: Object</code></pre>\n\n<p>【3】在理解原型时，我们把构造函数当做对象来看待</p>\n\n<p>构造函数的prototype属性指向原型，原型的constructor属性指向构造函数。</p>\n\n<p>new操作符构建的实例对象[[prototype]]指向原型，继承原型。</p>\n\n<pre>\n<code class=\"language-javascript\">        function foo(){\n            console.log(1)\n        }\n        console.log(foo);\n        console.log(foo.prototype);</code></pre>\n\n<p><img alt=\"\" height=\"107\" src=\"https://img-blog.csdnimg.cn/20201022185034924.png\" width=\"194\" /></p>\n",
            "markdowncontent": "",
            "tags": "js",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106425207"
        },
        //函数
        js_function: {
            "article_id": "106559765",
            "title": "Function类型（函数）",
            "description": "Function类型\n\n1、函数实际上是对象，函数名是指向函数对象的指针，因此函数名与其他指向该函数的变量没有什么不同。\n\n比如：\n\n\n        function func (){\n            console.log(\"funtion\");\n        }\n        var func1 = func;\n        func1();\n        console.log(func === func1);\n\n结果是打印“function”和“true”。即func和func1都",
            "content": "<h2>Function类型</h2>\n\n<p><strong>1、函数实际上是对象，函数名是指向函数对象的指针，因此函数名与其他指向该函数的变量没有什么不同。</strong></p>\n\n<p>比如：</p>\n\n<pre>\n<code class=\"language-javascript\">        function func (){\n            console.log(\"funtion\");\n        }\n        var func1 = func;\n        func1();\n        console.log(func === func1);</code></pre>\n\n<p>结果是打印&ldquo;function&rdquo;和&ldquo;true&rdquo;。即func和func1都指向同一个地址。&nbsp;</p>\n\n<p><strong>2、每个函数都是Function类型的实例，与其他引用类型一样，具有属性和方法。函数是引用类型。</strong></p>\n\n<p><strong>3、函数作为对象</strong></p>\n\n<p><strong>（1）属性</strong></p>\n\n<p>name属性：该函数的名字</p>\n\n<p>length属性：该函数希望接收的参数个数，即形参的个数。</p>\n\n<p><strong>prototype属性：函数的原型</strong></p>\n\n<p><strong>caller属性：该函数的作用域父级函数。该属性还未作为标准，但火狐谷歌IE等主流浏览器都已经实现。</strong></p>\n\n<p>第一：构造函数的prototype属性所指向的是原型。</p>\n\n<p>第二：实例的__proto__属性指向的是原型。</p>\n\n<p>所以：数组的原型是数组&nbsp;</p>\n\n<pre>\n<code class=\"language-javascript\">        实例的原型\n        console.log(arr.__proto__);\n        [constructor: ƒ, concat: ƒ, copyWithin: ƒ, fill: ƒ, find: ƒ, …]\n        \n        构造函数的原型\n        console.log(Array.prototype);\n        [constructor: ƒ, concat: ƒ, copyWithin: ƒ, fill: ƒ, find: ƒ, …]</code></pre>\n\n<p>所以：对象的原型是对象&nbsp;</p>\n\n<pre>\n<code class=\"language-javascript\">        实例的原型\n        console.log({}.__proto__);\n        {constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}\n\n        构造函数的原型\n        console.log(Object.prototype);\n        {constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}</code></pre>\n\n<p>所以：函数的原型是函数</p>\n\n<pre>\n<code class=\"language-javascript\">函数实例的原型\nconsole.log(func.__proto__)\nƒ () { [native code] }\n\n函数构造函数的原型\nconsole.log(Function.prototype);\nƒ () { [native code] }</code></pre>\n\n<p>&nbsp;特别注意：构造函数有prototype属性指向原型，即函数都是有prototype属性的，所以：函数的prototype属性指向<br />\n<img alt=\"\" height=\"56\" src=\"https://img-blog.csdnimg.cn/20200622103049200.png\" width=\"192\" /></p>\n\n<p><strong>（2）两个对象</strong></p>\n\n<p>this对象：指向当前调用者。</p>\n\n<p>arguments对象：与实参相关，类数组对象。</p>\n\n<p><strong>（3）三个方法</strong></p>\n\n<p><strong>call、apply、bind方法都是Function.prototype原型对象上的方法，即Function.prototype.call（）Function.prototype.apply（）&nbsp; &nbsp;和Function.prototype.bind（）方法 。</strong></p>\n\n<p><strong>Function.prototype.toString方法：</strong>返回一个表示当前函数源代码的字符串。返回当前函数的函数体。<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Function\"><code>Function</code></a>对象覆盖了从<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object\"><code>Object</code></a>继承来的<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/toString\"><code>toString</code></a>&nbsp;方法。在<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Function\"><code>Function</code></a>需要转换为字符串时，通常会自动调用函数的&nbsp;<code>toString&nbsp;</code>方法。</p>\n\n<p><strong>所以Function构造函数的每个实例函数都可以直接调用这几个方法。</strong></p>\n\n<p><strong>call方法：</strong>改变函数的this指向，即改变函数的执行上下文。接收的第一个参数为对象，后续参数直接传递给函数，诸葛列举出来</p>\n\n<p><strong>apply方法：</strong>改变函数的this指向，即改变函数的执行上下文。接收第一个参数为对象，第二个参数为数组。</p>\n\n<p><strong>bind方法：</strong>es5方法，接收一个对象，即该函数的执行上下文，不管该函数在哪执行，执行上下文都是传入的对象。bind获得是函数本身，而为直接执行。</p>\n\n<p><strong>4、函数作用域链</strong></p>\n\n<p>当函数访问变量时先在自身作用域内查找，若没有则向外层函数的作用域查找，依此类推，直到找到全局作用域为止。</p>\n\n<p>函数只有执行时，才形成作用域。整个函数执行完毕后，才一级一级的销毁作用域链。</p>\n\n<p><strong>5、this对象</strong></p>\n\n<p>函数中，this[属性]，必须查找this对象的属性，有，则使用，没有，就是undefined。</p>\n\n<p>6、函数的toString方法</p>\n\n<p>将函数体直接转为字符串</p>\n\n<pre>\n<code class=\"language-javascript\">function a(){\n    console.log(1)\n}\nconsole.log(typeof a.toString()+\"\\n\", a.toString())</code></pre>\n\n<p>&nbsp;<img alt=\"\" height=\"66\" src=\"https://img-blog.csdnimg.cn/20200930105327918.png\" width=\"167\" /></p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "js",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106559765"
        },
        js_with: `<a href="#js_with"></a>"<p><strong>1、with 语句用于设置代码在特定对象中的作用域。</strong></p>

        <p>例如：</p>
        
        <pre>
        <code class="language-javascript">
                var obj = {
                    name: "obj"
                }
                var name = "window";
                function func(){
                    var name = "func";
                    with(obj){
                        console.log(name);
                    }
                }
                func();</code></pre>
        
        <p>打印的结果：obj；</p>
        
        <p>实际上：对于with语句而言，会将指定的对象添加到作用域链中。</p>
        
        <pre>
        <code class="language-javascript">
                var obj = {
                    // name: "obj"
                }
                var name = "window";
                function func(){
                    var name = "func";
                    with(obj){
                        console.log(name);
                    }
                }
                func();</code></pre>
        
        <p>打印的结果：func；</p>
        
        <p><strong>2、理解with的作用，理解函数作用域链和this对象</strong></p>
        
        <p>例如：</p>
        
        <pre>
        <code class="language-javascript">
        ({
        x: 10,
        foo: function () {
            function bar() {
                console.log(x);
                console.log(y);
                console.log(this.x);
            }
            with (this) {
                var x = 20;
                var y = 30;
                bar.call(this);
            }
        }
        }).foo();</code></pre>
        
        <p><strong>分析：</strong></p>
        
        <h3>with的特点：<strong>（请完全理解下面的with特点）</strong></h3>
        
        <p><strong>作用域链的顶部添加with传入的对象，即：with内先查找和操作该对象的属性，如果该对象没有找到，再往作用域链的下一级查找，然后操作操作。</strong>（<strong>obj作用域&mdash;&mdash;&gt;函数1&mdash;&mdash;&gt;函数2&mdash;&mdash;&gt;...&mdash;&mdash;&gt;window全局作用域）</strong><br />
        问题中，变量声明提示和函数声明提升后的实际代码是：</p>
        
        <pre>
        <code class="language-javascript">
        ({
        x: 10,
        foo: function () {
            var x;//默认undefined，等会分析要用到
            var y;//默认undefined
            function bar() {
                console.log(x);
                console.log(y);
                console.log(this.x);
            }
            with (this) {
                x = 20;
                y = 30;
                bar.call(this);
            }
        }
        }).foo();</code></pre>
        
        <p><strong>var x； x变量的默认值undefined；</strong><br />
        <strong>var y；y变量的默认值undefined；</strong><br />
        <strong>等会分析会用到</strong></p>
        
        <p><strong>当代码走到with中时，this是外面的大对象。</strong><br />
        （1）x =20<br />
        根据with的特点（上面提到的），先在this大对象中找x属性（对象中叫属性，函数叫变量），巧了，this大对象有x:10，将值改为20；<br />
        （2）y =30<br />
        同样的，在this大对象中找y属性，没有！再去foo函数中y变量，有！设置y为30；<br />
        （3）bar.call(this)<br />
        大家要注意：call apply bind（ES5）都是只改变this指向，this指向只与this对象相关的操作有关。与函数变量没有一点关系！！！</p>
        
        <p>执行bar函数，call修改this指向，指向大对象。</p>
        
        <p><strong>bar函数执行</strong><br />
        （1）打印变量x。bar函数中没有变量x，往上面的作用域找，foo函数中有变量x，x的值是undefined。<strong>（with改变的x是指大对象的x属性）</strong><br />
        （2）打印变量y。bar函数没有变量y，往上找，foo函数中有变量y，值为30。<strong>（with语句改变y的时候，先去大对象中找y属性，没有，再去foo函数中找到y变量，并将其修改为30）</strong><br />
        （3）打印this.x。打印大对象的x属性，值为20。</p>
        
        <h3>所以结果：</h3>
        
        <p><strong>变量x：undefined</strong><br />
        <strong>变量y：30</strong><br />
        <strong>this.x：20</strong></p>
        
        <p>&nbsp;</p>
        
        <p>&nbsp;</p>
        "`
        ,
        js_closure: `"<h2>一、闭包closure</h2>

        <p>严格来说，闭包需要满足三个条件：【1】访问所在作用域；【2】函数嵌套；【3】在所在作用域外被调用<br />
        有些人觉得只满足条件1就可以，所以IIFE是闭包；有些人觉得满足条件1和2才可以，所以被嵌套的函数才是闭包；有些人觉得3个条件都满足才可以，所以在作用域以外的地方被调用的函数才是闭包</p>
        
        <p><strong>1、函数作用域链的副作用，引起闭包，闭包所保存的是整个变量对象</strong></p>
        
        <pre>
        <code class="language-javascript">
                function foo() {
                    var arr = [];
                    for (var i = 0; i &lt; 5; i++) {
                        arr[i] = function () {
                            console.log(i);
                        }
                    }
                    return arr;
                }
                var arrFunc = foo();
                for (var j = 0; j &lt; arrFunc.length; j++) {
                    arrFunc[j]();//55555
                }</code></pre>
        
        <p>&nbsp;打印结果：55555</p>
        
        <p>分析：数组arr中的每一项都指向一个新的函数，每个函数的作用域链都指向foo函数，所以当arr数组循环打印的时候，其实arr每项的function中没有<strong>i值</strong>，需要往作用域链上找&mdash;&mdash;&gt;foo函数中的<strong>i值</strong></p>
        
        <p><img alt="" height="280" src="https://img-blog.csdnimg.cn/20200609170244889.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70" width="780" /></p>
        
        <p>根据上面图解，我们<strong>让arr每项函数，的上级函数作用域 指向不同的且独立的函数</strong>，这样解决闭包中共享<strong>i值</strong>的问题</p>
        
        <pre>
        <code class="language-javascript">
                function foo(){
                    var arr = [];
                    for(var i = 0; i &lt; 5; i++){
                        arr[i] = function(j){
                            return function(){
                                console.log(j)
                            }
                        }(i)
                    }
                    return arr;
                }
                var arr = foo();
                for(var j = 0; j &lt; arr.length; j ++){
                    arr[j]();
                }</code></pre>
        
        <p>打印结果：01234</p>
        
        <p>&nbsp;分析：每一次匿名的立即执行函数 执行的时候，都生成新的作用域链，新的执行环境；将<strong>i值</strong>立即传入给匿名函数，使得arr数组每项函数在作用域链上查找<strong>i值</strong>的时候，可以找到对应的<strong>i值</strong>。</p>
        
        <p><img alt="" height="230" src="https://img-blog.csdnimg.cn/20200609170327515.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70" width="1124" /></p>
        
        <p>&nbsp;立即执行函数也可以包裹在arr外：达到同样的目的</p>
        
        <pre>
        <code class="language-javascript">
                function foo(){
                    var arr = [];
                    for(var i = 0; i &lt; 5; i++){
                        (function(j){
                            arr[i] = function(){
                                console.log(j)
                            }
                        })(i)
                    }
                    return arr;
                }</code></pre>
        
        <p>&nbsp;</p>
        
        <p><strong>2、立即执行函数&mdash;&mdash;闭包</strong></p>
        
        <p><strong>匿名函数的this指向window</strong></p>
        
        <pre>
        <code class="language-javascript">
                /**
                IIFE立即执行函数，闭包
                */
                var a = 1;
                function foo(){
                    var a = 2;
                    function bar(){
                        var a = 3;
                        (function(){
                            console.log(this);//window
                            console.log(a);//3
                        })()
                    }
                    bar();
                }
                foo();</code></pre>
        
        <p>&nbsp;打印结果：this是window；a是3；</p>
        
        <p>当执行到立即执行函数内部的时候：可以看到闭包a 是 3。</p>
        
        <p><img alt="" height="373" src="https://img-blog.csdnimg.cn/20200609161908745.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70" width="381" /></p>
        
        <p><strong>3、setTimeout的函数this是window</strong></p>
        
        <pre>
        <code class="language-javascript">
                function foo() {
                    for (var i = 0; i &lt; 5; i++) {
                        setTimeout(() =&gt; {
                            console.log(i)
                        }, 1000 * i)
                    }
                }
                foo();</code></pre>
        
        <p>打印结果：55555</p>
        
        <p><strong>分析：</strong></p>
        
        <p>setTimeout执行的环境是window，<strong>形成了闭包</strong>才能获取到foo函数中的<strong>i值</strong>。</p>
        
        <p>事件队列，setTimeout将函数添加到事件队列中，在合适的时间执行。</p>
        
        <p><strong>解决办法：</strong></p>
        
        <p>同样可以用立即执行函数，来给每个setTimeout的函数体的作用域链添加新的一级</p>
        
        <pre>
        <code class="language-javascript">
                function foo() {
                    for (var i = 0; i &lt; 5; i++) {
                        ((j) =&gt; {
                            setTimeout(() =&gt; {
                                console.log(j)
                            }, 1000 * j)
                        })(i)
                    }
                }
                foo();</code></pre>
        
        <p>打印结果：01234</p>
        
        <p>&nbsp;看看下面的例子：<strong>同样根据函数特性，每次执行，生成不同的作用域环境</strong></p>
        
        <pre>
        <code class="language-javascript">
                function foo(j) {
                    setTimeout(() =&gt; {
                        console.log(j)
                    }, 1000 * j)
                }
                for (var i = 0; i &lt; 5; i++) {
                    foo(i)
                }</code></pre>
        
        <p>打印结果：01234</p>
        
        <p>&nbsp;</p>
        
        <p><strong>4、非闭包，而是函数作用域</strong></p>
        
        <p>下面两个实例打印结果都是 55555</p>
        
        <p>函数的作用域是window，而在打印值的时候，window对象中的<strong>i值</strong>已经是5了</p>
        
        <pre>
        <code class="language-javascript">
                var arr = [];
                for (var i = 0; i &lt; 5; i++) {
                    arr[i] = function () {
                        console.log(i);
                    }
                }
                for (var j = 0; j &lt; arr.length; j++) {
                    arr[j]();//55555
                }</code></pre>
        
        <pre>
        <code class="language-javascript">
                for (var i = 0; i &lt; 5; i++) {
                    setTimeout(() =&gt; {
                        console.log(i)
                    }, 1000 * i)
                }</code></pre>
        
        <p><strong>5、并非return才形成闭包，而是因为函数作用域链中，某个函数的引用被暴露到外面，使得可以获取到对应的函数作用域链。</strong></p>
        
        <p><strong>比如：baz函数被引用到与foo函数同等位置的fn变量，也是闭包。</strong></p>
        
        <pre>
        <code class="language-javascript">
                var fn;
                function foo() {
                    var a = 2;
                    function baz() {
                        console.log(a);
                    }
                    fn = baz;
                }
                foo();
                fn(); // 2</code></pre>
        
        <p><strong>由此要对比是：函数中的原始值和引用值被引用到外部是不受影响的。&nbsp;</strong></p>
        
        <h2><strong>二、实际开发中使用闭包</strong></h2>
        
        <p><strong>1、setTimeout、setInterval</strong></p>
        
        <p><strong>当定时器中函数使用函数作用域链的变量时，其实就是闭包。</strong></p>
        
        <pre>
        <code class="language-javascript">
                function foo(){
                    var a = 1,
                        b = 2,
                        c = 3;
                    var obj = {
                        name: "obj"
                    }
                    setTimeout(function(){
                        console.log(a,b,c,obj.name);
                    },200)
                }
                foo();</code></pre>
        
        <p><strong>2、回调函数</strong></p>
        
        <pre>
        <code class="language-javascript">
                function foo() {
                    var a = 1,
                        b = 2,
                        c = 3;
                    var obj = {
                        name: "obj"
                    }
        
                    function callback() {
                        console.log(a, b, c, obj.name);
                    }
                    getData(callback);
                }
                foo();
                //作用域链 cb --&gt; ajax的执行函数 --&gt; getData
                function getData(cb) {
                    var url = "https://www.easy-mock.com/mock/5b4c4032189fc57b63eb8410/example/getMovie";
                    $.get(url, function (res) {
                        if (res) {
                            console.log(res);
                            if(cb &amp;&amp; typeof cb === "function"){
                                cb(); //回调函数执行位置
                            }
                            
                        }
                    }, "json")
                };</code></pre>
        
        <p>作用域链&nbsp;callback&nbsp;--&gt;&nbsp;foo</p>
        
        <p>作用域链&nbsp;cb&nbsp;--&gt;&nbsp;ajax的执行函数&nbsp;--&gt;&nbsp;getData</p>
        
        <p><strong>将某个作用域链的函数传给另一个作用域链，且执行。也是闭包</strong></p>
        
        <p>&nbsp;</p>
        
        <p><strong>3、jquery自定义组件</strong></p>
        
        <p>立即执行函数内，定义的对象，变量，构造函数，构造函数的原型，方法等，都可以通过一个闭包放在jquery对象上，供外部使用。</p>
        
        <pre>
        <code class="language-javascript">
                (function () {
                    function Dialog() {
        
                    }
                    Dialog.prototype.init = function () {
        
                    }
                    var methods = {
                        get: function () {
        
                        },
                        set: function () {
        
                        }
                    }
                    $.fn.pluginDialog = function (param, data) {
                        if (param &amp;&amp; typeof param === "object") {
                            var dialog = new Dialog(param);
                            $(this).data("dialog", dialog);
                        } else if (methods[param]) {
                            methods[param].call(this, data)
                        }
                    }
                })(jQuery)</code></pre>
        
        <h2>三、知乎精选：</h2>
        
        <p><strong><a href="https://zhuanlan.zhihu.com/p/25855075">从闭包说起的面试题</a></strong></p>
        "`
        ,
        /**数组 */
        js_array_problem: {
            "article_id": "106943896",
            "title": "数组练习题",
            "description": "1、不使用for循环，实现一个方法，传入m n两个参数，返回一个长度为m，每项为n的数组\n\n（1）递归，使用递归也特别注意return的使用。\n\n\n        function func(m, n) {\n            if(typeof m !== \"number\" &amp;&amp; m &lt;= 0){\n                return arr;\n            }else {\n                arr[arr.length] = n;\n       ",
            "content": "<h3>关于数组的练习题</h3>\n\n<p><strong>1、不使用for循环，实现一个方法，传入m n两个参数，返回一个长度为m，每项为n的数组</strong></p>\n\n<p>（1）递归，使用递归也特别注意return的使用。</p>\n\n<pre>\n<code class=\"language-javascript\">        function func(m, n) {\n            if(typeof m !== \"number\" &amp;&amp; m &lt;= 0){\n                return arr;\n            }else {\n                arr[arr.length] = n;\n                return arguments.callee(m-1, n);\n            }\n        }\n        console.log(func(8, 5)); [5, 5, 5, 5, 5, 5, 5, 5]</code></pre>\n\n<p>（2）类数组转数组后，调用map方法。Array.from得到是长度为m值为undefined的数组。不是空数组。</p>\n\n<pre>\n<code class=\"language-javascript\">        function func(m, n) {\n            var arrObj = {\n                length: m\n            }\n            return Array.from(arrObj).map(() =&gt; n);\n        }\n        console.log(func(8, 5));</code></pre>\n\n<p>其实Array.from，第二个参数是一个处理函数，跟map的效果一样：</p>\n\n<pre>\n<code class=\"language-javascript\">        function func(m, n) {\n            var arrObj = {\n                length: m\n            }\n            return Array.from(arrObj, () =&gt; n);\n        }\n        console.log(func(8, 5)); [5, 5, 5, 5, 5, 5, 5, 5]</code></pre>\n\n<p>&nbsp;以下方式得到的还是空数组。调用map方法无效。</p>\n\n<pre>\n<code class=\"language-javascript\">        function func(m, n) {\n            var arr = [];\n            arr.length = m;\n            return arr.map(() =&gt; n);\n        }</code></pre>\n\n<p><strong>2、编写一个程序将数组扁平化去并除其中重复部分数据，最终得到一个升序且不重复的数组</strong></p>\n\n<p>var arr = [ [1, 2, 2], [3, 4, 5, 5], [6, 7, 8, 9, [11, 12, [12, 13, [14] ] ] ], 10];</p>\n\n<pre>\n<code class=\"language-javascript\">var arr = [ [1, 2, 2], [3, 4, 5, 5], [6, 7, 8, 9, [11, 12, [12, 13, [14] ] ] ], 10];\nvar result = [];\narr.toString().split(\",\").sort((a,b)=&gt;a-b).map((item,index)=&gt;{\n//split的拆分使得数组的每项都是字符串类型\nresult.indexOf(item) == -1 ? result.push(Number(item)) : \"\";\n});\nconsole.log(result);  [1, 2, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10, 11, 12, 12, 13, 14]</code></pre>\n\n<p>数组的toSting会去处理数组每项及其子项，得到字符串：1,2,2,3,4,5,5,6,7,8,9,11,12,12,13,14,10。toSting就是拆除所有的中括号</p>\n\n<p>split按逗号查分字符串：[&quot;1&quot;, &quot;2&quot;, &quot;2&quot;, &quot;3&quot;, &quot;4&quot;, &quot;5&quot;, &quot;5&quot;, &quot;6&quot;, &quot;7&quot;, &quot;8&quot;, &quot;9&quot;, &quot;11&quot;, &quot;12&quot;, &quot;12&quot;, &quot;13&quot;, &quot;14&quot;, &quot;10&quot;]。使得每项都成了字符串类型。</p>\n\n<p><strong>3、请把两个数组 [&#39;A1&#39;, &#39;A2&#39;, &#39;B1&#39;, &#39;B2&#39;, &#39;C1&#39;, &#39;C2&#39;, &#39;D1&#39;, &#39;D2&#39;] 和 [&#39;A&#39;, &#39;B&#39;, &#39;C&#39;, &#39;D&#39;]，合并为 [&#39;A1&#39;, &#39;A2&#39;, &#39;A&#39;, &#39;B1&#39;, &#39;B2&#39;, &#39;B&#39;, &#39;C1&#39;, &#39;C2&#39;, &#39;C&#39;, &#39;D1&#39;, &#39;D2&#39;, &#39;D&#39;]。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var arr1 =  ['A1', 'A2', 'B1', 'B2', 'C1', 'C2', 'D1', 'D2'];\nvar arr2 = ['A', 'B', 'C', 'D'];\narr2.map((item, index) =&gt; {\n    arr1.splice((index+1)*2 + index,0,item);\n})\nconsole.log(arr1); [\"A1\", \"A2\", \"A\", \"B1\", \"B2\", \"B\", \"C1\", \"C2\", \"C\", \"D1\", \"D2\", \"D\"]</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "javascript",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106943896"
        },
        js_array: {
            "article_id": "106809064",
            "title": "js数组",
            "description": "一、数组介绍\n\n1、数组是有序列表，每一项可以保存任何类型的数据\n\n2、创建数组的方式：\n\n（1）构造函数\n\n\n        var arr = new Array(1,2,3);\n        console.log(arr);\n\n如果只是一个数字，表示数组的长度，比如：\n\n\n        var arr = new Array(3);\n        console.log(arr);\n        \n        //结果：[empty × 3]\n\n（2）数组字面量\n\n与对象一样，数组字..",
            "content": "<h3>一、数组介绍</h3>\n\n<p>1、数组是有序列表，每一项可以保存任何类型的数据</p>\n\n<p>关于对象属性：在&nbsp;JavaScript 中，以数字开头的属性不能用点号引用，必须用方括号。如果一个对象有一个名为&nbsp;<code>3d</code>&nbsp;的属性，那么只能用方括号来引用它。</p>\n\n<pre>\n<code class=\"language-javascript\">obj['3d']</code></pre>\n\n<p>&nbsp;引出数组属性：</p>\n\n<pre>\n<code class=\"language-javascript\">arr[0]  ||  arr['0']</code></pre>\n\n<p>2、创建数组的方式：</p>\n\n<p>（1）构造函数</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = new Array(1,2,3);\n        console.log(arr);</code></pre>\n\n<p>&nbsp;如果只是一个数字，表示数组的长度，比如：</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = new Array(3);\n        console.log(arr);\n        \n        //结果：[empty × 3]</code></pre>\n\n<p>&nbsp;（2）数组字面量</p>\n\n<p>与对象一样，数组字面量创建数组时，也不会调用Array构造函数。</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,3];\n        console.log(arr);</code></pre>\n\n<p>3、数组的length属性可以读写，通过设置length的值，可以从数组的末尾移除或添加新的项。</p>\n\n<p>4、数组调用toString方法：实际上数组的每项循环遍历调用toString方法，且是根据数组的每项的值的类型，调用对应类型的toString方法，<strong>此处特别注意对象的toString方法。特别注意：undefined和null没有toString方法，所以数组的toString方法在处理该项的时候，返回空字符。</strong></p>\n\n<p>例如：</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,\"str\",[false,[4],3],{name:\"zhu\"}];\n        console.log(arr.toString());\n\n        //结果\n        1,2,str,false,4,3,[object Object]</code></pre>\n\n<p>&nbsp;还有一些方法，会隐式的调用toString方法，比如alert方法，就会调用toString方法，那么当alert(数组)的时候，该数组也是执行toString（）方法。</p>\n\n<p>数组对toString方法进行了重写，但是由于undefined和null没有toString方法，所以数组调用toString方法时，如果有的项是undefined或null，该项得到的字符串为空。比如：</p>\n\n<pre>\n<code class=\"language-javascript\">var arr = [undefined, null, 1, false, [2,3],{}, \"abc\"]\nconsole.log(arr.toString());\n\",,1,false,2,3,[object Object],abc\"</code></pre>\n\n<h3>二、判断数组</h3>\n\n<p>1、调用对象的toString方法来判断</p>\n\n<p><strong>注意：此处的Object是构造函数</strong></p>\n\n<pre>\n<code class=\"language-javascript\">Object.prototype.toString.call(arr) === \"[object Array]\"</code></pre>\n\n<p>2、ES5的方法</p>\n\n<p><strong>Array.isArray()方法：该方法是Array构造函数上的方法</strong></p>\n\n<pre>\n<code class=\"language-javascript\">Array.isArray(arr)</code></pre>\n\n<p>3、arr.constructor其实是arr的原型上的构造函数属性constructor，指向Array的函数体</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,\"str\",[false,[4],3],{name:\"zhu\"}];\n        console.log(arr.constructor);//Array的构造函数的函数体\n        console.log((arr.constructor+\"\").indexOf(\"Array\") !== -1)</code></pre>\n\n<h3>&nbsp;</h3>\n",
            "markdowncontent": "",
            "tags": "js,javascript,数组的方法",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106809064"
        },
        js_array_methods:{
            "article_id": "106817618",
            "title": "js数组方法",
            "description": "11、indexOf方法和lastINdexOf方法 -- ES5方法\n\n正向查找和反向查找数组中的值，返回找到的第一个值的索引，如果没有返回-1\n\n\n        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\",\"b\"];\n        console.log(arr.indexOf(\"b\"));//2\n        console.log(arr.lastIndexOf(\"b\"));//5\n\n12、迭代方法 -- ES5方法\n\n对数组的每一项指定一个运行函数\n\n参数：item，i",
            "content": "<h2>数组方法</h2>\n\n<p><strong>改变原数组的方法有：push、pop、unshift、shift、sort、reverse、splice</strong></p>\n\n<p><strong>操作数组得到的结果数组，只是得到副本，得到原数组引用数据的地址所指的空间，得到原始值的复制值。</strong></p>\n\n<p><strong>（1）修改地址所指的空间值，副本受到影响</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var arr = [[\"a\"],2,3]\nvar arr1 = arr.slice(0,1);\narr[0][0] = \"b\";\nconsole.log(arr, arr1);</code></pre>\n\n<p><img alt=\"\" height=\"112\" src=\"https://img-blog.csdnimg.cn/20200829103309243.png\" width=\"359\" /></p>\n\n<p><strong>（2）改变原数组第一项的地址，副本不受影响。因为副本的地址还指向之前的位置。就好像构造函数创建的两个实例对象，当其中一个对象修改地址和修改地址的内容，结果是不同。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var arr = [[\"a\"],2,3]\nvar arr1 = arr.slice(0,1);\narr[0] = [\"b\"];\nconsole.log(arr, arr1);</code></pre>\n\n<p>&nbsp;<img alt=\"\" height=\"109\" src=\"https://img-blog.csdnimg.cn/20200829104151175.png\" width=\"350\" /></p>\n\n<p>&nbsp;</p>\n\n<p><strong>1、join方法</strong></p>\n\n<p>遍历每一项及其子项调用toString方法，使得每项及其子项成为字符串。</p>\n\n<p>用一个字符串来拼接数组的每一项，如果不给join传值，或者传入undefined值，默认按逗号拼接</p>\n\n<p><strong>特点：首先将数组的每一项连接，数组的每一项调用toString方法</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,\"str\",[false,undefined,[4],3],{name:\"zhu\"}];\n        console.log(arr.join(\"~\"));\n\n        结果\n        1~2~str~false,,4,3~[object Object]</code></pre>\n\n<p>（1）不改变原数组；（2）返回字符串结果</p>\n\n<p><strong>如果数组的项中有undefined或null：得到空字符；而字符串中处理undefined和null时，得到字符串&ldquo;undefined&rdquo;和&ldquo;null&rdquo;。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var arr = [undefined, null, \"\", 1, false, [2,[8,9],3],{}, \"abc\"]\nconsole.log(arr.join(''));\n\"1false2,8,9,3[object Object]abc\"</code></pre>\n\n<p>&nbsp;join方法与字符串的split方法对应！</p>\n\n<p><strong>2、push方法</strong></p>\n\n<p>在数组末尾添加一项，并修改length值</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,\"str\",[false,[4],3],{name:\"zhu\"}];\n        console.log(arr.push(true)); //6\n        console.log(arr); //[1, 2, \"str\", Array(3), {…}, true]\n        </code></pre>\n\n<p>（1）修改原数组；（2）返回length值</p>\n\n<p><strong>3、pop方法</strong></p>\n\n<p>在数组末尾移除一项，并修改length值</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,\"str\",[false,[4],3],{name:\"zhu\"}];\n        console.log(arr.pop());//{name: \"zhu\"}\n        console.log(arr);//[1, 2, \"str\", Array(3)]</code></pre>\n\n<p>（1）修改原数组；（2）返回移除项</p>\n\n<p><strong>4、unshift方法</strong></p>\n\n<p>在数组前面添加一项，并修改length值</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,\"str\",[false,[4],3],{name:\"zhu\"}];\n        console.log(arr.unshift(true));//6\n        console.log(arr);//[true, 1, 2, \"str\", Array(3), {…}]</code></pre>\n\n<p>（1）修改原数组；（2）返回length值&nbsp;</p>\n\n<p><strong>5、shift方法</strong></p>\n\n<p>在数组前面移除一项，并修改length值</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,\"str\",[false,[4],3],{name:\"zhu\"}];\n        console.log(arr.shift());//1\n        console.log(arr);//[2, \"str\", Array(3), {…}]</code></pre>\n\n<p>（1）修改原数组；（2）返回移除项&nbsp;</p>\n\n<p><strong>6、reverse方法</strong></p>\n\n<p>反转数组</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,2,\"str\",[false,[4],3],{name:\"zhu\"}];\n        console.log(arr.reverse());//[{…}, Array(3), \"str\", 2, 1]\n        console.log(arr);//[{…}, Array(3), \"str\", 2, 1]</code></pre>\n\n<p>（1）修改原数组；（2）返回修改后的原数组</p>\n\n<p><strong>7、sort方法</strong></p>\n\n<p>默认按每项的首个字符的ASCII码排序</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\"];\n        arr.sort()\n        console.log(arr);//[\"a\", \"ac\", \"b\", \"bc\", \"c\"]</code></pre>\n\n<p>升序或降序</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1,3,5,2,4];\n        arr.sort(function(a,b){\n            return a - b;//升序\n            return b - a;//降序\n        })\n        console.log(arr);//[1, 2, 3, 4, 5]</code></pre>\n\n<p>（1）修改原数组</p>\n\n<p><strong>8、concat方法</strong></p>\n\n<p>拼接数组</p>\n\n<p>如果没有给该方法传参，只是赋值该数组并返回副本。&mdash;&mdash;<strong>副本</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\"];\n        var arr1 = arr.concat();\n        arr.push(1)\n        console.log(arr);//[\"a\", \"c\", \"b\", \"ac\", \"bc\", 1]\n        console.log(arr1);//[\"a\", \"c\", \"b\", \"ac\", \"bc\"]</code></pre>\n\n<p>传参</p>\n\n<p>可以传递多个参数，每个参数按顺序拼接到副本中，如果参数是一个数组，将该数组的每一项拼接到副本中。</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\"];\n        var arr1 = arr.concat([1,[true],2,3],4,{name:\"zhu\"});\n        console.log(arr);//[\"a\", \"c\", \"b\", \"ac\", \"bc\"]\n        console.log(arr1);//[\"a\", \"c\", \"b\", \"ac\", \"bc\", 1, Array(1), 2, 3, 4, {…}]</code></pre>\n\n<p>（1）不修改原数组；（2）返回拼接后的数组&nbsp;</p>\n\n<p><strong>9、slice方法</strong></p>\n\n<p>赋值原数组作为一个副本，从副本中截取一段</p>\n\n<p><strong>传参为数组索引，如果传的参数有负值，则用数组长度length 加上该值来确定位置。如果结束位置小于起始位置，返回空数组。</strong></p>\n\n<p>不传参，截取整段数组，即copy数组&mdash;&mdash;<strong>副本（如果引用值地址发生变化，副本受影响）</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\"];\n        var arr1 = arr.slice();\n        console.log(arr);//[\"a\", \"c\", \"b\", \"ac\", \"bc\"]\n        console.log(arr1);//[\"a\", \"c\", \"b\", \"ac\", \"bc\"]</code></pre>\n\n<p>传 一个参数，截取时，起始索引值到数组末尾</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\"];\n        var arr1 = arr.slice(1);\n        console.log(arr);//[\"a\", \"c\", \"b\", \"ac\", \"bc\"]\n        console.log(arr1);//[\"c\", \"b\", \"ac\", \"bc\"]</code></pre>\n\n<p>&nbsp;传两个参数，截取的起始索引值到终止索引值</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\"];\n        var arr1 = arr.slice(1,3);\n        console.log(arr);//[\"a\", \"c\", \"b\", \"ac\", \"bc\"]\n        console.log(arr1);//[\"c\", \"b\"]</code></pre>\n\n<p><strong>10、splice方法</strong></p>\n\n<p>传递参数：从第几位，截取几个，插入的值</p>\n\n<p>会 将后面要插入的值，插入到原数组中。</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\"];\n        var arr1 = arr.splice(2,0,true,1,2,3);\n        console.log(arr);//[\"a\", \"c\", true, 1, 2, 3, \"b\", \"ac\", \"bc\"]\n        console.log(arr1);//[]</code></pre>\n\n<p>如果是数组或对象也是直接插入&nbsp;</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\"];\n        var arr1 = arr.splice(2,0,[true,1,2,3],{name:\"zhu\"});\n        console.log(arr);//[\"a\", \"c\", Array(4), {…}, \"b\", \"ac\", \"bc\"]\n        console.log(arr1);//[]</code></pre>\n\n<p>（1）修改原数组；（2）返回截取的数组。</p>\n\n<p><strong>11、indexOf方法和lastIndexOf方法 -- ES5方法</strong></p>\n\n<p>正向查找和反向查找数组中的值，返回找到的第一个值的索引，如果没有返回-1</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\",\"c\",\"b\",\"ac\",\"bc\",\"b\"];\n        console.log(arr.indexOf(\"b\"));//2\n        console.log(arr.lastIndexOf(\"b\"));//5</code></pre>\n\n<p><strong>问：如果是查找引用值呢？</strong></p>\n\n<p><strong>答：indexOf方法查找，是根据===来判断的，如果是引用值，要判断地址是否相同！</strong></p>\n\n<pre>\n<code class=\"language-javascript\">    var obj = {b:2}\n    var arr = [1, true, {a:0}, [\"m\"], undefined, null, NaN, obj];\n    console.log(arr.indexOf(NaN))  // -1\n    console.log(arr.indexOf(null))  //5\n    console.log(arr.indexOf(undefined)) //4\n    console.log(arr.indexOf([\"m\"])) //-1\n    console.log(arr.indexOf(obj)) //7</code></pre>\n\n<p><strong>12、迭代方法 -- ES5方法</strong></p>\n\n<p>对数组的每一项指定一个运行函数</p>\n\n<p>参数：item，index（value，key）</p>\n\n<pre>\n<code class=\"language-javascript\">        arr.map((item,index) =&gt; {\n            console.log(item,index)\n        })</code></pre>\n\n<h3>&nbsp;对这个五个迭代方法的思考：</h3>\n\n<p><strong>forEach</strong>循环处理数组，无返回结果；</p>\n\n<p><strong>map</strong>循环处理，返回处理<strong>函数的返回值</strong>组成<strong>数组</strong>。</p>\n\n<p><strong>filter</strong>循环处理，返回处理<strong>结果为true的项</strong>组成的<strong>数组</strong>。</p>\n\n<p><strong>every</strong>循环处理，每项为true，返回true</p>\n\n<p><strong>some</strong>循环处理，某项为true，返回true。所以some可能不会执行每项，因为找到true项就停止循环。</p>\n\n<p><strong>（1）map 方法&nbsp;</strong></p>\n\n<p>返回每次函数调用<strong>执行结果</strong>组成的数组，即<strong>处理函数的返回值</strong>组成的数组</p>\n\n<p><strong>注意：</strong>&nbsp;<strong>map() 不会对空数组进行检测。arr是长度为5的空数组，map方法不执行。arr为【】空数组也不执行。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">            var arr = new Array(5)\n            arr.map(function(){\n                console.log(1);\n            })</code></pre>\n\n<p><strong>注意：</strong>&nbsp;<strong>map() 不会改变原始数组。</strong></p>\n\n<p><strong>（2）filter方法</strong></p>\n\n<p>返回执行结果true的项组成的数组</p>\n\n<p><strong>（3）forEach方法&nbsp;</strong></p>\n\n<p>没有 返回值</p>\n\n<p><strong>（4）every方法</strong></p>\n\n<p>若每一项的执行结果为true，则返回true</p>\n\n<p><strong>（5）some方法&nbsp;</strong></p>\n\n<p>若任意一项的执行结果为true，则返回true</p>\n\n<p><strong>13、归并方法 -- ES5方法</strong></p>\n\n<p><strong>（1）reduce方法</strong></p>\n\n<p>重点是reduce 方法执行函数的参数&nbsp;</p>\n\n<p>第一个参数是该执行函数的返回值，默认返回值为undefined。</p>\n\n<p>第二个参数是当前执行的项</p>\n\n<p>第三个参数是当前执行的index</p>\n\n<p>第四个参数是arr数组本身。</p>\n\n<p>注意第一次执行的prev是arr[0]，cur是arr[1]，index是1。</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [10,20,30,40];\n        arr.reduce(function(prev,cur,index,arr){\n            console.log(prev,cur,index);\n            return \"a\";\n        })</code></pre>\n\n<pre>\n<code>结果\n10 20 1\na 30 2\na 40 3</code></pre>\n\n<p>reduce除了在接收一个函数外，还可以接收一个参数，作为 prev的初始值&nbsp;</p>\n\n<p>可以这么理解：</p>\n\n<p>第二个参数作为prev的初始值，每次循环的return返回值，会重新赋值给第二个参数，作为下一次循环的初始值。<strong>且reduce的执行结果（方法返回值）就是第二个参数的最终值。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [10, 20, 30, 40,40];\n        var newArr = arr.reduce(function (prev, cur,index) {\n            // prev.indexOf(cur) === -1 &amp;&amp; prev.push(cur);\n            console.log(prev,cur,index);\n            return prev+1;\n        }, 1);\n        console.log(newArr);</code></pre>\n\n<p>结果：</p>\n\n<p><img alt=\"\" height=\"143\" src=\"https://img-blog.csdnimg.cn/20200617190615307.png\" width=\"234\" /></p>\n\n<p><strong>再次理解：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        let arr = [10,20,30,40];\n        let result = arr.reduce((prev,cur,index, arr1) =&gt; {\n            return prev.concat(cur);\n        },[])\n        console.log(result); [10, 20, 30, 40]</code></pre>\n\n<p><strong>在接收第二个参数的情况下，此处接收第二个参数：[]</strong></p>\n\n<p>prev：第二个参数作为prev的初始值，之后prev的值是return的值。</p>\n\n<p>cur：cur就是arr的每项</p>\n\n<p>index：就是每次遍历的index值</p>\n\n<p>arr1：是arr的副本</p>\n\n<p>最终prev值会作为reduce的返回值。</p>\n\n<p>大多数接收一个数组，让prev作为一个数组去处理cur。那么返回值就是一个数组。当然也可以不传数组，传别的类型的值，进行处理。</p>\n\n<p><strong>如果没有第二个参数：</strong>数组arr的第一项作为prev的初始值，而index要从1开始，cur也是得到arr1[1]，开始处理。</p>\n\n<p><strong>（2）reduceRight方法</strong></p>\n\n<p>该方法从数组末尾开始执行，其他的和reduce没有区别。</p>\n\n<p><strong>14、Array.of方法 -- ES6</strong></p>\n\n<p><strong>创建数组的方式：解决Array（7）创建length为7的undefined值的问题。</strong></p>\n\n<p><code><strong>Array.of()</strong></code>&nbsp;方法创建一个具有可变数量参数的新数组实例，而不考虑参数的数量或类型。</p>\n\n<p>&nbsp;<code><strong>Array.of()</strong></code>&nbsp;和&nbsp;<code><strong>Array</strong></code>&nbsp;构造函数之间的区别在于处理整数参数：<code><strong>Array.of(7)</strong></code><strong>&nbsp;</strong>创建一个具有单个元素&nbsp;<strong>7</strong>&nbsp;的数组，而&nbsp;<strong><code>Array(7)</code>&nbsp;</strong>创建一个长度为7的空数组（<strong>注意：</strong>这是指一个有7个空位(empty)的数组，而不是由7个<code>undefined</code>组成的数组）。</p>\n\n<pre>\n<code>Array.of(7);       // [7] \nArray.of(1, 2, 3); // [1, 2, 3]\n\nArray(7);          // [ , , , , , , ]\nArray(1, 2, 3);    // [1, 2, 3]\n</code></pre>\n\n<p id=\"Syntax\">示例</p>\n\n<pre>\n<code>Array.of(1);         // [1]\nArray.of(1, 2, 3);   // [1, 2, 3]\nArray.of(undefined); // [undefined]\n</code></pre>\n\n<p id=\"Compatibility\"><strong>兼容旧环境</strong></p>\n\n<p>如果原生不支持的话，在其他代码之前执行以下代码会创建&nbsp;<code>Array.of()</code>&nbsp;。</p>\n\n<pre>\n<code>if (!Array.of) {\n  Array.of = function() {\n    return Array.prototype.slice.call(arguments);\n  };\n}</code></pre>\n\n<p><strong>&nbsp;arguments中是什么内容呢？显示的属性：实参的每项；隐式的属性：length、callee、Symbol</strong></p>\n\n<p><strong>所以调用数组的slice把实参的项切割出来。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">if (!Array.myof) {\n  Array.myof = function() {\n      console.log(arguments)\n    return Array.prototype.slice.call(arguments);\n  };\n}\nArray.myof(7);</code></pre>\n\n<p><img alt=\"\" height=\"104\" src=\"https://img-blog.csdnimg.cn/20200829110353353.png\" width=\"406\" /></p>\n\n<p><strong>15、Array.from方法 -- ES6</strong></p>\n\n<p><code><strong>Array.from()</strong></code>&nbsp;方法从一个类似数组或可迭代对象创建一个新的，浅拷贝的数组实例。</p>\n\n<p><code>Array.from()</code>&nbsp;可以通过以下方式来创建数组对象：</p>\n\n<ul>\n\t<li>伪数组对象（拥有一个&nbsp;<code>length</code>&nbsp;属性和若干索引属性的任意对象）</li>\n\t<li><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/iterable\">可迭代对象</a>（可以获取对象中的元素,如 Map和 Set 等）</li>\n</ul>\n\n<p><code>Array.from()</code>&nbsp;方法有一个可选参数&nbsp;<code>mapFn</code>，让你可以在最后生成的数组上再执行一次&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/map\"><code>map</code></a>&nbsp;方法后再返回。也就是说<code>&nbsp;Array.from(obj, mapFn)&nbsp;</code>就相当于<code>Array.from(obj).map(mapFn)。</code></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcd\";\nArray.from(str, function(item, index){\n    console.log(item);\n})</code></pre>\n\n<p><code>比如：</code></p>\n\n<p id=\"从_String_生成数组\"><strong>从&nbsp;<code>String</code>&nbsp;生成数组</strong></p>\n\n<pre>\n<code>Array.from('foo'); \n// [ \"f\", \"o\", \"o\" ]</code></pre>\n\n<p id=\"从_Set_生成数组\"><strong>从&nbsp;<code>Set</code>&nbsp;生成数组</strong></p>\n\n<pre>\n<code>const set = new Set(['foo', 'bar', 'baz', 'foo']);\nArray.from(set);\n// [ \"foo\", \"bar\", \"baz\" ]</code></pre>\n\n<p id=\"从_Map_生成数组\"><strong>从&nbsp;<code>Map</code>&nbsp;生成数组</strong></p>\n\n<pre>\n<code>const map = new Map([[1, 2], [2, 4], [4, 8]]);\nArray.from(map);\n// [[1, 2], [2, 4], [4, 8]]\n\nconst mapper = new Map([['1', 'a'], ['2', 'b']]);\nArray.from(mapper.values());\n// ['a', 'b'];\n\nArray.from(mapper.keys());\n// ['1', '2'];\n</code></pre>\n\n<p id=\"从类数组对象（arguments）生成数组\"><strong>从类数组对象（arguments）生成数组</strong></p>\n\n<pre>\n<code>function f() {\n  return Array.from(arguments);\n}\n\nf(1, 2, 3);\n\n// [ 1, 2, 3 ]</code></pre>\n\n<p id=\"在_Array.from_中使用箭头函数\"><strong>在&nbsp;<code>Array.from</code>&nbsp;中使用箭头函数</strong></p>\n\n<pre>\n<code>// Using an arrow function as the map function to\n// manipulate the elements\nArray.from([1, 2, 3], x =&gt; x + x);\n// [2, 4, 6]\n\n\n// Generate a sequence of numbers\n// Since the array is initialized with `undefined` on each position,\n// the value of `v` below will be `undefined`\nArray.from({length: 5}, (v, i) =&gt; i);\n// [0, 1, 2, 3, 4]</code></pre>\n\n<p><strong>16、find方法和findIndex方法&nbsp; &nbsp;ES6 兼容性Edge12</strong></p>\n\n<p>find() 方法返回通过测试（函数内判断）的数组的第一个元素的值。</p>\n\n<p>find() 方法为数组中的每个元素都调用一次函数执行：</p>\n\n<ul>\n\t<li>当数组中的元素在测试条件时返回&nbsp;<em>true</em>&nbsp;时, find() 返回符合条件的元素，之后的值不会再调用执行函数。</li>\n\t<li>如果没有符合条件的元素返回 undefined</li>\n</ul>\n\n<p><strong>注意:</strong>&nbsp;<strong>find() 对于空数组，函数是不会执行的。</strong></p>\n\n<p><strong>注意:</strong>&nbsp;<strong>find() 并没有改变数组的原始值。</strong></p>\n\n<p>MDN解释：<code><strong>find()</strong></code>&nbsp;方法返回数组中满足提供的测试函数的第一个元素的值。否则返回&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/undefined\"><code>undefined</code></a>。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex\"><code>findIndex()</code></a>&nbsp;方法，它返回数组中找到的元素的索引，而不是其值。</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [1, 2, 3, 4, 5];\n        let item = arr.find((item, index) =&gt; {\n            return item &gt; 3 &amp;&amp; index &gt; 3;\n        })\n        console.log(item);   5</code></pre>\n\n<p>特点：遍历去找，找到<strong>符合条件</strong>的就返回该值，停止遍历。</p>\n\n<p><strong>find方法和findIndex方法也是迭代方法，find返回为true的项，findIndex返回为true的index。同样找到就返回，且停止查找。</strong></p>\n\n<p><strong>而indexOf和lastIndexOf是找指定的项，而不是指定的条件。</strong></p>\n\n<h3>问答：</h3>\n\n<p><strong>类数组转数组的方法有：</strong></p>\n\n<p>（1）Array.prototype.slice.call(arguments)</p>\n\n<p>（2）Array.from(arguments)</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "javascript",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106817618"
        },
        //对象
        object_property: {
            "article_id": "106854845",
            "title": "数据属性和访问器属性——Object.defineProperty()方法",
            "description": "一、数据属性\n\n通常我们给对象设置一个属性，configurable 、 enumerable和 writable都是true。在调用 Object.defineProperty() 方法时，如果不指定， configurable 、 enumerable和 writable 特性的默认值都是 false。\n\n拥有布尔值的键configurable、enumerable和writable的默认值都是false。\n\t属性值和函数的键value、get和set字段的默认值为undefined...",
            "content": "<h3>一、数据属性</h3>\n\n<p><strong>Object.defineProperty()</strong>方法会直接在一个对象上<strong>定义一个新属性</strong>，或者<strong>修改一个对象的现有属性</strong>，并<strong>返回此对象</strong>。</p>\n\n<p>通常我们给对象设置一个属性，configurable 、 enumerable和 writable都是true。</p>\n\n<p>在调用 Object.defineProperty() 方法<strong>设置属性</strong>时，如果不指定， configurable 、 enumerable和 writable 特性的默认值都是 false。</p>\n\n<p>当用Object.defineProperty（）<strong>设置属性</strong>的时候：</p>\n\n<ul>\n\t<li>拥有布尔值的键&nbsp;<code>configurable</code>、<code>enumerable</code>&nbsp;和&nbsp;<code>writable</code>&nbsp;的默认值都是&nbsp;<code>false</code>。</li>\n\t<li>属性值和函数的键&nbsp;<code>value</code>、<code>get</code>&nbsp;和&nbsp;<code>set</code>&nbsp;字段的默认值为&nbsp;<code>undefined</code>。</li>\n</ul>\n\n<p>比如：设置属性值不可以被修改</p>\n\n<pre>\n<code class=\"language-javascript\">const object1 = {};\n\nObject.defineProperty(object1, 'property1', {\n  value: 42,\n  writable: false\n});\n\nobject1.property1 = 77;\n// throws an error in strict mode\n\nconsole.log(object1.property1);\n// expected output: 42</code></pre>\n\n<p>但是在调用Object.defineProperty() 方法<strong>修改属性</strong>时，指定修改属性的哪个描述值，而不会影响未被修改的描述值。</p>\n\n<h3><strong>1、configurable</strong></h3>\n\n<p><strong>特点：</strong></p>\n\n<p><strong>值为false，不允许删除该属性；不允许修改enumerable；允许writable从true改为false，不允许writable从false改为true；不管理value。（理解为控制力逐渐衰弱）</strong></p>\n\n<p><strong>值为true，允许修改其他值，也可以修改configurable本身为false，当为false时，不可以修改configurable为true。</strong></p>\n\n<p><strong>违反configurable特性的操作，都要报错。修改value值不管成功与否，都不报错，因为configurable不控制value。</strong></p>\n\n<p>当我们使用Object.defineProperty()方法时，依赖于对象的该属性的自身的设置情况。</p>\n\n<p>比如下面实例中，同时修改属性的writable和value值，修改writable值要依赖属性修改前的configurable值；修改value的值，要依赖属性修改前的writable值。</p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name: \"obj\"};\n        Object.defineProperty(obj,'age',{\n            configurable: false,\n            enumerable: true,\n            writable: true,\n            value: \"24\"\n        });\n        Object.defineProperty(obj,'age',{\n            writable: false,\n            value: \"26\"\n        });\n        obj.age = 25\n\n        console.log(obj.age);--26</code></pre>\n\n<p>&nbsp;上面obj.age = 25修改属性值，无效，但不会报错。但是当writable为false的后，再通过defineProperty设置value，报错</p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name: \"obj\"};\n        Object.defineProperty(obj,'age',{\n            configurable: false,\n            enumerable: true,\n            value: \"24\",\n            writable: true\n        });\n        Object.defineProperty(obj,'age',{\n            value: \"26\",\n            writable: false\n        });\n        报错\n        Object.defineProperty(obj,'age',{\n            value: \"24\"\n        });</code></pre>\n\n<p><strong>2、enumerable</strong></p>\n\n<p><strong>特点：enumerable表明该属性是显式的还是隐式的，值为true表明属性为显式，值为false表明属性为隐式的</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name: \"zhu\", age: 24}\n        Object.defineProperty(obj, \"age\", {\n            enumerable: false\n        })\n        console.log(obj)</code></pre>\n\n<p>&nbsp;结果：注意看name属性和age属性的颜色</p>\n\n<p><img alt=\"\" height=\"74\" src=\"https://img-blog.csdnimg.cn/20201021143735384.png\" width=\"188\" /></p>\n\n<p><strong><code>enumerable</code>&nbsp;定义了对象的属性是否可以在&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/for...in\"><code>for...in</code></a>&nbsp;循环和&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/keys\"><code>Object.keys()</code></a>&nbsp;中被枚举。</strong></p>\n\n<p>该键值为false时，该属性不可被枚举，<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/for...in\"><code>for...in</code></a>&nbsp;或&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/keys\"><code>Object.keys</code></a><a href=\"https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Object/keys\">&nbsp;</a>方法不能枚举到该属性。</p>\n\n<p>&nbsp;</p>\n\n<p><strong>for in遍历</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name: \"obj\"};\n        Object.defineProperty(obj,'age',{\n            configurable: true,\n            enumerable: false,\n            value: \"24\",\n            writable: true\n        });\n        var arr = [];\n        for(prop in obj){\n            arr.push(prop)\n        }\n        \n        console.log(arr); 结果：[\"name\"]</code></pre>\n\n<p><strong>&nbsp;<code>Object.keys</code>&nbsp;</strong>返回一个<strong>所有元素为字符串的数组</strong>，其元素来自于从给定的<code>object</code>上面<strong>可直接枚举的属性</strong>。这些属性的顺序与手动遍历该对象属性时的一致。</p>\n\n<p><strong>由对象或数组的可枚举的keys值构成的数组（每项都是字符串）。数组的keys值是数组的索引，对象的keys值是对象的属性。</strong></p>\n\n<p>例如：</p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name:\"zhu\",age: 24,nb:true};\n        var arr = [\"str\",1,false,[0]];\n        console.log(Object.keys(obj))  [\"name\", \"age\", \"nb\"]\n        console.log(Object.keys(arr));  [\"0\", \"1\", \"2\", \"3\"]</code></pre>\n\n<p>&nbsp;补充：如果你想获取一个对象的所有属性,，甚至包括不可枚举的，请查看<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames\"><code>Object.getOwnPropertyNames</code></a>。</p>\n\n<p><strong>&nbsp;<code>Object.keys</code>&nbsp;</strong>遍历</p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name: \"obj\"};\n        Object.defineProperty(obj,'age',{\n            configurable: true,\n            enumerable: false,\n            value: \"24\",\n            writable: true\n        });\n        console.log(Object.keys(obj));  [\"name\"]</code></pre>\n\n<p><strong>3、writable</strong></p>\n\n<p><strong>writable控制value是否可以被修改，如果writable为true，value值可以修改；如果writable为false，value值不可以被修改。</strong></p>\n\n<p><strong>当&nbsp;<code>writable</code>&nbsp;属性设置为&nbsp;<code>false</code>&nbsp;时，该属性被称为&ldquo;不可写的&rdquo;，普通方式 对象.属性 修改无效</strong>，<strong>defineProperty修改会报错。&nbsp;</strong></p>\n\n<p><strong>4、value</strong></p>\n\n<p>value值的设置就是属性值。</p>\n\n<h3>二、拓展知识：</h3>\n\n<p><strong>1、Object.defineProperties() 定义多个属性：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {}\n        Object.defineProperties(obj,{\n            name:{\n                configurable: true,\n                enumerable: true,\n                writable: true,\n                value:\"zhu\"\n            },\n            age: {\n                writable: true,\n                value:24\n            }\n        })\n        console.log(obj); </code></pre>\n\n<p><img alt=\"\" height=\"79\" src=\"https://img-blog.csdnimg.cn/20201021152154110.png\" width=\"186\" /></p>\n\n<p><strong><code>2、Object.getOwnPropertyDescriptor()</code></strong>&nbsp;方法返回指定对象上一个自有属性对应属性的<strong>描述符对象</strong>。（自有属性指的是直接赋予该对象的属性，不需要从原型链上进行查找的属性）&nbsp;</p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name: \"zhu\", age: 24}\n        Object.defineProperty(obj, \"age\", {\n            enumerable: false\n        })\n        var descriptor = Object.getOwnPropertyDescriptor(obj, \"age\")\n        console.log(descriptor);</code></pre>\n\n<p><img alt=\"\" height=\"105\" src=\"https://img-blog.csdnimg.cn/20201021150920497.png\" width=\"485\" /></p>\n\n<p><strong>3、<code>Object.getOwnPropertyDescriptors()</code>&nbsp;方法用来获取一个对象的所有自身属性的描述符。</strong></p>\n\n<p>使用：Object.getOwnPropertyDescriptors(obj)</p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {}\n        Object.defineProperties(obj,{\n            name:{\n                configurable: true,\n                enumerable: true,\n                writable: true,\n                value:\"zhu\"\n            },\n            age: {\n                writable: true,\n                value:24\n            }\n        })\n        console.log(Object.getOwnPropertyDescriptors(obj)); </code></pre>\n\n<p><img alt=\"\" height=\"232\" src=\"https://img-blog.csdnimg.cn/2020102115240533.png\" width=\"208\" /></p>\n\n<h3>二、访问器属性</h3>\n\n<p>1、访问器属性：包含getter和setter函数。读取访问器属性时，调用getter函数，返回有效的值；在写入访问器属性时，调用setter函数传入新值。它包含了4个特性：</p>\n\n<ol>\n\t<li>\n\t<p>[[Configurable]]：表示是否能通过delete删除属性从而重新定义属性，能否修改属性的特性，能否把属性修改为访问器属性。</p>\n\t</li>\n\t<li>\n\t<p>[[Enumerable]]：表示能否用for-in循环或Object.keys()返回。</p>\n\t</li>\n\t<li>\n\t<p>[[Get]]：读取属性时调用的函数，默认undefined。</p>\n\t</li>\n\t<li>\n\t<p>[[Set]]：写入属性时调用的函数，默认undefined。</p>\n\t</li>\n</ol>\n\n<p><strong>实际上，writable、value和get、set属性不能共存，这正是区分数据属性和访问器属性的根本所在。</strong></p>\n\n<p><strong>注意：get和set函数中使用的变量，要在Object.defineProperty外声明。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name: \"zhu\"};\n        Object.defineProperty(obj, \"age\", {\n            // 使用了方法名称缩写（ES2015 特性）\n            // 下面两个缩写等价于：\n            // get : function() { return bValue; },\n            // set : function(newValue) { bValue = newValue; },\n            get() {\n                console.log(\"getter\");打印getter\n                return bValue;\n            },\n            set(newValue) {\n                console.log(\"setter\");打印setter\n                bValue = newValue;\n            },\n            enumerable: true,\n            configurable: true\n        })\n        obj.age = 24; 这一步走的就是setter\n        console.log(obj.age); 这一步走的是getter</code></pre>\n\n<p><strong>2、关于继承</strong></p>\n\n<p>如果访问器属性在原型上，<strong>通过实例对象</strong>，也可以<strong>修改原型</strong>上的<strong>访问器属性。（普通属性和数据属性是不可以通过实例修改原型的属性值。当执行修改操作时，实际上是在实例上添加设置该属性，而非在原型上）</strong></p>\n\n<pre>\n<code class=\"language-javascript\">function myclass() {\n}\n\nvar value;\nObject.defineProperty(myclass.prototype, \"x\", {\n  get() {\n    return value;\n  },\n  set(x) {\n    value = x;\n  }\n});\n\nvar a = new myclass();\nvar b = new myclass();\na.x = 1;\nconsole.log(b.x); // 1</code></pre>\n\n<p><strong>3、关于this</strong></p>\n\n<p>切记访问器属性中的get和set函数中的this是&ldquo;谁的属性，this是谁&rdquo;</p>\n\n<pre>\n<code class=\"language-javascript\">        function myclass() {}\n\n        Object.defineProperty(myclass.prototype, \"x\", {\n            get() {\n                return this.stored_x;\n            },\n            set(x) {\n                this.stored_x = x;\n            }\n        });\n\n        var a = new myclass();\n        var b = new myclass();\n        a.x = 1; 在a对象上添加了stored_x属性，且值为x 1\n        console.log(myclass.prototype.x);  获取的时候，获取的是原型上的stored_x</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "javascript",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106854845"
        },
        object_method1: {
            "article_id": "106859332",
            "title": "Object构造函数（一）",
            "description": "Object.prototype\n\n可以为所有 Object 类型的对象添加属性，对象原型，上面有封装好的方法。可以供所有的对象继承使用。\n\n\n\n1、\n\nObject.defineProperty()\n\n给对象添加一个属性并指定该属性的配置。\n\nObject.defineProperties()\n\n给对象添加多个属性并分别指定它们的配置。\n\n2、\n\nObject.keys()\n\n返回一个包含所有给定对象自身可枚举属性名称的数组。\n\nObject.values()\n\n返回给定对象自身可枚举值的数组。\n\n\n  ",
            "content": "<h2>Object构造函数</h2>\n\n<p>JavaScript中的所有对象都来自&nbsp;<code>Object</code>；所有对象从<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/prototype\"><code>Object.prototype</code></a>继承方法和属性，尽管它们可能被覆盖。例如，其他构造函数的原型将覆盖&nbsp;<code>constructor</code>&nbsp;属性并提供自己的&nbsp;<code>toString()</code>&nbsp;方法。<code>Object</code>&nbsp;原型对象的更改将传播到所有对象，除非受到这些更改的属性和方法将沿原型链进一步覆盖。</p>\n\n<p><img alt=\"\" height=\"688\" src=\"https://img-blog.csdnimg.cn/20200620110743495.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"1163\" /></p>\n\n<p><strong><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/prototype\"><code>Object.prototype</code></a></strong></p>\n\n<p>可以为所有 Object 类型的对象添加属性，对象原型，上面有封装好的方法。可以供所有的对象继承使用。</p>\n\n<p><img alt=\"\" height=\"270\" src=\"https://img-blog.csdnimg.cn/20200619164057780.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"428\" /></p>\n\n<p><strong>（1）Object.prototype.constructor </strong></p>\n\n<p>构造函数 指向Object</p>\n\n<p><strong>（2）Object.prototype.hasOwnProperty </strong></p>\n\n<p>判断是否是自身的属性，所有对象继承该属性，对象可以直接调用hasOwnProperty，来判断对象上是否有某个属性，比如：</p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {name: \"zhu\"};\n        console.log(obj.hasOwnProperty(\"name\")); true</code></pre>\n\n<p>可以结合for in，来遍历获取对象自身的属性和属性值。</p>\n\n<p>&nbsp;<strong>（3）Object.prototype.toString方法</strong></p>\n\n<p>对象原型的该toString方法可以来识别数据类型，而且每个对象也继承了该toString方法，该方法的详细介绍在&ldquo;数据类型的toString&rdquo;有介绍</p>\n\n<p><strong>1、ES5的方法，兼容IE9</strong></p>\n\n<p>这两个方法，在&ldquo;数据属性和访问器属性&rdquo;中有介绍</p>\n\n<p><code>（1）<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty\">Object.defineProperty()</a></code></p>\n\n<p>给对象添加一个属性并指定该属性的配置。</p>\n\n<p><code>（2）<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperties\">Object.defineProperties()</a></code></p>\n\n<p>给对象添加多个属性并分别指定它们的配置。</p>\n\n<p><strong><code>（3）Object.getOwnPropertyDescriptor()</code></strong>&nbsp;方法返回指定对象上一个自有属性对应的属性描述符</p>\n\n<p>如果指定的属性存在于对象上，则返回其属性描述符对象（property descriptor），否则返回&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/undefined\"><code>undefined</code></a>。</p>\n\n<p>比如，</p>\n\n<p><strong>正常的对象属性</strong></p>\n\n<pre>\n<code class=\"language-javascript\">o = { bar: 42 };\nd = Object.getOwnPropertyDescriptor(o, \"bar\");\n d {\n   configurable: true,\n   enumerable: true,\n   value: 42,\n   writable: true\n }</code></pre>\n\n<p><strong>&nbsp;通过defineProperty设置的对象属性</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        o = {};\n        Object.defineProperty(o, \"baz\", {\n            value: 8675309,\n            writable: false,\n            enumerable: false\n        });\n        d = Object.getOwnPropertyDescriptor(o, \"baz\");\n        d = {\n            value: 8675309,\n            writable: false,\n            enumerable: false,\n            configurable: false\n        }</code></pre>\n\n<p><strong>2、获取对象自身属性或值组成数组</strong></p>\n\n<p><code><strong>（1）<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/keys\">Object.keys()</a></strong>&nbsp;<strong>兼容新ES5 IE9</strong></code></p>\n\n<p>返回一个包含所有给定<strong>对象自身</strong>可枚举属性名称的数组。</p>\n\n<p>数组返回索引值组成的数组：</p>\n\n<pre>\n<code class=\"language-javascript\">var arr = ['a', 'b', 'c'];\nconsole.log(Object.keys(arr));  console: ['0', '1', '2']</code></pre>\n\n<p>对象返回自身可枚举的属性组成的数组：</p>\n\n<pre>\n<code class=\"language-javascript\">var obj = { 0: 'a', 1: 'b', 2: 'c' };\nconsole.log(Object.keys(obj));  console: ['0', '1', '2']\n\nvar anObj = { 100: 'a', 2: 'b', 7: 'c' };\nconsole.log(Object.keys(anObj));  console: ['2', '7', '100']</code></pre>\n\n<p>在ES5里，如果此方法的参数不是对象（而是一个原始值），那么它会抛出&nbsp;TypeError。在ES6中，非对象的参数将被强制转换为一个对象&nbsp;</p>\n\n<pre>\n<code class=\"language-javascript\">Object.keys(\"foo\");\nTypeError: \"foo\" is not an object (ES5 code)\n\nObject.keys(\"foo\");\n[\"0\", \"1\", \"2\"]                   (ES2015 code)</code></pre>\n\n<p><strong><code>（2）<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/values\">Object.values()</a>&nbsp;</code>兼容性：Edge 14&nbsp;</strong></p>\n\n<p>返回给定<strong>对象自身</strong>可枚举值的数组。</p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {\n            name: \"zhu\",\n            age: 24\n        }\n        obj.__proto__ = {\n            protoName: \"p\"\n        }\n        console.log(Object.values(obj));  [\"zhu\", 24]\n        \n        var arr = [];\n        for(prop in obj){\n            arr.push(obj[prop]);\n        }\n        console.log(arr);  [\"zhu\", 24, \"p\"]</code></pre>\n\n<p>也可以遍历数组，获取数组的值组成的数组，其实就是数组本身。</p>\n\n<p>也可以遍历字符串，把字符串作为数组遍历得到值，组成的数组。</p>\n\n<pre>\n<code class=\"language-javascript\">console.log(Object.values('foo')); // ['f', 'o', 'o']</code></pre>\n\n<p><code><strong>（3）<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/entries\">Object.entries()</a></strong>&nbsp;</code><strong>兼容性：Edge14</strong></p>\n\n<p>返回给定<strong>对象自身</strong>可枚举属性的&nbsp;<code>[key, value]</code>&nbsp;数组。</p>\n\n<p><code>Object.entries()</code>返回一个数组，其元素是与直接在<code>object</code>上找到的可枚举属性键值对相对应的数组。属性的顺序与通过手动循环对象的属性值所给出的顺序相同。</p>\n\n<pre>\n<code class=\"language-javascript\">const obj = { foo: 'bar', baz: 42 };\nconsole.log(Object.entries(obj)); // [ ['foo', 'bar'], ['baz', 42] ]\n\n// array like object\nconst obj = { 0: 'a', 1: 'b', 2: 'c' };\nconsole.log(Object.entries(obj)); // [ ['0', 'a'], ['1', 'b'], ['2', 'c'] ]\n\n// array like object with random key ordering\nconst anObj = { 100: 'a', 2: 'b', 7: 'c' };\nconsole.log(Object.entries(anObj)); // [ ['2', 'b'], ['7', 'c'], ['100', 'a'] ]\n\n// getFoo is property which isn't enumerable\nconst myObj = Object.create({}, { getFoo: { value() { return this.foo; } } });\nmyObj.foo = 'bar';\nconsole.log(Object.entries(myObj)); // [ ['foo', 'bar'] ]\n\n// non-object argument will be coerced to an object\nconsole.log(Object.entries('foo')); // [ ['0', 'f'], ['1', 'o'], ['2', 'o'] ]\n\n// iterate through key-value gracefully\nconst obj = { a: 5, b: 7, c: 9 };\nfor (const [key, value] of Object.entries(obj)) {\n  console.log(`${key} ${value}`); // \"a 5\", \"b 7\", \"c 9\"\n}\n\n// Or, using array extras\nObject.entries(obj).forEach(([key, value]) =&gt; {\nconsole.log(`${key} ${value}`); // \"a 5\", \"b 7\", \"c 9\"\n});</code></pre>\n\n<p>Polyfill&nbsp; 支持IE9以上：</p>\n\n<pre>\n<code class=\"language-javascript\">if (!Object.entries)\n  Object.entries = function( obj ){\n    var ownProps = Object.keys( obj ),\n        i = ownProps.length,\n        resArray = new Array(i); // preallocate the Array\n    while (i--)\n      resArray[i] = [ownProps[i], obj[ownProps[i]]];\n    \n    return resArray;\n  };</code></pre>\n\n<p>若想支持IE8，替换上面Object.keys</p>\n\n<p><strong>&nbsp;（4）Object.getOwnPropertySymbols() 兼容性：ES6&nbsp; Edge12</strong></p>\n\n<p>方法返回一个给定对象自身的所有 Symbol&nbsp;属性的数组。</p>\n\n<p>与<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames\"><code>Object.getOwnPropertyNames()</code></a>类似，您可以将给定对象的所有符号属性作为 Symbol&nbsp;数组获取。 请注意，<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames\"><code>Object.getOwnPropertyNames()</code></a>本身不包含对象的 Symbol&nbsp;属性，只包含字符串属性。因为所有的对象在初始化的时候不会包含任何的 Symbol，除非你在对象上赋值了 Symbol&nbsp;否则<code>Object.getOwnPropertySymbols()</code>只会返回一个空的数组。</p>\n\n<pre>\n<code class=\"language-javascript\">var obj = {};\nvar a = Symbol(\"a\");\nvar b = Symbol.for(\"b\");\n\nobj[a] = \"localSymbol\";\nobj[b] = \"globalSymbol\";\n\nvar objectSymbols = Object.getOwnPropertySymbols(obj);\n\nconsole.log(objectSymbols.length); // 2\nconsole.log(objectSymbols)         // [Symbol(a), Symbol(b)]\nconsole.log(objectSymbols[0])      // Symbol(a)</code></pre>\n\n<p><strong>（5）Object.getOwnPropertyNames() 兼容性ES5&nbsp;&nbsp;IE9</strong></p>\n\n<p>返回一个由指定对象的<strong>所有自身属性的属性名</strong>（包括不可枚举属性但不包括Symbol值作为名称的属性）组成的数组</p>\n\n<p>该数组对元素是&nbsp;<code>obj</code>自身拥有的枚举或不可枚举属性名称字符串。&nbsp;数组中枚举属性的顺序与通过&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/for...in\"><code>for...in</code></a>&nbsp;循环（或&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/keys\"><code>Object.keys</code></a>）迭代该对象属性时一致。<strong>数组中不可枚举属性的顺序未定义</strong>。</p>\n\n<p>数组：数组的顺序 为定义</p>\n\n<pre>\n<code class=\"language-javascript\">var arr = [\"a\", \"b\", \"c\"];\nconsole.log(Object.getOwnPropertyNames(arr).sort());  [\"0\", \"1\", \"2\", \"length\"]</code></pre>\n\n<p>类数组：</p>\n\n<pre>\n<code class=\"language-javascript\">        function foo(){\n            console.log(arguments);\n            console.log(Object.getOwnPropertyNames(arguments))\n        }\n        foo(\"zhu\",true,3,5);</code></pre>\n\n<p>结果：</p>\n\n<p><img alt=\"\" height=\"301\" src=\"https://img-blog.csdnimg.cn/20200620135133533.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"525\" /></p>\n\n<p>对象：</p>\n\n<pre>\n<code class=\"language-javascript\">var my_obj = Object.create({}, {\n  getFoo: {\n    value: function() { return this.foo; },\n    enumerable: false\n  }\n});\nmy_obj.foo = 1;\n\nconsole.log(Object.getOwnPropertyNames(my_obj).sort());  [\"foo\", \"getFoo\"]</code></pre>\n\n<p>如果你只要获取到可枚举属性，查看<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/keys\"><code>Object.keys</code></a>或用<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/for...in\"><code>for...in</code></a>循环（还会获取到原型链上的可枚举属性，不过可以使用<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/hasOwnProperty\"><code>hasOwnProperty()</code></a>方法过滤掉）&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "javascript",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106859332"
        },
        object_method2: {
            "article_id": "106871487",
            "title": "Object构造函数（二）",
            "description": "3、对象的设置\n\n（1）Object.isExtensible()和Object.preventExtensions() ES5\n\nObject.isExtensible()方法判断一个对象是否是可扩展的（是否可以在它上面添加新的属性）。返回一个Boolean值。\n\n默认情况下，对象是可扩展的：即可以为他们添加新的属性。以及它们的__proto__属性可以被更改。Object.preventExtensions，Object.seal或Object.freeze方法都可以标记一个对象为不可扩展（...",
            "content": "<p><strong>3、对象的设置</strong></p>\n\n<p><strong>（1）Object.isExtensible()和Object.preventExtensions() ES5</strong></p>\n\n<p><code><strong>Object.isExtensible()</strong></code>&nbsp;方法判断一个对象是否是可扩展的（是否可以在它上面添加新的属性）。返回一个Boolean值。</p>\n\n<p>默认情况下，对象是可扩展的：即可以为他们添加新的属性。以及它们的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/proto\"><code>__proto__</code></a>&nbsp;属性可以被更改。<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/preventExtensions\"><code>Object.preventExtensions</code></a>，<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/seal\"><code>Object.seal</code></a>&nbsp;或&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze\"><code>Object.freeze</code></a>&nbsp;方法都可以标记一个对象为不可扩展（non-extensible）</p>\n\n<pre>\n<code class=\"language-javascript\"> 新对象默认是可扩展的.\nvar empty = {};\nObject.isExtensible(empty);  === true\n\n ...可以变的不可扩展.\nObject.preventExtensions(empty);\nObject.isExtensible(empty);  === false\n\n 密封对象是不可扩展的.\nvar sealed = Object.seal({});\nObject.isExtensible(sealed);  === false\n\n冻结对象也是不可扩展.\nvar frozen = Object.freeze({});\nObject.isExtensible(frozen);  === false</code></pre>\n\n<p>注意：</p>\n\n<p>在 ES5 中，如果参数不是一个对象类型，将抛出一个&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError\"><code>TypeError</code></a>&nbsp;异常。</p>\n\n<p>在 ES6 中， non-object 参数将被视为一个不可扩展的普通对象，因此会返回 false 。&nbsp;</p>\n\n<pre>\n<code class=\"language-javascript\">Object.isExtensible(1);\nTypeError: 1 is not an object (ES5 code)\n\nObject.isExtensible(1);\nfalse                         (ES6 code)</code></pre>\n\n<p>&nbsp;<code><strong>Object.preventExtensions()</strong></code>方法让一个对象变的不可扩展，也就是永远不能再添加新的属性。返回已经不可扩展的该对象。</p>\n\n<p>如果一个对象可以添加新的属性，则这个对象是可扩展的。<code>Object.preventExtensions()</code>将对象标记为不再可扩展，这样它将永远不会具有它被标记为不可扩展时持有的属性之外的属性。注意，一般来说，不可扩展对象的属性可能仍然可被<em>删除</em>。尝试将新属性添加到不可扩展对象将静默失败或抛出<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError\"><code>TypeError</code></a>（最常见的情况是<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions_and_function_scope/Strict_mode\">strict mode</a>中，但不排除其他情况）。</p>\n\n<p><code>Object.preventExtensions()</code>仅阻止添加自身的属性。但其对象类型的原型依然可以添加新的属性。</p>\n\n<p>该方法使得目标对象的&nbsp;<code>[[prototype]]</code>&nbsp; 不可变；任何重新赋值&nbsp;<code>[[prototype]]</code>&nbsp;操作都会抛出&nbsp;<code>TypeError</code>&nbsp;。这种行为只针对内部的&nbsp;<code>[[prototype]]</code>&nbsp;属性， 目标对象的其它属性将保持可变。</p>\n\n<p>一旦将对象变为不可扩展的对象，就再也不能使其可扩展。</p>\n\n<p>在 ES5 中，如果参数不是一个对象类型（而是原始类型），将抛出一个<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError\"><code>TypeError</code></a>异常。在 ES2015 中，非对象参数将被视为一个不可扩展的普通对象，因此会被直接返回。</p>\n\n<p><strong>（2）Object.isFrozen()和Object.freeze()&nbsp; ES5&nbsp; IE9</strong></p>\n\n<p><code><strong>Object.isFrozen()</strong></code>方法判断一个对象是否被<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze\">冻结</a>。返回Boolean值。</p>\n\n<p>一个对象是冻结的是指它不可<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/isExtensible\"><code>扩展</code></a>，所有属性都是不可配置的，且所有数据属性（即没有getter或setter组件的访问器的属性）都是不可写的。</p>\n\n<pre>\n<code>// 一个对象默认是可扩展的,所以它也是非冻结的.\nObject.isFrozen({}); // === false\n\n// 一个不可扩展的空对象同时也是一个冻结对象.\nvar vacuouslyFrozen = Object.preventExtensions({});\nObject.isFrozen(vacuouslyFrozen) //=== true;\n\n// 一个非空对象默认也是非冻结的.\nvar oneProp = { p: 42 };\nObject.isFrozen(oneProp) //=== false\n\n// 让这个对象变的不可扩展,并不意味着这个对象变成了冻结对象,\n// 因为p属性仍然是可以配置的(而且可写的).\nObject.preventExtensions(oneProp);\nObject.isFrozen(oneProp) //=== false\n\n// 此时,如果删除了这个属性,则它会成为一个冻结对象.\ndelete oneProp.p;\nObject.isFrozen(oneProp) //=== true\n\n// 一个不可扩展的对象,拥有一个不可写但可配置的属性,则它仍然是非冻结的.\nvar nonWritable = { e: \"plep\" };\nObject.preventExtensions(nonWritable);\nObject.defineProperty(nonWritable, \"e\", { writable: false }); // 变得不可写\nObject.isFrozen(nonWritable) //=== false\n\n// 把这个属性改为不可配置,会让这个对象成为冻结对象.\nObject.defineProperty(nonWritable, \"e\", { configurable: false }); // 变得不可配置\nObject.isFrozen(nonWritable) //=== true\n\n// 一个不可扩展的对象,拥有一个不可配置但可写的属性,则它仍然是非冻结的.\nvar nonConfigurable = { release: \"the kraken!\" };\nObject.preventExtensions(nonConfigurable);\nObject.defineProperty(nonConfigurable, \"release\", { configurable: false });\nObject.isFrozen(nonConfigurable) //=== false\n\n// 把这个属性改为不可写,会让这个对象成为冻结对象.\nObject.defineProperty(nonConfigurable, \"release\", { writable: false });\nObject.isFrozen(nonConfigurable) //=== true\n\n// 一个不可扩展的对象,值拥有一个访问器属性,则它仍然是非冻结的.\nvar accessor = { get food() { return \"yum\"; } };\nObject.preventExtensions(accessor);\nObject.isFrozen(accessor) //=== false\n\n// ...但把这个属性改为不可配置,会让这个对象成为冻结对象.\nObject.defineProperty(accessor, \"food\", { configurable: false });\nObject.isFrozen(accessor) //=== true\n\n// 使用Object.freeze是冻结一个对象最方便的方法.\nvar frozen = { 1: 81 };\nObject.isFrozen(frozen) //=== false\nObject.freeze(frozen);\nObject.isFrozen(frozen) //=== true\n\n// 一个冻结对象也是一个密封对象.\nObject.isSealed(frozen) //=== true\n\n// 当然,更是一个不可扩展的对象.\nObject.isExtensible(frozen) //=== false</code></pre>\n\n<p>&nbsp;在 ES5 中，如果参数不是一个对象类型，将抛出一个<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError\"><code>TypeError</code></a>异常。在 ES2015 中，非对象参数将被视为一个冻结的普通对象，因此会返回<code>true</code>。</p>\n\n<p><strong><code>Object.freeze()</code></strong>&nbsp;方法可以<strong>冻结</strong>一个对象。</p>\n\n<p>一个被冻结的对象再也不能被修改；冻结了一个对象则不能向这个对象添加新的属性，不能删除已有属性，不能修改该对象已有属性的可枚举性、可配置性、可写性，以及不能修改已有属性的值。</p>\n\n<p>此外，冻结一个对象后该对象的原型也不能被修改。<code>freeze()</code>&nbsp;返回和传入的参数相同的对象。<strong>这个方法返回传递的对象，而不是创建一个被冻结的副本。</strong></p>\n\n<p>被冻结对象自身的所有属性都不可能以任何方式被修改。任何修改尝试都会失败，无论是静默地还是通过抛出<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError\"><code>TypeError</code></a>异常（最常见但不仅限于<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Strict_mode\">strict mode</a>）。</p>\n\n<p>数据属性的值不可更改，访问器属性（有getter和setter）也同样（但由于是函数调用，给人的错觉是还是可以修改这个属性）。如果一个<strong>属性的值是个对象</strong>，则<strong>这个属性对象</strong>中的属性是可以修改的，除非它也是个冻结对象。数组作为一种对象，被冻结，其元素不能被修改。没有数组元素可以被添加或移除。</p>\n\n<p><strong>深冻结对象：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">// 深冻结函数.\nfunction deepFreeze(obj) {\n\n  // 取回定义在obj上的属性名\n  var propNames = Object.getOwnPropertyNames(obj);\n\n  // 在冻结自身之前冻结属性\n  propNames.forEach(function(name) {\n    var prop = obj[name];\n\n    // 如果prop是个对象，冻结它\n    if (typeof prop == 'object' &amp;&amp; prop !== null)\n      deepFreeze(prop);\n  });\n\n  // 冻结自身(no-op if already frozen)\n  return Object.freeze(obj);\n}\n\nobj2 = {\n  internal: {}\n};\n\ndeepFreeze(obj2);\nobj2.internal.a = 'anotherValue';\nobj2.internal.a; // undefined</code></pre>\n\n<p>在ES5中，如果这个方法的参数不是一个对象（一个原始值），那么它会导致&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError\"><code>TypeError</code></a>。在ES2015中，非对象参数将被视为要被冻结的普通对象，并被简单地返回。</p>\n\n<p>用<code>Object.seal()</code>密封的对象可以改变它们现有的属性。使用<code>Object.freeze()</code>&nbsp;冻结的对象中现有属性是不可变的</p>\n\n<p><strong>（3）Object.isSealed()和Object.seal()&nbsp; 兼容性ES5 IE9</strong></p>\n\n<p><strong><code>Object.isSealed()</code></strong>&nbsp;方法判断一个对象是否被密封。返回Boolean值。</p>\n\n<p>如果这个对象是密封的，则返回&nbsp;<code>true</code>，否则返回&nbsp;<code>false</code>。密封对象是指那些不可&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/isExtensible\"><code>扩展</code></a>&nbsp;的，且所有自身属性都不可配置且因此不可删除（但不一定是不可写）的对象。</p>\n\n<pre>\n<code>// 新建的对象默认不是密封的.\nvar empty = {};\nObject.isSealed(empty); // === false\n\n// 如果你把一个空对象变的不可扩展,则它同时也会变成个密封对象.\nObject.preventExtensions(empty);\nObject.isSealed(empty); // === true\n\n// 但如果这个对象不是空对象,则它不会变成密封对象,因为密封对象的所有自身属性必须是不可配置的.\nvar hasProp = { fee: \"fie foe fum\" };\nObject.preventExtensions(hasProp);\nObject.isSealed(hasProp); // === false\n\n// 如果把这个属性变的不可配置,则这个对象也就成了密封对象.\nObject.defineProperty(hasProp, \"fee\", { configurable: false });\nObject.isSealed(hasProp); // === true\n\n// 最简单的方法来生成一个密封对象,当然是使用Object.seal.\nvar sealed = {};\nObject.seal(sealed);\nObject.isSealed(sealed); // === true\n\n// 一个密封对象同时也是不可扩展的.\nObject.isExtensible(sealed); // === false\n\n// 一个密封对象也可以是一个冻结对象,但不是必须的.\nObject.isFrozen(sealed); // === true ，所有的属性都是不可写的\nvar s2 = Object.seal({ p: 3 });\nObject.isFrozen(s2); // === false， 属性\"p\"可写\n\nvar s3 = Object.seal({ get p() { return 0; } });\nObject.isFrozen(s3); // === true ，访问器属性不考虑可写不可写,只考虑是否可配置</code></pre>\n\n<p><code><strong>Object.seal()</strong></code>方法封闭一个对象，阻止添加新属性并将所有现有属性标记为不可配置。当前属性的值只要原来是可写的就可以改变。返回被密封的对象，即被密封对象的引用。</p>\n\n<p>通常，一个对象是<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/isExtensible\">可扩展的</a>（可以添加新的属性）。密封一个对象会让这个对象变的不能添加新属性，且所有已有属性会变的不可配置。属性不可配置的效果就是属性变的不可删除，以及一个数据属性不能被重新定义成为访问器属性，或者反之。但属性的值仍然可以修改。因为writable还是原本设置的，可能是true。数据属性的writable由定义时决定，普通的属性writable是true。</p>\n\n<p>尝试删除一个密封对象的属性或者将某个密封对象的属性从数据属性转换成访问器属性都会失败，抛出异常。</p>\n\n<p>不会影响从原型链上继承的属性。但&nbsp;<code><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/proto\">__proto__</a>（该属性还不是在所有浏览器通用的）</code>&nbsp;属性的值也会不能修改。</p>\n",
            "markdowncontent": "",
            "tags": "js",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106871487"
        },
        object_method3 :{
            "article_id": "106871849",
            "title": "Object构造函数（三）",
            "description": "4、对象原型的操作\n\n（1）Object.setPrototypeOf() ES6 IE11\n\n设置对象的原型（即内部 [[Prototype]] 属性）\n\nObject.setPrototypeOf()方法设置一个指定的对象的原型 ( 即, 内部[[Prototype]]属性）到另一个对象或 null。\n\n当调用该方法 的时候，如果对象的[[Prototype]]是不可扩展(通过Object.isExtensible()查看)，就会抛出TypeError异常。如果prototype参数不是一个对...",
            "content": "<p><strong>4、对象原型的操作</strong></p>\n\n<p><strong>（1）Object.setPrototypeOf() ES6&nbsp; IE11</strong></p>\n\n<p>设置对象的原型（即内部 [[Prototype]] 属性）</p>\n\n<p><strong>Object.setPrototypeOf()&nbsp;</strong>方法设置一个指定的对象的原型 ( 即, 内部[[Prototype]]属性）到另一个对象或 &nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null\"><code>null</code></a>。</p>\n\n<p>当调用该方法 的时候，如果对象的[[Prototype]]是不可扩展(通过&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/isExtensible\"><code>Object.isExtensible()</code></a>查看)，就会抛出&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError\"><code>TypeError</code></a>异常。如果<code>prototype</code>参数不是一个对象或者<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null\"><code>null</code></a>(例如，数字，字符串，boolean，或者&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/undefined\"><code>undefined</code></a>)，则什么都不做。否则，该方法将<code>obj</code>的<code>[[Prototype]]</code>修改为新的值。</p>\n\n<p><code>Object.setPrototypeOf()是</code><strong>ECMAScript 6</strong>最新草案中的方法，相对于&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/__proto__\"><code>Object.prototype.__proto__</code></a>&nbsp;，它被认为是修改对象原型更合适的方法。</p>\n\n<p><strong>注意传参，源对象&nbsp; 原型对象。返回修改了原型后的源对象。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj1 = {\n            name: \"obj1\"\n        }\n        var obj = {\n            age: 24\n        }\n        var dict = Object.setPrototypeOf(obj, obj1);\n        console.log(dict.name); obj1</code></pre>\n\n<p><strong>警告:</strong>&nbsp;由于现代 JavaScript 引擎优化属性访问所带来的特性的关系，更改对象的&nbsp;<code>[[Prototype]]</code>在<em><strong>各个</strong></em>浏览器和 JavaScript 引擎上都是一个很慢的操作。其在更改继承的性能上的影响是微妙而又广泛的，这不仅仅限于&nbsp;<code>obj.__proto__ = ...</code>&nbsp;语句上的时间花费，而且可能会延伸到<em><strong>任何</strong></em>代码，那些可以访问<em><strong>任何</strong></em><code>[[Prototype]]</code>已被更改的对象的代码。如果你关心性能，你应该避免设置一个对象的&nbsp;<code>[[Prototype]]</code>。相反，你应该使用&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/create\"><code>Object.create()</code></a>来创建带有你想要的<code>[[Prototype]]</code>的新对象。&nbsp;</p>\n\n<p><strong>（2）Object.getPrototypeOf() ES5&nbsp; IE9</strong></p>\n\n<p><code><strong>Object.getPrototypeOf()</strong></code>&nbsp;方法<strong>返回指定对象的原型</strong>（内部<code>[[Prototype]]</code>属性的值）。如果没有继承属性，则返回&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null\"><code>null</code></a>&nbsp;。</p>\n\n<pre>\n<code>var proto = {};\nvar obj = Object.create(proto);\nObject.getPrototypeOf(obj) === proto; // true\n\nvar reg = /a/;\nObject.getPrototypeOf(reg) === RegExp.prototype; // true</code></pre>\n\n<p><strong>（3）<code>Object.create()</code>方法创建一个新对象并指定对象原型&nbsp; 兼容性：ES5&nbsp; IE9</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {\n            name: \"zhu\",\n            age: 24\n        }\n        var dict = Object.create(obj);\n        console.log(dict);\n\n        </code></pre>\n\n<p><img alt=\"\" height=\"86\" src=\"https://img-blog.csdnimg.cn/20200620145848998.png\" width=\"198\" /></p>\n\n<p><strong>5、Object上的两个工具方法</strong></p>\n\n<p><strong>（1）Object.assign()浅复制对象&nbsp; 兼容性：Edge12&nbsp; ES6</strong></p>\n\n<p><code><strong>Object.assign()</strong></code>&nbsp;方法用于将所有可枚举属性的值从一个或多个源对象复制到目标对象。它将返回目标对象。</p>\n\n<p>如果目标对象中的属性具有相同的键，则属性将被源对象中的属性覆盖。后面的源对象的属性将类似地覆盖前面的源对象的属性。</p>\n\n<p><code>Object.assign</code>&nbsp;方法只会拷贝<strong>源对象自身的并且可枚举的属性</strong>到目标对象。该方法使用源对象的<code>[[Get]]</code>和目标对象的<code>[[Set]]</code>，所以它会调用相关 getter 和 setter。因此，它分配属性，而不仅仅是复制或定义新的属性。如果合并源包含getter，这可能使其不适合将新属性合并到原型中。</p>\n\n<p>如果属性不可写，会引发<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypeError\"><code>TypeError</code></a>，如果在引发错误之前添加了任何属性，则可以更改<code>target</code>对象。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Symbol\"><code>Symbol</code></a>&nbsp;类型的属性都会被拷贝，字符串也会作为一个对象进行拷贝。</p>\n\n<pre>\n<code class=\"language-javascript\">        var sym = Symbol('foo'); //Symbol(foo)\n        var str = \"abc\";\n        var obj = {\n            name: \"zhu\",\n            age: 24,\n            s: sym\n        }\n        var dict = Object.assign({},str,obj);\n        console.log(dict);  {0: \"a\", 1: \"b\", 2: \"c\", name: \"zhu\", age: 24, s: Symbol(foo)}\n        console.log(sym);  Symbol(foo)</code></pre>\n\n<p><code>Object.assign()</code>拷贝的是（可枚举）属性值。假如源值是一个对象的引用，它仅仅会复制其引用值。</p>\n\n<p>异常会打断后续的copy，但前面的copy是成功的，比如一个对象，一部分属性已经copy了，发生异常后，后续停止copy，而前面的属性copy成功。</p>\n\n<p>原始类型会被包装，null 和 undefined 会被忽略。注意，只有字符串的包装对象才可能有自身可枚举属性。</p>\n\n<p><strong>（2）Object.is()判断两个值是否相等&nbsp; Edge12&nbsp; ES6</strong></p>\n\n<p>&nbsp;判断两个值是否<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Equality_comparisons_and_sameness\">相同</a>。如果下列任何一项成立，则两个值相同：</p>\n\n<ul>\n\t<li>两个值都是&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/undefined\"><code>undefined</code></a></li>\n\t<li>两个值都是&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null\"><code>null</code></a></li>\n\t<li>两个值都是&nbsp;<code>true</code>&nbsp;或者都是&nbsp;<code>false</code></li>\n\t<li>两个值是由相同个数的字符按照相同的顺序组成的字符串</li>\n\t<li>两个值指向同一个对象</li>\n\t<li>两个值都是数字并且\n\t<ul>\n\t\t<li>都是正零&nbsp;<code>+0</code></li>\n\t\t<li>都是负零&nbsp;<code>-0</code></li>\n\t\t<li>都是&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a></li>\n\t\t<li>都是除零和&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a>&nbsp;外的其它同一个数字</li>\n\t</ul>\n\t</li>\n</ul>\n\n<p>这种相等性判断逻辑和传统的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Equality\"><code>==</code></a>&nbsp;运算不同，<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Equality\"><code>==</code></a>&nbsp;运算符会对它两边的操作数做隐式类型转换（如果它们类型不同），然后才进行相等性比较，（所以才会有类似&nbsp;<code>&quot;&quot; == false</code>&nbsp;等于&nbsp;<code>true</code>的现象），但&nbsp;<code>Object.is</code>&nbsp;不会做这种类型转换。</p>\n\n<p>这与&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Identity\"><code>===</code></a>&nbsp;运算符的判定方式也不一样。<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Identity\"><code>===</code></a>&nbsp;运算符（和<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Equality\"><code>==</code></a>&nbsp;运算符）将数字值&nbsp;<code>-0</code>&nbsp;和&nbsp;<code>+0</code>&nbsp;视为相等，并认为&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number/NaN\"><code>Number.NaN</code></a>&nbsp;不等于&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a>。</p>\n\n<p>兼容性：Edge12</p>\n\n<p>兼容性写法：</p>\n\n<pre>\n<code class=\"language-javascript\">if (!Object.is) {\n  Object.is = function(x, y) {\n    // SameValue algorithm\n    if (x === y) { // Steps 1-5, 7-10\n      // Steps 6.b-6.e: +0 != -0\n      return x !== 0 || 1 / x === 1 / y;\n    } else {\n      // Step 6.a: NaN == NaN\n      return x !== x &amp;&amp; y !== y;\n    }\n  };\n}</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "javascript",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106871849"
        },
        object_inherit: {
            "article_id": "106895850",
            "title": "继承与原型链",
            "description": "在 ES2015/ES6 中引入了class关键字，但那只是语法糖，JavaScript 仍然是基于原型的，它是动态的。\n\n继承：每个实例对象（ object ）都有一个私有属性（称之为 __proto__ 兼容性：IE11，不是标准）指向它的构造函数的原型对象（prototype）。该原型对象也有一个自己的原型对象( __proto__ ) ，层层向上直到一个对象的原型对象为null。根据定义，null没有原型，并作为这个原型链中的最后一个环节。\n\n几乎所有 JavaScript 中的对象都...",
            "content": "<p>在 ES2015/ES6 中引入了&nbsp;<code>class</code>&nbsp;关键字，但那只是语法糖，JavaScript 仍然是基于原型的，它是动态的。</p>\n\n<p>继承：每个实例对象（ object ）都有一个私有属性（称之为 __proto__&nbsp; &nbsp;兼容性：IE11，不是标准）指向它的构造函数的原型对象（<strong>prototype&nbsp;</strong>）。该原型对象也有一个自己的原型对象( __proto__ ) ，层层向上直到一个对象的原型对象为&nbsp;<code>null</code>。根据定义，<code>null</code>&nbsp;没有原型，并作为这个<strong>原型链</strong>中的最后一个环节。</p>\n\n<p>几乎所有 JavaScript 中的对象都是位于原型链顶端的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object\"><code>Object</code></a>&nbsp;的实例。</p>\n\n<h2 id=\"基于原型链的继承\">基于原型链的继承</h2>\n\n<h3 id=\"继承属性\">继承属性</h3>\n\n<p>JavaScript 对象是动态的属性&ldquo;包&rdquo;（指其自己的属性）。JavaScript 对象有一个指向一个原型对象的链。当试图访问一个对象的属性时，它不仅仅在该对象上搜寻，还会搜寻该对象的原型，以及该对象的原型的原型，依次层层向上搜索，直到找到一个名字匹配的属性或到达原型链的末尾。</p>\n\n<p>遵循ECMAScript标准，<code>someObject.[[Prototype]]</code>&nbsp;符号是用于指向&nbsp;<code>someObject</code>&nbsp;的原型。从 ECMAScript 6 开始，<code>[[Prototype]]</code>&nbsp;可以通过&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/getPrototypeOf\"><code>Object.getPrototypeOf()</code></a>和&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/setPrototypeOf\"><code>Object.setPrototypeOf()</code></a>&nbsp;访问器来访问。这个等同于 JavaScript 的非标准但许多浏览器实现的属性&nbsp;<code>__proto__</code>。</p>\n\n<p>但它不应该与构造函数&nbsp;<code>func</code>&nbsp;的&nbsp;<code>prototype</code>&nbsp;属性相混淆。被构造函数创建的实例对象的&nbsp;<code>[[Prototype]]</code>&nbsp;指向&nbsp;<code>func</code>&nbsp;的&nbsp;<code>prototype</code>&nbsp;属性。<strong><code>Object.prototype</code>&nbsp;</strong>属性表示&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object\"><code>Object</code></a>的原型对象。</p>\n\n<p>这里演示当尝试访问属性时会发生什么：</p>\n\n<pre>\n<code>// 让我们从一个函数里创建一个对象o，它自身拥有属性a和b的：\nlet f = function () {\n   this.a = 1;\n   this.b = 2;\n}\n/* 这么写也一样\nfunction f() {\n  this.a = 1;\n  this.b = 2;\n}\n*/\nlet o = new f(); // {a: 1, b: 2}\n\n// 在f函数的原型上定义属性\nf.prototype.b = 3;\nf.prototype.c = 4;\n\n// 不要在 f 函数的原型上直接定义 f.prototype = {b:3,c:4};这样会直接打破原型链\n// o.[[Prototype]] 有属性 b 和 c\n//  (其实就是 o.__proto__ 或者 o.constructor.prototype)\n// o.[[Prototype]].[[Prototype]] 是 Object.prototype.\n// 最后o.[[Prototype]].[[Prototype]].[[Prototype]]是null\n// 这就是原型链的末尾，即 null，\n// 根据定义，null 就是没有 [[Prototype]]。\n\n// 综上，整个原型链如下: \n\n// {a:1, b:2} ---&gt; {b:3, c:4} ---&gt; Object.prototype---&gt; null\n\nconsole.log(o.a); // 1\n// a是o的自身属性吗？是的，该属性的值为 1\n\nconsole.log(o.b); // 2\n// b是o的自身属性吗？是的，该属性的值为 2\n// 原型上也有一个'b'属性，但是它不会被访问到。\n// 这种情况被称为\"属性遮蔽 (property shadowing)\"\n\nconsole.log(o.c); // 4\n// c是o的自身属性吗？不是，那看看它的原型上有没有\n// c是o.[[Prototype]]的属性吗？是的，该属性的值为 4\n\nconsole.log(o.d); // undefined\n// d 是 o 的自身属性吗？不是，那看看它的原型上有没有\n// d 是 o.[[Prototype]] 的属性吗？不是，那看看它的原型上有没有\n// o.[[Prototype]].[[Prototype]] 为 null，停止搜索\n// 找不到 d 属性，返回 undefined</code></pre>\n\n<p>代码来源链接：<a href=\"https://repl.it/@khaled_hossain_code/prototype\">https://repl.it/@khaled_hossain_code/prototype</a></p>\n\n<p>给对象设置属性会创建自有属性。获取和设置属性的唯一限制是内置&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Working_with_Objects#Defining_getters_and_setters\">getter 或 setter</a>&nbsp;的属性。</p>\n\n<h3 id=\"继承方法\">继承方法</h3>\n\n<p>在 JavaScript 里，任何函数都可以添加到对象上作为对象的属性。函数的继承与其他的属性继承没有差别，包括上面的&ldquo;属性遮蔽&rdquo;（这种情况相当于其他语言的<strong>方法重写</strong>）。</p>\n\n<p>当继承的函数被调用时，<strong><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/this\">this</a>&nbsp;指向的是当前对象，而不是函数所在的对象。</strong></p>\n\n<pre>\n<code>var o = {\n  a: 2,\n  m: function(){\n    return this.a + 1;\n  }\n};\n\nconsole.log(o.m()); // 3\n// 当调用 o.m 时，'this' 指向了 o.\n\nvar p = Object.create(o);\n// p是一个继承自 o 的对象\n\np.a = 4; // 创建 p 的自身属性 'a'\nconsole.log(p.m()); // 5\n// 调用 p.m 时，'this' 指向了 p\n// 又因为 p 继承了 o 的 m 函数\n// 所以，此时的 'this.a' 即 p.a，就是 p 的自身属性 'a' </code></pre>\n\n<h2 id=\"在_JavaScript_中使用原型\">在 JavaScript 中使用原型</h2>\n\n<p>接下去，来仔细分析一下这些应用场景下，&nbsp;JavaScript&nbsp;在背后做了哪些事情。</p>\n\n<p>我们可以给doSomething函数的原型对象添加新属性，如下：</p>\n\n<pre>\n<code>function doSomething(){}\ndoSomething.prototype.foo = \"bar\";\nconsole.log( doSomething.prototype );</code></pre>\n\n<p>可以看到运行后的结果如下：</p>\n\n<pre>\n<code>{\n    foo: \"bar\",\n    constructor: ƒ doSomething(),\n    __proto__: {\n        constructor: ƒ Object(),\n        hasOwnProperty: ƒ hasOwnProperty(),\n        isPrototypeOf: ƒ isPrototypeOf(),\n        propertyIsEnumerable: ƒ propertyIsEnumerable(),\n        toLocaleString: ƒ toLocaleString(),\n        toString: ƒ toString(),\n        valueOf: ƒ valueOf()\n    }\n}</code></pre>\n\n<p>现在我们可以通过new操作符来创建基于这个原型对象的doSomething实例。使用new操作符，只需在调用doSomething函数语句之前添加new。这样，便可以获得这个函数的一个实例对象。一些属性就可以添加到该原型对象中。</p>\n\n<p>请尝试运行以下代码：</p>\n\n<pre>\n<code>function doSomething(){}\ndoSomething.prototype.foo = \"bar\"; // add a property onto the prototype\nvar doSomeInstancing = new doSomething();\ndoSomeInstancing.prop = \"some value\"; // add a property onto the object\nconsole.log( doSomeInstancing );</code></pre>\n\n<p>运行的结果类似于以下的语句。</p>\n\n<pre>\n<code>{\n    prop: \"some value\",\n    __proto__: {\n        foo: \"bar\",\n        constructor: ƒ doSomething(),\n        __proto__: {\n            constructor: ƒ Object(),\n            hasOwnProperty: ƒ hasOwnProperty(),\n            isPrototypeOf: ƒ isPrototypeOf(),\n            propertyIsEnumerable: ƒ propertyIsEnumerable(),\n            toLocaleString: ƒ toLocaleString(),\n            toString: ƒ toString(),\n            valueOf: ƒ valueOf()\n        }\n    }\n}</code></pre>\n\n<p>如上所示,&nbsp;<code>doSomeInstancing</code>&nbsp;中的<code>__proto__</code>是&nbsp;<code>doSomething.prototype</code>. 但这是做什么的呢？当你访问<code>doSomeInstancing</code>&nbsp;中的一个属性，浏览器首先会查看<code>doSomeInstancing</code>&nbsp;中是否存在这个属性。</p>\n\n<p>如果&nbsp;<code>doSomeInstancing</code>&nbsp;不包含属性信息, 那么浏览器会在&nbsp;<code>doSomeInstancing</code>&nbsp;的&nbsp;<code>__proto__</code>&nbsp;中进行查找(同&nbsp;doSomething.prototype). 如属性在&nbsp;<code>doSomeInstancing</code>&nbsp;的&nbsp;<code>__proto__</code>&nbsp;中查找到，则使用&nbsp;<code>doSomeInstancing</code>&nbsp;中&nbsp;<code>__proto__</code>&nbsp;的属性。</p>\n\n<p>否则，如果&nbsp;<code>doSomeInstancing</code>&nbsp;中&nbsp;<code>__proto__</code>&nbsp;不具有该属性，则检查<code>doSomeInstancing</code>&nbsp;的&nbsp;<code>__proto__</code>&nbsp;的 &nbsp;<code>__proto__</code>&nbsp;是否具有该属性。</p>\n\n<p><strong><code>默认情况下函数的prototype是带有constructor属性的对象：</code></strong></p>\n\n<p><img alt=\"\" height=\"53\" src=\"https://img-blog.csdnimg.cn/20200628133037204.png\" width=\"196\" /></p>\n\n<p>默认情况下，任何<strong>函数的原型</strong>的&nbsp;<strong><code>__proto__</code>&nbsp;属性</strong> 都是&nbsp;<code>window.Object.prototype。</code></p>\n\n<p><img alt=\"\" height=\"264\" src=\"https://img-blog.csdnimg.cn/20200628133141640.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"373\" /></p>\n\n<p>因此, 通过<code>doSomeInstancing</code>&nbsp;的&nbsp;<code>__proto__</code>&nbsp;的 &nbsp;<code>__proto__</code>&nbsp; ( 同 doSomething.prototype 的&nbsp;<code>__proto__&nbsp;</code>(同&nbsp;&nbsp;<code>Object.prototype</code>)) 来查找要搜索的属性。</p>\n\n<p>&nbsp;</p>\n\n<p>如果属性不存在&nbsp;<code>doSomeInstancing</code>&nbsp;的&nbsp;<code>__proto__</code>&nbsp;的 &nbsp;<code>__proto__</code>&nbsp;中， 那么就会在<code>doSomeInstancing</code>&nbsp;的&nbsp;<code>__proto__</code>&nbsp;的 &nbsp;<code>__proto__</code>&nbsp;的 &nbsp;<code>__proto__</code>&nbsp;中查找。然而, 这里存在个问题：<code>doSomeInstancing</code>&nbsp;的&nbsp;<code>__proto__</code>&nbsp;的 &nbsp;<code>__proto__</code>&nbsp;的 &nbsp;<code>__proto__</code>&nbsp;其实不存在。因此，只有这样，在&nbsp;<code>__proto__</code>&nbsp;的整个原型链被查看之后，这里没有更多的&nbsp;<code>__proto__</code>&nbsp;， 浏览器断言该属性不存在，并给出属性值为&nbsp;<code>undefined</code>&nbsp;的结论。</p>\n\n<p>让我们在控制台窗口中输入更多的代码，如下：<strong>new操作符的原理</strong></p>\n\n<pre>\n<code>function doSomething(){}\ndoSomething.prototype.foo = \"bar\";\nvar doSomeInstancing = new doSomething();\ndoSomeInstancing.prop = \"some value\";\nconsole.log(\"doSomeInstancing.prop:      \" + doSomeInstancing.prop);\nconsole.log(\"doSomeInstancing.foo:       \" + doSomeInstancing.foo);\nconsole.log(\"doSomething.prop:           \" + doSomething.prop);\nconsole.log(\"doSomething.foo:            \" + doSomething.foo);\nconsole.log(\"doSomething.prototype.prop: \" + doSomething.prototype.prop);\nconsole.log(\"doSomething.prototype.foo:  \" + doSomething.prototype.foo);</code></pre>\n\n<p>结果如下：</p>\n\n<pre>\n<code>doSomeInstancing.prop:      some value\ndoSomeInstancing.foo:       bar\ndoSomething.prop:           undefined\ndoSomething.foo:            undefined\ndoSomething.prototype.prop: undefined\ndoSomething.prototype.foo:  bar</code></pre>\n\n<h2 id=\"使用不同的方法来创建对象和生成原型链\">使用不同的方法来创建对象和生成原型链</h2>\n\n<h3 id=\"使用语法结构创建的对象\">使用语法结构创建的对象</h3>\n\n<pre>\n<code>var o = {a: 1};\n\n// o 这个对象继承了 Object.prototype 上面的所有属性\n// o 自身没有名为 hasOwnProperty 的属性\n// hasOwnProperty 是 Object.prototype 的属性\n// 因此 o 继承了 Object.prototype 的 hasOwnProperty\n// Object.prototype 的原型为 null\n// 原型链如下:\n// o ---&gt; Object.prototype ---&gt; null\n\nvar a = [\"yo\", \"whadup\", \"?\"];\n\n// 数组都继承于 Array.prototype \n// (Array.prototype 中包含 indexOf, forEach 等方法)\n// 原型链如下:\n// a ---&gt; Array.prototype ---&gt; Object.prototype ---&gt; null\n\nfunction f(){\n  return 2;\n}\n\n// 函数都继承于 Function.prototype\n// (Function.prototype 中包含 call, bind等方法)\n// 原型链如下:\n// f ---&gt; Function.prototype ---&gt; Object.prototype ---&gt; null</code></pre>\n\n<h3 id=\"使用构造器创建的对象\">使用构造器创建的对象</h3>\n\n<p>在 JavaScript 中，构造器其实就是一个普通的函数。当使用&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/new\">new 操作符</a>&nbsp;来作用这个函数时，它就可以被称为构造方法（构造函数）。</p>\n\n<pre>\n<code>function Graph() {\n  this.vertices = [];\n  this.edges = [];\n}\n\nGraph.prototype = {\n  addVertex: function(v){\n    this.vertices.push(v);\n  }\n};\n\nvar g = new Graph();\n// g 是生成的对象，他的自身属性有 'vertices' 和 'edges'。\n// 在 g 被实例化时，g.[[Prototype]] 指向了 Graph.prototype。</code></pre>\n\n<h3 id=\"使用_Object.create_创建的对象\">使用&nbsp;<code>Object.create</code>&nbsp;创建的对象</h3>\n\n<p>ECMAScript 5 中引入了一个新方法：<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/create\"><code>Object.create()</code></a>。可以调用这个方法来创建一个新对象。<strong>第二个参数对应<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperties\"><code>Object.defineProperties()</code></a>的第二个参数，给新对象添加属性。</strong></p>\n\n<p>新对象的原型就是调用 create 方法时传入的第一个参数：</p>\n\n<pre>\n<code>var a = {a: 1}; \n// a ---&gt; Object.prototype ---&gt; null\n\nvar b = Object.create(a);\n// b ---&gt; a ---&gt; Object.prototype ---&gt; null\nconsole.log(b.a); // 1 (继承而来)\n\nvar c = Object.create(b);\n// c ---&gt; b ---&gt; a ---&gt; Object.prototype ---&gt; null\n\nvar d = Object.create(null);\n// d ---&gt; null\nconsole.log(d.hasOwnProperty); // undefined, 因为d没有继承Object.prototype</code></pre>\n\n<h3 id=\"使用_class_关键字创建的对象\">使用&nbsp;<code>class</code>&nbsp;关键字创建的对象</h3>\n\n<p>ECMAScript6 引入了一套新的关键字用来实现&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Classes\">class</a>。使用基于类语言的开发人员会对这些结构感到熟悉，但它们是不同的。JavaScript 仍然基于原型。这些新的关键字包括&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/class\"><code>class</code></a>,&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Classes/constructor\"><code>constructor</code></a>，<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Classes/static\"><code>static</code></a>，<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Classes/extends\"><code>extends</code></a>&nbsp;和&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/super\"><code>super</code></a>。</p>\n\n<pre>\n<code>\"use strict\";\n\nclass Polygon {\n  constructor(height, width) {\n    this.height = height;\n    this.width = width;\n  }\n}\n\nclass Square extends Polygon {\n  constructor(sideLength) {\n    super(sideLength, sideLength);\n  }\n  get area() {\n    return this.height * this.width;\n  }\n  set sideLength(newLength) {\n    this.height = newLength;\n    this.width = newLength;\n  }\n}\n\nvar square = new Square(2);</code></pre>\n\n<h3 id=\"性能\">性能</h3>\n\n<p>在原型链上查找属性比较耗时，对性能有副作用，这在性能要求苛刻的情况下很重要。另外，试图访问不存在的属性时会遍历整个原型链。</p>\n\n<p>遍历对象的属性时，原型链上的<strong>每个</strong>可枚举属性都会被枚举出来。要检查对象是否具有自己定义的属性，而不是其原型链上的某个属性，则必须使用所有对象从&nbsp;<code>Object.prototype</code>&nbsp;继承的&nbsp;<code><a href=\"https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/Object/hasOwnProperty\">hasOwnProperty</a></code>&nbsp;方法。下面给出一个具体的例子来说明它：</p>\n\n<p><code><a href=\"https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/Object/hasOwnProperty\">hasOwnProperty</a></code>&nbsp;是 JavaScript 中唯一一个处理属性并且<strong>不会</strong>遍历原型链的方法。（译者注：原文如此。另一种这样的方法：<code><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/keys\">Object.keys()</a></code>）</p>\n\n<p>注意：检查属性是否为&nbsp;<code><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/undefined\">undefined</a></code>&nbsp;是<strong>不能够</strong>检查其是否存在的。该属性可能已存在，但其值恰好被设置成了&nbsp;<code>undefined</code>。</p>\n\n<h3 id=\"总结：4_个用于拓展原型链的方法\">总结：4 个用于拓展原型链的方法</h3>\n\n<p>下面列举四种用于拓展原型链的方法，以及他们的优势和缺陷。下列四个例子都创建了完全相同的&nbsp;<code>inst</code>&nbsp;对象（所以在控制台上的输出也是一致的），为了举例，唯一的区别是他们的创建方法不同。</p>\n\n<table>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style=\"width:96px\">名称</td>\n\t\t\t<td style=\"width:283px\">例子</td>\n\t\t\t<td style=\"width:227px\">优势</td>\n\t\t\t<td style=\"width:239px\">缺陷</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"width:96px\">New-initialization</td>\n\t\t\t<td style=\"width:283px\">\n\t\t\t<pre>\n<code class=\"language-javascript\">function foo(){}\nfoo.prototype = {\n  foo_prop: \"foo val\"\n};\nfunction bar(){}\nvar proto = new foo;\nproto.bar_prop = \"bar val\";\nbar.prototype = proto;\nvar inst = new bar;\nconsole.log(inst.foo_prop);\nconsole.log(inst.bar_prop);</code></pre>\n\t\t\t</td>\n\t\t\t<td style=\"width:227px\">支持目前以及所有可想象到的浏览器(IE5.5都可以使用). 这种方法非常快，非常符合标准，并且充分利用JIST优化。</td>\n\t\t\t<td style=\"width:239px\">为使用此方法，这个问题中的函数必须要被初始化。 在这个初始化过程中，构造可以存储一个唯一的信息，并强制在每个对象中生成。但是，这个一次性生成的独特信息，可能会带来潜在的问题。另外，构造函数的初始化，可能会给生成对象带来并不想要的方法。 然而，如果你只在自己的代码中使用，你也清楚（或有通过注释等写明）各段代码在做什么，这些在大体上都根本不是问题（事实上，还常常是有益处的）。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"width:96px\">Object.create</td>\n\t\t\t<td style=\"width:283px\">\n\t\t\t<pre>\n<code>function foo(){}\nfoo.prototype = {\n  foo_prop: \"foo val\"\n};\nfunction bar(){}\nvar proto = Object.create(\n  foo.prototype\n);\nproto.bar_prop = \"bar val\";\nbar.prototype = proto;\nvar inst = new bar;\nconsole.log(inst.foo_prop);\nconsole.log(inst.bar_prop);\n</code></pre>\n\n\t\t\t<pre>\n<code>function foo(){}\nfoo.prototype = {\n  foo_prop: \"foo val\"\n};\nfunction bar(){}\nvar proto = Object.create(\n  foo.prototype,\n  {\n    bar_prop: {\n      value: \"bar val\"\n    }\n  }\n);\nbar.prototype = proto;\nvar inst = new bar;\nconsole.log(inst.foo_prop);\nconsole.log(inst.bar_prop)</code></pre>\n\t\t\t</td>\n\t\t\t<td style=\"width:227px\">支持当前所有非微软版本或者 IE9 以上版本的浏览器。允许一次性地直接设置&nbsp;<code>__proto__</code>&nbsp;属性，以便浏览器能更好地优化对象。同时允许通过&nbsp;<code>Object.create(null)</code>来创建一个没有原型的对象。</td>\n\t\t\t<td style=\"width:239px\">不支持 IE8 以下的版本。然而，随着微软不再对系统中运行的旧版本浏览器提供支持，这将不是在大多数应用中的主要问题。 另外，这个慢对象初始化在使用第二个参数的时候有可能成为一个性能黑洞，因为每个对象的描述符属性都有自己的描述对象。当以对象的格式处理成百上千的对象描述的时候，可能会造成严重的性能问题。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"width:96px\">\n\t\t\t<p>Object.setPrototypeOf</p>\n\t\t\t</td>\n\t\t\t<td style=\"width:283px\">\n\t\t\t<pre>\n<code>function foo(){}\nfoo.prototype = {\n  foo_prop: \"foo val\"\n};\nfunction bar(){}\nvar proto = {\n  bar_prop: \"bar val\"\n};\nObject.setPrototypeOf(\n  proto, foo.prototype\n);\nbar.prototype = proto;\nvar inst = new bar;\nconsole.log(inst.foo_prop);\nconsole.log(inst.bar_prop);\n</code></pre>\n\n\t\t\t<pre>\n<code>function foo(){}\nfoo.prototype = {\n  foo_prop: \"foo val\"\n};\nfunction bar(){}\nvar proto;\nproto=Object.setPrototypeOf(\n  { bar_prop: \"bar val\" },\n  foo.prototype\n);\nbar.prototype = proto;\nvar inst = new bar;\nconsole.log(inst.foo_prop);\nconsole.log(inst.bar_prop)</code></pre>\n\t\t\t</td>\n\t\t\t<td style=\"width:227px\">支持所有现代浏览器和微软IE9+浏览器。允许动态操作对象的原型，甚至能强制给通过&nbsp;<code>Object.create(null)</code>创建出来的没有原型的对象添加一个原型。</td>\n\t\t\t<td style=\"width:239px\">这个方式表现并不好，应该被弃用。如果你在生产环境中使用这个方法，那么快速运行 Javascript 就是不可能的，因为许多浏览器优化了原型，尝试在调用实例之前猜测方法在内存中的位置，但是动态设置原型干扰了所有的优化，甚至可能使浏览器为了运行成功，使用完全未经优化的代码进行重编译。 不支持 IE8 及以下的浏览器版本。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"width:96px\">__proto__</td>\n\t\t\t<td style=\"width:283px\">\n\t\t\t<pre>\n<code>function foo(){}\nfoo.prototype = {\n  foo_prop: \"foo val\"\n};\nfunction bar(){}\nvar proto = {\n  bar_prop: \"bar val\",\n  __proto__: foo.prototype\n};\nbar.prototype = proto;\nvar inst = new bar;\nconsole.log(inst.foo_prop);\nconsole.log(inst.bar_prop);\n</code></pre>\n\n\t\t\t<pre>\n<code>var inst = {\n  __proto__: {\n    bar_prop: \"bar val\",\n    __proto__: {\n      foo_prop: \"foo val\",\n      __proto__: Object.prototype\n    }\n  }\n};\nconsole.log(inst.foo_prop);\nconsole.log(inst.bar_prop)</code></pre>\n\t\t\t</td>\n\t\t\t<td style=\"width:227px\">支持所有现代非微软版本以及 IE11 以上版本的浏览器。将&nbsp;<code>__proto__</code>&nbsp;设置为非对象的值会静默失败，并不会抛出错误。</td>\n\t\t\t<td style=\"width:239px\">应该完全将其抛弃因为这个行为完全不具备性能可言。 如果你在生产环境中使用这个方法，那么快速运行 Javascript 就是不可能的，因为许多浏览器优化了原型，尝试在调用实例之前猜测方法在内存中的位置，但是动态设置原型干扰了所有的优化，甚至可能使浏览器为了运行成功，使用完全未经优化的代码进行重编译。不支持 IE10 及以下的浏览器版本。</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<h2 id=\"prototype_和_Object.getPrototypeOf\"><code>prototype</code>&nbsp;和&nbsp;<code>Object.getPrototypeOf</code></h2>\n\n<p>你可能已经注意到我们的 function A 有一个叫做&nbsp;<code>prototype</code>&nbsp;的特殊属性。该特殊属性可与 JavaScript 的&nbsp;<code>new</code>&nbsp;操作符一起使用。对原型对象的引用被复制到新实例的内部&nbsp;<code>[[Prototype]]</code>属性。例如，当执行&nbsp;<code>var a1 = new A();</code>&nbsp;时，JavaScript（在内存中创建对象之后，和在运行函数&nbsp;<code>A()</code>&nbsp;把&nbsp;<code>this</code>&nbsp;指向对象之前）设置&nbsp;<code>a1.[[Prototype]] = A.prototype;</code>。然后当您访问实例的属性时，JavaScript 首先会检查它们是否直接存在于该对象上，如果不存在，则会&nbsp;<code>[[Prototype]]</code>&nbsp;中查找。这意味着你在&nbsp;<code>prototype</code>&nbsp;中定义的所有内容都可以由所有实例有效地共享，你甚至可以稍后更改部分&nbsp;<code>prototype</code>，并在所有现有实例中显示更改（如果有必要的话）。</p>\n\n<p>像上面的例子中，如果你执行&nbsp;<code>var a1 = new A(); var a2 = new A();</code>&nbsp;那么&nbsp;<code>a1.doSomething</code>&nbsp;事实上会指向&nbsp;<code>Object.getPrototypeOf(a1).doSomething</code>，它就是你在&nbsp;<code>A.prototype.doSomething</code>&nbsp;中定义的内容。也就是说：<code>Object.getPrototypeOf(a1).doSomething == Object.getPrototypeOf(a2).doSomething == A.prototype.doSomething</code>（补充：实际上，执行&nbsp;<code>a1.doSomething()</code>&nbsp;相当于执行&nbsp;<code>Object.getPrototypeOf(a1).doSomething.call(a1)==A.prototype.doSomething.call(a1)</code>）</p>\n\n<p>简而言之，&nbsp;<code>prototype</code>&nbsp;是用于类的，而&nbsp;<code>Object.getPrototypeOf()</code>&nbsp;是用于实例的（instances），两者功能一致。</p>\n\n<p><code>[[Prototype]]</code>&nbsp;看起来就像<strong>递归</strong>引用， 如&nbsp;<code>a1.doSomething</code>、<code>Object.getPrototypeOf(a1).doSomething</code>、<code>Object.getPrototypeOf(Object.getPrototypeOf(a1)).doSomething</code>&nbsp;等等等， 直到它被找到或&nbsp;<code>Object.getPrototypeOf</code>&nbsp;返回&nbsp;<code>null</code>。</p>\n\n<p>因此，当你执行：</p>\n\n<pre>\n<code>var o = new Foo();</code></pre>\n\n<p>JavaScript 实际上执行的是：</p>\n\n<pre>\n<code>var o = new Object();\no.__proto__ = Foo.prototype;\nFoo.call(o);</code></pre>\n\n<p>（或者类似上面这样的），然后，当你执行：</p>\n\n<pre>\n<code>o.someProp;</code></pre>\n\n<p>它检查 o 是否具有&nbsp;<code>someProp</code>&nbsp;属性。如果没有，它会查找&nbsp;<code>Object.getPrototypeOf(o).someProp</code>，如果仍旧没有，它会继续查找&nbsp;<code>Object.getPrototypeOf(Object.getPrototypeOf(o)).someProp</code>。</p>\n\n<h2 id=\"结论\">结论</h2>\n\n<p>在使用原型继承编写复杂代码之前，理解原型继承模型是<strong>至关重要</strong>的。此外，请注意代码中原型链的长度，并在必要时将其分解，以避免可能的性能问题。此外，原生原型<strong>不应该</strong>被扩展，除非它是为了与新的 JavaScript 特性兼容。</p>\n",
            "markdowncontent": "",
            "tags": "javascript",
            "categories": "JavaScript",
            "type": "repost",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Inheritance_and_the_prototype_chain",
            "authorized_status": true,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106895850"
        },
        object_new: `"<p><strong>1、new操作符之前的修改原型</strong></p>

        <p>申明一个函数的时候 ，该函数的prototype属性指向原型，且原型中默认是constructor属性，指向该函数。</p>
        
        <p>注意修改函数的prototype，是修改引用指向，还是修改了对象本身。</p>
        
        <p>修改引用指向，就是修改了原型，指向一个新的对象：</p>
        
        <pre>
        <code class="language-javascript">
                function Foo(){
                    this.name = "foo";
                    this.age = 24
                }
                console.log(Foo.prototype);
                有默认的原型，原型内的属性：
                constructor: ƒ Foo()
                __proto__: Object
                
                修改了原型
                Foo.prototype = {
                    a: 1,
                    b: 2
                }</code></pre>
        
        <p>给默认原型添加属性和方法 ：</p>
        
        <pre>
        <code class="language-javascript">
                function Foo(){
                    this.name = "foo";
                    this.age = 24
                }
                
                Foo.prototype.a = 1;
                Foo.prototype.b = 2;
                console.log(Foo.prototype);//默认原型上添加属性和方法
        
                默认原型上添加了a和b属性
                a: 1
                b: 2
                constructor: ƒ Foo()
                __proto__: Object</code></pre>
        
        <p><strong><code>2、new</code>&nbsp;关键字会进行如下的操作：</strong></p>
        
        <ol>
            <li>创建一个空对象（即<code><strong>{}</strong></code>）；</li>
            <li>该对象的__proto__属性指向构造函数的原型（prototype）；</li>
            <li>将该对象作为<code><strong>this</strong></code>的上下文 ；即执行构造函数，他的this是该对象。</li>
            <li>如果构造函数没有返回对象，则返回<code><strong>this</strong></code>。</li>
        </ol>
        
        <p>new操作符会用到原型，将[[Prototype]]属性会指向构造函数的原型。修改[[Prototype]]的指向和修改[[Prototype]]指向的原型是不同的。</p>
        
        <p><strong>3、new操作符之后修改原型</strong></p>
        
        <p>修改实例的__proto__指向：不影响其他的实例的原型</p>
        
        <pre>
        <code class="language-javascript">
                function Foo(){
                    this.name = "foo";
                    this.age = 24
                }
                Foo.prototype.a = 1;
                Foo.prototype.b = 2;
        
                var obj1 = new Foo();
                obj1.__proto__ = {
                    name: "新的对象"
                }
                console.log(obj1.__proto__);</code></pre>
        
        <p>修改__proto__指向的原型：其实也可以像a和b赋值那样添加属性</p>
        
        <pre>
        <code class="language-javascript">
                function Foo(){
                    this.name = "foo";
                    this.age = 24
                }
                Foo.prototype.a = 1;
                Foo.prototype.b = 2;
        
                var obj1 = new Foo();
                obj1.__proto__.name = "原型上添加name属性";
                console.log(obj1.__proto__);</code></pre>
        
        <p>&nbsp;</p>
        "`
        ,
        /**内置对象 */
        js_date: `<h2>日期对象Date</h2>`
        ,
        js_math: `<h2>Math对象</h2>`,
        js_number: {
            "article_id": "108217648",
            "title": "Number类型",
            "description": "Number数字类型\n\n1、Number对象由Number()构造器创建。JavaScript的Number类型为双精度IEEE 754 64位浮点类型。最近出了stage3BigInt任意精度数字类型，已经进入stage3规范。\n\n2、关于number对象和number字面量的区别：\n\n\nnew Number(value); \nvar a = new Number('123'); // a === 123 is false\nvar b = Number('123'); // b === 123 i...",
            "content": "<h1>Number数字类型</h1>\n\n<h3><strong><code>1、Number</code>&nbsp;对象由&nbsp;<code>Number()</code>&nbsp;构造器创建。</strong></h3>\n\n<p>JavaScript的<code>Number</code>类型为<a href=\"https://en.wikipedia.org/wiki/Floating-point_arithmetic\">双精度IEEE 754 64位浮点</a>类型。<span style=\"color:#333333\">最近出了stage3</span><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/BigInt\"><code>BigInt</code></a>&nbsp;任意精度数字类型，已经进入stage3规范。</p>\n\n<h3><strong>2、关于number对象和number字面量的区别：</strong></h3>\n\n<pre>\n<code class=\"language-javascript\">new Number(value); \nvar a = new Number('123'); // a === 123 is false\nvar b = Number('123'); // b === 123 is true\na instanceof Number; // is true\nb instanceof Number; // is false</code></pre>\n\n<h3><strong>3、Number()方法</strong></h3>\n\n<p>可以将参数转为数字，如果参数无法被转换为数字，则返回&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a>。</p>\n\n<pre>\n<code class=\"language-javascript\">console.log(Number(\"3.14\")) //3.14  \nconsole.log(Number(true))    //1\nconsole.log(Number(\"\"))     //0\nconsole.log(Number(null))   //0\nconsole.log(Number([]))  //0\nNumber('0x11')    // 17\nNumber('0b11')    // 3\nNumber('0o11')    // 9\nconsole.log(Number(undefined)) //NaN\nconsole.log(Number(\"3.14a\")) //NaN\nconsole.log(Number({}))  //NaN</code></pre>\n\n<h3>相关方法：</h3>\n\n<p><strong>（1）valueOf()</strong></p>\n\n<p>返回一个 Number 对象的基本数字值。</p>\n\n<pre>\n<code class=\"language-javascript\">var num = new Number(123);\nconsole.log(num, num.valueOf());\n</code></pre>\n\n<p>&nbsp;<img alt=\"\" height=\"22\" src=\"https://img-blog.csdnimg.cn/20200901113607258.png\" width=\"131\" /></p>\n\n<p><strong>（2）toString()</strong></p>\n\n<p>&nbsp;2 ~ 36 之间的整数，省略该参数，则使用基数 10</p>\n\n<p>指定数字转换为指定进制的数字字符串。</p>\n\n<pre>\n<code class=\"language-javascript\">console.log((3).toString(2));//\"11\"</code></pre>\n\n<p><strong>（3）tofixed()</strong></p>\n\n<p>规定数字保留几位小数，参数介于 0 到 20 （包括）之间，不传参数时为0，默认保留整数，该数值在必要时进行四舍五入，另外在必要时会用 0 来填充小数部分，以便小数部分有指定的位数。</p>\n\n<p>浮点数不能精确地用二进制表示所有小数。这可能会导致意外的结果，例如&nbsp;<code>0.1 + 0.2 === 0.3</code>&nbsp;返回&nbsp;<code>false</code>&nbsp;.</p>\n\n<p><img alt=\"\" height=\"44\" src=\"https://img-blog.csdnimg.cn/20200901140151638.png\" width=\"148\" /></p>\n\n<p>0.55 1.55 2.55 3.55 4.55等，实际存储的值要小一点。</p>\n\n<p><img alt=\"\" height=\"127\" src=\"https://img-blog.csdnimg.cn/20200901140058800.png\" width=\"187\" /></p>\n\n<pre>\n<code class=\"language-javascript\">console.log((3.245).toFixed(2));//3.25 \nconsole.log((3).toFixed(2));//3.00</code></pre>\n\n<p><strong>（4）&nbsp;toPrecision()</strong></p>\n\n<p>保留数字的长度，也会进行四舍五入。该参数是 1 ~ 21 之间（且包括 1 和 21）的值</p>\n\n<pre>\n<code class=\"language-javascript\">var num = new Number(13.3714);\nvar a = num.toPrecision();  13.3714\nvar b = num.toPrecision(2);  13\nvar c = num.toPrecision(3);  13.4\nvar d = num.toPrecision(10);  13.37140000\n</code></pre>\n\n<p><strong>（5）toExponential()</strong></p>\n\n<p>转为科学计数法，且保留几位小数。是 0 ~ 20 之间的值，包括 0 和 20。</p>\n\n<p>科学计数法的值，保留两位小数！</p>\n\n<pre>\n<code class=\"language-javascript\">console.log((123.456789).toExponential(2));//1.23e+2</code></pre>\n\n<h3><a href=\"https://blog.csdn.net/zyz00000000/article/details/107610841\">内置Math对象的几个常用方法&nbsp;</a></h3>\n\n<h3><strong>4、静态属性</strong></h3>\n\n<p>英文integer：整数</p>\n\n<p><code>整数的范围：-2^53</code>到<code>2^53</code>之间（不含两个端点）</p>\n\n<p><strong>（1）最大安全数和最小安全数</strong></p>\n\n<p><strong><code>Number.MAX_SAFE_INTEGER</code></strong>&nbsp;常量表示在&nbsp;JavaScript 中最大的安全整数（maxinum safe integer)（<code><strong>2^</strong>53&nbsp;- 1）。</code></p>\n\n<p>MAX_SAFE_INTEGER 是一个值为&nbsp;9007199254740991的常量。这里安全存储的意思是指能够准确区分两个不相同的值。</p>\n\n<p>例如&nbsp;<code>Number.MAX_SAFE_INTEGER&nbsp;+&nbsp;1 === Number.MAX_SAFE_INTEGER&nbsp;+&nbsp;2 将得到 true的结果，而这在数学上是错误的。</code></p>\n\n<p>可以通过Math对象的pow方法，计算（<code><strong>2^</strong>53&nbsp;- 1）</code></p>\n\n<pre>\n<code class=\"language-javascript\">console.log(Math.pow(2, 53) - 1 )  //9007199254740991</code></pre>\n\n<p><strong><code>Number.MIN_SAFE_INTEGER</code></strong>&nbsp;代表在&nbsp;JavaScript中最小的安全的integer型数字 (<code>-(2^53&nbsp;- 1)</code>).</p>\n\n<pre>\n<code class=\"language-javascript\">Number.MIN_SAFE_INTEGER // -9007199254740991\n-(Math.pow(2, 53) - 1)  // -9007199254740991</code></pre>\n\n<p><strong><code>（2）最大正（负）数和最小正（负）数</code></strong></p>\n\n<p><code><strong>Number.MAX_VALUE</strong></code>&nbsp;属性表示在 JavaScript 里所能表示的最大数值。<code>MAX_VALUE</code>&nbsp;属性值接近于&nbsp;<code>1.79E+308</code>。大于&nbsp;<code>MAX_VALUE</code>&nbsp;的值代表 &quot;<code>Infinity</code>&quot;。</p>\n\n<p>最小的负数是&nbsp;<code>-MAX_VALUE</code>。</p>\n\n<p><code><strong>Number.MIN_VALUE</strong></code>&nbsp;属性表示在 JavaScript 中所能表示的最小的正值。<code>MIN_VALUE</code>&nbsp;属性是 JavaScript 里最接近 0 的正值，而不是最小的负值。<code>MIN_VALUE</code>&nbsp;的值约为 5e-324。小于&nbsp;<code>MIN_VALUE</code>&nbsp;(&quot;underflow values&quot;) 的值将会转换为 0。</p>\n\n<p>最大的负数是&nbsp;<code>-MIN_VALUE。</code></p>\n\n<p><strong>（3）正无穷和负无穷</strong></p>\n\n<p><code><strong>Number.POSITIVE_INFINITY</strong></code>&nbsp;属性表示正无穷大。</p>\n\n<p><code>Number.POSITIVE_INFINITY</code>&nbsp;的值同全局对象&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Infinity\"><code>Infinity</code></a>&nbsp;属性的值相同。</p>\n\n<p>该值的表现同数学上的无穷大有点儿不同：</p>\n\n<ul>\n\t<li>任何正值，包括&nbsp;<code>POSITIVE_INFINITY</code>，乘以&nbsp;<code>POSITIVE_INFINITY</code>&nbsp;为&nbsp;<code>POSITIVE_INFINITY</code>。</li>\n\t<li>任何负值，包括&nbsp;<code>NEGATIVE_INFINITY</code>，乘以&nbsp;<code>POSITIVE_INFINITY</code>&nbsp;为&nbsp;<code>NEGATIVE_INFINITY</code>。</li>\n\t<li>0 乘以&nbsp;<code>POSITIVE_INFINITY</code>&nbsp;为 NaN。</li>\n\t<li>NaN 乘以&nbsp;<code>POSITIVE_INFINITY</code>&nbsp;为 NaN。</li>\n\t<li><code>POSITIVE_INFINITY</code>&nbsp;除以&nbsp;<code>NEGATIVE_INFINITY</code>&nbsp;以外的任何负值为&nbsp;<code>NEGATIVE_INFINITY</code>。</li>\n\t<li><code>POSITIVE_INFINITY</code>&nbsp;除以&nbsp;<code>POSITIVE_INFINITY</code>&nbsp;以外的任何正值为&nbsp;<code>POSITIVE_INFINITY</code>。</li>\n\t<li><code>POSITIVE_INFINITY</code>&nbsp;除以&nbsp;<code>NEGATIVE_INFINITY</code>&nbsp;或&nbsp;<code>POSITIVE_INFINITY</code>&nbsp;为 NaN。</li>\n\t<li>任何数除以&nbsp;<code>POSITIVE_INFINITY</code>&nbsp;为 0</li>\n</ul>\n\n<p><code><strong>Number.NEGATIVE_INFINITY</strong></code>&nbsp;属性表示负无穷大。</p>\n\n<p>该值的行为同数学上的无穷大（infinity）有一点儿不同：</p>\n\n<ul>\n\t<li>任何正值，包括&nbsp;<code>POSITIVE_INFINITY，</code>乘以&nbsp;<code>NEGATIVE_INFINITY</code>&nbsp;为&nbsp;<code>NEGATIVE_INFINITY</code>。</li>\n\t<li>任何负值，包括&nbsp;<code>NEGATIVE_INFINITY</code>，乘以&nbsp;<code>NEGATIVE_INFINITY</code>&nbsp;为&nbsp;<code>POSITIVE_INFINITY</code>。</li>\n\t<li>0 乘以&nbsp;<code>NEGATIVE_INFINITY</code>&nbsp;为&nbsp;<code>NaN</code>.</li>\n\t<li>NaN 乘以&nbsp;<code>NEGATIVE_INFINITY</code>&nbsp;为<code>&nbsp;NaN</code>.</li>\n\t<li><code>NEGATIVE_INFINITY</code>&nbsp;除以任何负值（除了&nbsp;<code>NEGATIVE_INFINITY）</code>为&nbsp;<code>POSITIVE_INFINITY</code>。</li>\n\t<li><code>NEGATIVE_INFINITY</code>&nbsp;除以任何正值（除了&nbsp;<code>POSITIVE_INFINITY</code>）为&nbsp;<code>NEGATIVE_INFINITY</code>。</li>\n\t<li><code>NEGATIVE_INFINITY</code>&nbsp;除以&nbsp;<code>NEGATIVE_INFINITY</code>&nbsp;或&nbsp;<code>POSITIVE_INFINITY</code>&nbsp;是&nbsp;<code>NaN</code>。</li>\n\t<li>任何数除以&nbsp;<code>NEGATIVE_INFINITY</code>&nbsp;为 0。</li>\n</ul>\n\n<p><strong>（4）NaN值</strong></p>\n\n<p><code><strong>Number.NaN</strong></code>&nbsp;表示&ldquo;非数字&rdquo;（Not-A-Number）。和&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a>&nbsp;相同。</p>\n\n<h3>5、方法 （ES6）</h3>\n\n<p>全局属性&nbsp;<strong><code>NaN</code></strong>&nbsp;的值表示不是一个数字（Not-A-Number）。</p>\n\n<p><strong><code>（1）Number.isNaN()</code></strong>&nbsp;方法确定传递的值是否为&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a>，并且检查其类型是否为&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number\"><code>Number</code></a>。它是原来的全局&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/isNaN\"><code>isNaN()</code></a>&nbsp;的更稳妥的版本。</p>\n\n<pre>\n<code class=\"language-javascript\">Number.isNaN(NaN);        // true\nNumber.isNaN(Number.NaN); // true\nNumber.isNaN(0 / 0)       // true\n\n// 下面这几个如果使用全局的 isNaN() 时，会返回 true。\nNumber.isNaN(\"NaN\");      // false，字符串 \"NaN\" 不会被隐式转换成数字 NaN。\nNumber.isNaN(undefined);  // false\nNumber.isNaN({});         // false\nNumber.isNaN(\"blabla\");   // false\n\n// 下面的都返回 false\nNumber.isNaN(true);\nNumber.isNaN(null);\nNumber.isNaN(37);\nNumber.isNaN(\"37\");\nNumber.isNaN(\"37.37\");\nNumber.isNaN(\"\");\nNumber.isNaN(\" \");</code></pre>\n\n<p id=\"Polyfill\"><strong>Polyfill：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">Number.isNaN = Number.isNaN || function(value) {\n    return typeof value === \"number\" &amp;&amp; isNaN(value);\n}</code></pre>\n\n<p><strong><code>（2）Number.isInteger()</code></strong>&nbsp;方法用来判断给定的参数是否为整数。</p>\n\n<pre>\n<code class=\"language-javascript\">Number.isInteger(0);         // true\nNumber.isInteger(1);         // true\nNumber.isInteger(-100000);   // true\n\nNumber.isInteger(0.1);       // false\nNumber.isInteger(Math.PI);   // false\n\nNumber.isInteger(Infinity);  // false\nNumber.isInteger(-Infinity); // false\nNumber.isInteger(\"10\");      // false\nNumber.isInteger(true);      // false\nNumber.isInteger(false);     // false\nNumber.isInteger([1]);       // false</code></pre>\n\n<p><strong>Polyfill：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">Number.isInteger = Number.isInteger || function(value) {\n    return typeof value === \"number\" &amp;&amp; \n           isFinite(value) &amp;&amp; \n           Math.floor(value) === value;\n};</code></pre>\n\n<p><strong><code>（3）Number.isSafeInteger()</code></strong>&nbsp;方法用来判断传入的参数值是否是一个&ldquo;安全整数&rdquo;（safe integer）</p>\n\n<pre>\n<code class=\"language-javascript\">Number.isSafeInteger(3);                    // true\nNumber.isSafeInteger(Math.pow(2, 53))       // false\nNumber.isSafeInteger(Math.pow(2, 53) - 1)   // true\nNumber.isSafeInteger(NaN);                  // false\nNumber.isSafeInteger(Infinity);             // false\nNumber.isSafeInteger(\"3\");                  // false\nNumber.isSafeInteger(3.1);                  // false\nNumber.isSafeInteger(3.0);                  // true</code></pre>\n\n<p><strong>Polyfill：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">Number.isSafeInteger = Number.isSafeInteger || function (value) {\n   return Number.isInteger(value) &amp;&amp; Math.abs(value) &lt;= Number.MAX_SAFE_INTEGER;\n};</code></pre>\n\n<p><strong><code>（4）Number.isFinite()</code></strong>&nbsp;方法用来检测传入的参数是否是一个有穷数（finite number）</p>\n\n<p>和全局的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/isFinite\"><code>isFinite()</code></a>&nbsp;函数相比，这个方法不会强制将一个非数值的参数转换成数值，这就意味着，只有数值类型的值，且是有穷的（finite），才返回&nbsp;<code>true</code></p>\n\n<pre>\n<code class=\"language-javascript\">Number.isFinite(Infinity);  // false\nNumber.isFinite(NaN);       // false\nNumber.isFinite(-Infinity); // false\n\nNumber.isFinite(0);         // true\nNumber.isFinite(2e64);      // true\n\nNumber.isFinite('0');       // false, 全局函数 isFinite('0') 会返回 true</code></pre>\n\n<p><strong>Polyfill：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">Number.isFinite = Number.isFinite || function(value) {\n    return typeof value === \"number\" &amp;&amp; isFinite(value);\n}</code></pre>\n\n<p><strong><code>（5）Number.parseFloat()</code></strong>&nbsp;方法可以把一个字符串解析成浮点数。该方法与全局的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/parseFloat\"><code>parseFloat()</code></a>&nbsp;函数相同，并且处于&nbsp;ECMAScript 6 规范中（用于全局变量的模块化）。</p>\n\n<p>给定值被解析成浮点数，如果无法被解析成浮点数，则返回<code>NaN</code></p>\n\n<p><strong><code>（6）Number.parseInt()</code></strong>&nbsp;方法依据指定基数 [ 参数&nbsp;<strong>radix&nbsp;</strong>的值]，把字符串 [ 参数&nbsp;<strong>string</strong>&nbsp;的值] 解析成整数。</p>\n\n<h3>与Number相关的全局属性</h3>\n\n<p>1、全局属性&nbsp;<strong><code>NaN</code></strong>&nbsp;</p>\n\n<p>NaN的值表示不是一个数字（Not-A-Number）。编码中很少直接使用<code>到 NaN</code>。通常都是在计算失败时，作为 Math 的某个方法的返回值出现的。</p>\n\n<p>2、全局属性&nbsp;<code><strong>Infinity</strong></code>&nbsp;是一个数值，表示无穷大。</p>\n\n<p><code>Infinity</code>&nbsp;的初始值是&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number/POSITIVE_INFINITY\"><code>Number.POSITIVE_INFINITY</code></a>。<code>Infinity</code>（正无穷大）大于任何值。该值和数学意义上的无穷大很像，例如任何正值乘以&nbsp;<code>Infinity</code>&nbsp;为&nbsp;<code>Infinity</code>, 任何数值除以&nbsp;<code>Infinity</code>&nbsp;为 0</p>\n\n<p>3、<code>parseFloat</code>是个全局函数。<strong><code>parseFloat()</code>&nbsp;</strong>函数解析一个参数（必要时先转换为字符串）并返回一个浮点数。</p>\n\n<p><strong>4、parseInt(<em>string</em>,&nbsp;<em>radix</em>)&nbsp;&nbsp;</strong>&nbsp;解析一个字符串并返回指定基数的十进制整数，&nbsp;<code>radix</code>&nbsp;是2-36之间的整数，表示被解析字符串的基数。</p>\n\n<p>下面的例子都返回<strong>3.14</strong></p>\n\n<pre>\n<code class=\"language-javascript\">parseFloat(3.14);\nparseFloat('3.14');\nparseFloat('  3.14  ');\nparseFloat('314e-2');\nparseFloat('0.0314E+2');\nparseFloat('3.14some non-digit characters');\nparseFloat({ toString: function() { return \"3.14\" } });</code></pre>\n\n<p>考虑使用&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number\"><code>Number(<em>value</em>)</code></a>&nbsp;进行更严谨的解析，只要参数带有无效字符就会被转换为&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a>&nbsp;。</p>\n",
            "markdowncontent": "",
            "tags": "javascript,Number类型",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/108217648"
        },
        js_object_getProperty_funcs: {
            "article_id": "109204555",
            "title": "对象属性及方法",
            "description": "一、对象属性理解\n\n\n\n比如：数组中的length属性，描述符对象enumerable值为false，隐式属性。\n\n看下正常的数组和对象的属性：\n\n\n        var arr = [\"a\", true, 3]\n        var obj = {\n            name: \"zhu\",\n            age: 24\n        }\n        console.log(arr)\n        console.log(obj)\n\n\n\n二、关于遍历\n\n1、for...in循环",
            "content": "<p>一、对象属性理解</p>\n\n<p><img alt=\"\" height=\"284\" src=\"https://img-blog.csdnimg.cn/2020102116384135.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"654\" /></p>\n\n<p>比如：数组中的length属性，描述符对象enumerable值为false，隐式属性。</p>\n\n<p>看下正常的数组和对象的属性：</p>\n\n<pre>\n<code class=\"language-javascript\">        var arr = [\"a\", true, 3]\n        var obj = {\n            name: \"zhu\",\n            age: 24\n        }\n        console.log(arr)\n        console.log(obj)</code></pre>\n\n<p><img alt=\"\" height=\"172\" src=\"https://img-blog.csdnimg.cn/20201021164246307.png\" width=\"233\" /></p>\n\n<h3>二、关于遍历</h3>\n\n<p><strong>1、for<code>...</code>in循环（自身显式的属性，意外的获取了原型链上的属性）</strong></p>\n\n<p><strong>只遍历显式属性和原型链上的显式属性，</strong>&nbsp;<strong>当然也可以遍历数组。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var sym = Symbol(\"foo\");\n        var arr = [\"a\", true, 3];\n        var obj = {\n            name: \"zhu\",\n            age: 24,\n            [sym]: \"symbol-foo\",\n            [Symbol(\"bar\")]: \"symbol-bar\"\n        }\n        //age为隐式属性\n        Object.defineProperty(obj, \"age\", {\n            enumerable: false\n        })\n        //原型上的属性\n        obj.__proto__ = {\n            fa: \"father\"\n        }\n        var objStack = [];\n        var arrStack = [];\n        for(var prop in obj){\n            objStack.push(prop)\n        }\n        for(var i in arr){\n            arrStack.push(i)\n        }\n        console.log(objStack)\n        console.log(arrStack)</code></pre>\n\n<pre>\n<code>[\"name\", \"fa\"]\n[\"0\", \"1\", \"2\"]</code></pre>\n\n<p>&nbsp;所以我们在使用for in 遍历的时候，防止遍历到原型链上的属性，我们会用hasOwnProperty方法来判断是不是对象本身的属性</p>\n\n<pre>\n<code class=\"language-javascript\">console.log(obj.hasOwnProperty(\"fa\"))</code></pre>\n\n<p>&nbsp;而 in 方法可以判断属性是不是对象原型及原型链上的属性</p>\n\n<pre>\n<code class=\"language-javascript\">console.log(\"fa\" in obj )</code></pre>\n\n<p><strong><code>2、for...of循环</code></strong>&nbsp;</p>\n\n<p><strong>只遍历iterable类型的值，且处理的是iterable的值，而非属性或键；对象不是iterable类型</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var arrStack = [];\n        var arr = [\"a\", true, 3, sym, Symbol(\"bar\")];\n        for(var i of arr){  \n            arrStack.push(i)\n        }\n        console.log(arrStack) //[\"a\", true, 3, Symbol(foo), Symbol(bar)]</code></pre>\n\n<p><strong>3、Object.keys()方法（自身显式的属性）</strong></p>\n\n<p>返回指定<strong>对象自身</strong>可枚举属性名称组成的数组。</p>\n\n<pre>\n<code class=\"language-javascript\">        var sym = Symbol(\"foo\");\n        var arr = [\"a\", true, 3];\n        var obj = {\n            name: \"zhu\",\n            age: 24,\n            [sym]: \"symbol-foo\",\n            [Symbol(\"bar\")]: \"symbol-bar\"\n        }\n        //age为隐式属性\n        Object.defineProperty(obj, \"age\", {\n            enumerable: false\n        })\n        //原型上的属性\n        obj.__proto__ = {\n            fa: \"father\"\n        }\n\n        console.log(Object.keys(obj)) //[\"name\"]\n        console.log(Object.keys(arr)) //[\"0\", \"1\", \"2\"]</code></pre>\n\n<p><strong>4、Object.getOwnPropertyNames()方法&mdash;&mdash;自身所有的属性</strong></p>\n\n<p>获取对象自身的所有属性，不包含Symbol属性</p>\n\n<p>即自身显式的和隐式的属性</p>\n\n<pre>\n<code class=\"language-javascript\">        var sym = Symbol(\"foo\");\n        var arr = [\"a\", true, 3];\n        var obj = {\n            name: \"zhu\",\n            age: 24,\n            [sym]: \"symbol-foo\",\n            [Symbol(\"bar\")]: \"symbol-bar\"\n        }\n        //age为隐式属性\n        Object.defineProperty(obj, \"age\", {\n            enumerable: false\n        })\n        //原型上的属性\n        obj.__proto__ = {\n            fa: \"father\"\n        }\n        console.log(Object.getOwnPropertyNames(obj)) //[\"name\", \"age\"]\n        console.log(Object.getOwnPropertyNames(arr)) //[\"0\", \"1\", \"2\", \"length\"]</code></pre>\n\n<p><strong>&nbsp;5、Object.getOwnPropertySymbols()方法&mdash;&mdash;自身Symbol属性</strong></p>\n\n<p>获取对象自身的Symbol属性</p>\n\n<pre>\n<code class=\"language-javascript\">        var sym = Symbol(\"foo\");\n        var arr = [\"a\", true, 3];\n        var obj = {\n            name: \"zhu\",\n            age: 24,\n            [sym]: \"symbol-foo\",\n            [Symbol(\"bar\")]: \"symbol-bar\"\n        }\n        //age为隐式属性\n        Object.defineProperty(obj, \"age\", {\n            enumerable: false\n        })\n        //原型上的属性\n        obj.__proto__ = {\n            fa: \"father\"\n        }\n        console.log(Object.getOwnPropertySymbols(obj)) //[Symbol(foo), Symbol(bar)]\n        console.log(Object.getOwnPropertySymbols(arr)) //[]</code></pre>\n\n<h3>三、总结</h3>\n\n<p><strong>（1）for in获取对象的显式属性，因为是早期版本，也会获取原型链上的显式属性</strong></p>\n\n<p><strong>（2）Object.keys()&nbsp; ES的方法，对上面方法进行优化，只获取对象本身的显式属性</strong></p>\n\n<p><strong>（3）Object.getOwnPropertyNames()方法，获取对象自身显式的和隐式的属性</strong></p>\n\n<p><strong>（4）Object.getOwnPropertySymbols()方法，获取对象自身的Symbol属性</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "JavaScript,对象属性",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/109204555"
        },
        js_operational_character_priority: {
            "article_id": "108345985",
            "title": "运算符优先级",
            "description": "下面的表将所有运算符按照优先级的不同从高（20）到低（1）排列。\n\n优先级\n\t\t\t运算类型\n\t\t\t关联性\n\t\t\t运算符\n\t\t20\n\t\t\t圆括号\n\t\t\tn/a（不相关）\n\t\t\t( … )\n\t\t19\n\t\t\t成员访问\n\t\t\t从左到右\n\t\t\t… . …\n\t\t需计算的成员访问\n\t\t\t从左到右\n\t\t\t… [ … ]\n\t\tnew(带参数列表)\n\t\t\tn/a\n\t\t\tnew … ( … )\n\t\t函数调用\n\t\t\t从左到右\n\t\t\t… (…)\n\t\t可选链（Optional chaining）\n\t\t\t从左到右\n\t\t\t?.\n\t\t...",
            "content": "<p>下面的表将所有运算符按照优先级的不同从高（20）到低（1）排列。</p>\n\n<table>\n\t<tbody>\n\t\t<tr>\n\t\t\t<th>优先级</th>\n\t\t\t<th>运算类型</th>\n\t\t\t<th>关联性</th>\n\t\t\t<th>运算符</th>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>20</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Grouping\"><code>圆括号</code></a></td>\n\t\t\t<td>n/a（不相关）</td>\n\t\t\t<td><code>( &hellip; )</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"5\">19</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Property_Accessors#%E7%82%B9%E7%AC%A6%E5%8F%B7%E8%A1%A8%E7%A4%BA%E6%B3%95\"><code>成员访问</code></a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; . &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Property_Accessors#%E6%8B%AC%E5%8F%B7%E8%A1%A8%E7%A4%BA%E6%B3%95\"><code>需计算的成员访问</code></a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; [ &hellip; ]</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/new\"><code>new</code></a>&nbsp;(带参数列表)</td>\n\t\t\t<td>n/a</td>\n\t\t\t<td><code>new &hellip; ( &hellip; )</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Functions\">函数调用</a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; (&nbsp;&hellip;&nbsp;)</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Optional_chaining\">可选链（Optional chaining）</a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>?.</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"1\">18</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/new\">new</a>&nbsp;(无参数列表)</td>\n\t\t\t<td>从右到左</td>\n\t\t\t<td><code>new &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"2\">17</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Increment\">后置递增</a>(运算符在后)</td>\n\t\t\t<td colspan=\"1\" rowspan=\"2\">n/a<br />\n\t\t\t&nbsp;</td>\n\t\t\t<td><code>&hellip; ++</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Decrement\">后置递减</a>(运算符在后)</td>\n\t\t\t<td><code>&hellip; --</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td colspan=\"1\" rowspan=\"10\">16</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Logical_Operators#Logical_NOT\">逻辑非</a></td>\n\t\t\t<td colspan=\"1\" rowspan=\"10\">从右到左</td>\n\t\t\t<td><code>! &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_NOT\">按位非</a></td>\n\t\t\t<td><code>~ &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Unary_plus\">一元加法</a></td>\n\t\t\t<td><code>+ &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Unary_negation\">一元减法</a></td>\n\t\t\t<td><code>- &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Increment\">前置递增</a></td>\n\t\t\t<td><code>++ &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Decrement\">前置递减</a></td>\n\t\t\t<td><code>-- &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/typeof\">typeof</a></td>\n\t\t\t<td><code>typeof &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/void\">void</a></td>\n\t\t\t<td><code>void &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/delete\">delete</a></td>\n\t\t\t<td><code>delete &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await\">await</a></td>\n\t\t\t<td><code>await &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>15</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Exponentiation\">幂</a></td>\n\t\t\t<td>从右到左</td>\n\t\t\t<td><code>&hellip;&nbsp;**&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"3\">14</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Multiplication\">乘法</a></td>\n\t\t\t<td colspan=\"1\" rowspan=\"3\">从左到右<br />\n\t\t\t&nbsp;</td>\n\t\t\t<td><code>&hellip; *&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Division\">除法</a></td>\n\t\t\t<td><code>&hellip; /&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Remainder\">取模</a></td>\n\t\t\t<td><code>&hellip; %&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"2\">13</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Addition\">加法</a></td>\n\t\t\t<td colspan=\"1\" rowspan=\"2\">从左到右<br />\n\t\t\t&nbsp;</td>\n\t\t\t<td><code>&hellip; +&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Subtraction\">减法</a></td>\n\t\t\t<td><code>&hellip; -&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"3\">12</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators\">按位左移</a></td>\n\t\t\t<td colspan=\"1\" rowspan=\"3\">从左到右</td>\n\t\t\t<td><code>&hellip; &lt;&lt;&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators\">按位右移</a></td>\n\t\t\t<td><code>&hellip; &gt;&gt;&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators\">无符号右移</a></td>\n\t\t\t<td><code>&hellip; &gt;&gt;&gt;&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"6\">11</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Less_than_operator\">小于</a></td>\n\t\t\t<td colspan=\"1\" rowspan=\"6\">从左到右</td>\n\t\t\t<td><code>&hellip; &lt;&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Less_than__or_equal_operator\">小于等于</a></td>\n\t\t\t<td><code>&hellip; &lt;=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Greater_than_operator\">大于</a></td>\n\t\t\t<td><code>&hellip; &gt;&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Greater_than_or_equal_operator\">大于等于</a></td>\n\t\t\t<td><code>&hellip; &gt;=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/in\">in</a></td>\n\t\t\t<td><code>&hellip; in&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/instanceof\">instanceof</a></td>\n\t\t\t<td><code>&hellip; instanceof&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"4\">10</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Equality\">等号</a></td>\n\t\t\t<td colspan=\"1\" rowspan=\"4\">从左到右<br />\n\t\t\t&nbsp;</td>\n\t\t\t<td><code>&hellip; ==&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Inequality\">非等号</a></td>\n\t\t\t<td><code>&hellip; !=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Identity\">全等号</a></td>\n\t\t\t<td><code>&hellip; ===&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comparison_Operators#Nonidentity\">非全等号</a></td>\n\t\t\t<td><code>&hellip; !==&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>9</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_AND\">按位与</a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; &amp;&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>8</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_XOR\">按位异或</a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; ^&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>7</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_OR\">按位或</a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; |&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>6</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Logical_Operators#Logical_AND\">逻辑与</a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; &amp;&amp;&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>5</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Logical_Operators#Logical_OR\">逻辑或</a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; ||&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>4</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Conditional_Operator\">条件运算符</a></td>\n\t\t\t<td>从右到左</td>\n\t\t\t<td><code>&hellip; ? &hellip; : &hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td rowspan=\"12\">3</td>\n\t\t\t<td rowspan=\"12\"><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Assignment_Operators\">赋值</a></td>\n\t\t\t<td rowspan=\"12\">从右到左</td>\n\t\t\t<td><code>&hellip; =&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; +=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; -=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; *=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; /=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; %=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; &lt;&lt;=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; &gt;&gt;=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; &gt;&gt;&gt;=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; &amp;=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; ^=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>&hellip; |=&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td colspan=\"1\" rowspan=\"2\">2</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/yield\">yield</a></td>\n\t\t\t<td colspan=\"1\" rowspan=\"2\">从右到左</td>\n\t\t\t<td><code>yield&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/yield*\">yield*</a></td>\n\t\t\t<td><code>yield*&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>1</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Spread_operator\">展开运算符</a></td>\n\t\t\t<td>n/a</td>\n\t\t\t<td><code>...</code>&nbsp;&hellip;</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>0</td>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Comma_Operator\">逗号</a></td>\n\t\t\t<td>从左到右</td>\n\t\t\t<td><code>&hellip; ,&nbsp;&hellip;</code></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>实例（1）</p>\n\n<pre>\n<code class=\"language-javascript\">return typeof obj === \"object\" || typeof obj === \"function\" ?\n\t\t\tclass2type[toString.call(obj)] || \"object\" :\n\t\t\ttypeof obj;</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "运算符优先级",
            "categories": "JavaScript",
            "type": "repost",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Operator_Precedence",
            "authorized_status": true,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/108345985"
        },
        js_json: {
            "article_id": "108221971",
            "title": "JSON对象",
            "description": "1、JSON这个对象除了parse()和stringify()方法，本身并没有其他作用，也不能被调用或者作为构造函数调用。\n\nJSON是一种语法，用来序列化对象、数组、数值、字符串、布尔值和null。JavaScript不是JSON，JSON也不是JavaScript。\n\n兼容IE8\n\n2、JSON.parse()方法用来解析JSON字符串，构造由字符串描述的JavaScript值或对象。提供可选的reviver函数用以在返回之前对所得到的对象执行变换(操作)。\n\n如果指定了reviver...",
            "content": "<p>1、JSON这个对象除了parse()和<code>stringify()</code>&nbsp;方法，本身并没有其他作用，也不能被调用或者作为构造函数调用。</p>\n\n<p><strong>JSON</strong>&nbsp;是一种语法，用来序列化对象、数组、数值、字符串、布尔值和&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null\"><code>null</code></a>&nbsp;。<strong>JavaScript不是JSON，JSON也不是JavaScript。</strong></p>\n\n<h3><strong><code>JSON.stringify()</code>将值转换为相应的JSON格式：</strong></h3>\n\n<p>我们要把一个对象传给后端，在传值的时候，就要把这个对象JSON字符串化，JSON字符串化的特点：</p>\n\n<p>（1）会将对象中各种进制的数字转换为十进制；</p>\n\n<p>（2）undefined会转为null；</p>\n\n<p>（3）无视函数；</p>\n\n<p><strong>If undefined, a function, or a symbol is encountered during conversion it is either omitted (when it is found in an object) or censored to null (when it is found in an array).</strong></p>\n\n<p><strong>如果在转换过程中遇到未定义的函数或符号，它要么被省略(当它在对象中找到时)，要么被审查为空(当它在数组中找到时)。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {\n            bool: true,\n            obj1: null, //空对象\n            num1: 0xa, //16进制\n            num2: 0o7, //8进制\n            num3: 0b1, //2进制\n            obj2: {\n                name: \"zhu\",\n                age: 24\n            },\n            arr: [\"zhu\", false, 24, undefined, null],\n            obj3: {\n                func: function () {\n                    console.log(1)\n                }\n            } //最后一项不能有,结尾\n        }\n        var objJsonStr = JSON.stringify(obj)\n        console.log(objJsonStr);\n        console.log(JSON.parse(objJsonStr))</code></pre>\n\n<p><img alt=\"\" height=\"237\" src=\"https://img-blog.csdnimg.cn/20201020150949558.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"864\" /></p>\n\n<p><strong>兼容IE8</strong></p>\n\n<p><code><strong>2、JSON.parse()</strong></code>&nbsp;方法用来解析JSON字符串，构造由字符串描述的JavaScript值或对象。提供可选的&nbsp;<strong>reviver</strong>&nbsp;函数用以在返回之前对所得到的对象执行变换(操作)。</p>\n\n<p>如果指定了&nbsp;<code>reviver</code>&nbsp;函数，则解析出的 JavaScript 值（解析值）会经过一次转换后才将被最终返回（返回值）。更具体点讲就是：解析值本身以及它所包含的所有属性，会按照一定的顺序（从最最里层的属性开始，一级级往外，最终到达顶层，也就是解析值本身）分别的去调用&nbsp;<code>reviver</code>&nbsp;函数，在调用过程中，当前属性所属的对象会作为&nbsp;<code>this</code>&nbsp;值，当前属性名和属性值会分别作为第一个和第二个参数传入&nbsp;<code>reviver</code>&nbsp;中。如果&nbsp;<code>reviver</code>&nbsp;返回&nbsp;<code>undefined</code>，则当前属性会从所属对象中删除，如果返回了其他值，则返回的值会成为当前属性新的属性值。</p>\n\n<p>当遍历到最顶层的值（解析值）时，传入&nbsp;<code>reviver</code>&nbsp;函数的参数会是空字符串&nbsp;<code>&quot;&quot;</code>（因为此时已经没有真正的属性）和当前的解析值（有可能已经被修改过了），当前的&nbsp;<code>this</code>&nbsp;值会是&nbsp;<code>{&quot;&quot;: 修改过的解析值}</code>，在编写&nbsp;<code>reviver</code>&nbsp;函数时，要注意到这个特例。（这个函数的遍历顺序依照：从最内层开始，按照层级顺序，依次向外遍历）</p>\n\n<p>没有返回值的情况：</p>\n\n<pre>\n<code class=\"language-javascript\">var josnStr = '{\"a\": 1,\"b\": 2}';//JSON字符串是字符串\nvar jsonPaser = JSON.parse(josnStr,function(key, value){\n    console.log(key,value)\n})\nconsole.log(jsonPaser)//undefined\na 1\nb 2\n  {}\n</code></pre>\n\n<p>&nbsp;有返回值的情况：</p>\n\n<pre>\n<code class=\"language-javascript\">var josnStr = '{\"a\": 1,\"b\": 2}';//JSON字符串是字符串\nvar jsonPaser = JSON.parse(josnStr,function(key, value){\n    console.log(key,value)\n    return value;\n})\nconsole.log(jsonPaser) //{a: 1, b: 2}\n\na 1\nb 2\n  {a: 1, b: 2}\n\n</code></pre>\n\n<p><code>JSON.parse()</code>&nbsp;不允许用逗号作为结尾</p>\n\n<p>以下都会报错</p>\n\n<pre>\n<code class=\"language-javascript\">// both will throw a SyntaxError\nJSON.parse(\"[1, 2, 3, 4, ]\");\nJSON.parse('{\"foo\" : 1, }');</code></pre>\n\n<h3>3、&nbsp;<code><strong>JSON.stringify()</strong></code>&nbsp;方法</h3>\n\n<p><strong>重要的方法，在前端往后端传值时，需调用该方法</strong></p>\n\n<p>将一个 JavaScript&nbsp;对象或值转换为 JSON 字符串，如果指定了一个 replacer 函数，则可以选择性地替换值，或者指定的&nbsp;replacer 是数组，则可选择性地仅包含数组指定的属性。</p>\n\n<p><strong>如果第二个参数是一个数组：只有包含在这个数组中的属性名才会被序列化到最终的 JSON 字符串中。仔细观察以下几个实例</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var obj = {\n    a: 1,\n    b: 2,\n    c: 3,\n    d :{\n        m: 0\n    }\n}\nvar jsonStr = JSON.stringify(obj, [\"a\",\"b\"])\nconsole.log(jsonStr) //'{\"a\":1,\"b\":2}'</code></pre>\n\n<pre>\n<code class=\"language-javascript\">var obj = {\n    a: 1,\n    b: 2,\n    c: 3,\n    d :{\n        m: 0\n    }\n}\nvar jsonStr = JSON.stringify(obj, [\"a\",\"b\",\"d\"])\nconsole.log(jsonStr) //'{\"a\":1,\"b\":2,\"d\":{}}'</code></pre>\n\n<pre>\n<code class=\"language-javascript\">var obj = {\n    a: 1,\n    b: 2,\n    c: 3,\n    d :{\n        m: 0\n    }\n}\nvar jsonStr = JSON.stringify(obj, [\"a\",\"b\",\"m\"])\nconsole.log(jsonStr)   //'{\"a\":1,\"b\":2}'</code></pre>\n\n<pre>\n<code class=\"language-javascript\">var obj = {\n    a: 1,\n    b: 2,\n    c: 3,\n    d :{\n        m: 0\n    }\n}\nvar jsonStr = JSON.stringify(obj, [\"a\",\"b\",\"d\",\"m\"])\nconsole.log(jsonStr) //'{\"a\":1,\"b\":2,\"d\":{\"m\":0}}'</code></pre>\n\n<p><strong>如果该参数是一个函数，则在序列化过程中，被序列化的值的每个属性都会经过该函数的转换和处理</strong></p>\n\n<p>&nbsp;<strong>由外层到内层处理每个属性和属性值，特别注意，函数的下一次处理，是基于函数上一次处理的返回值的。</strong>看下面例子：</p>\n\n<pre>\n<code class=\"language-javascript\">var obj = {\n    a: 1,\n    b: 2,\n    c: 3,\n    d :{\n        m: 0\n    }\n}\nvar jsonStr = JSON.stringify(obj, function(key, value){\n    console.log(key, value) // \" \" {a: 1, b: 2, c: 3, d: {…}}\n\n})\nconsole.log(jsonStr)//undefined</code></pre>\n\n<p><strong>如果函数每次处理都有返回值：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var obj = {\n    a: 1,\n    b: 2,\n    c: 3,\n    d :{\n        m: 0\n    }\n}\nvar jsonStr = JSON.stringify(obj, function(key, value){\n    console.log(key, value)\n    return value;\n})\nconsole.log(jsonStr)</code></pre>\n\n<p><strong>结果：遍历的时候会处理值为函数的项，但返回的结果中，不包含函数项&nbsp;</strong></p>\n\n<p><img alt=\"\" height=\"154\" src=\"https://img-blog.csdnimg.cn/20200825161852711.png\" width=\"302\" /></p>\n\n<h3><strong><code>JSON.stringify()</code>将值转换为相应的JSON格式：</strong></h3>\n\n<ul>\n\t<li>转换值如果有 toJSON() 方法，该方法定义什么值将被序列化。</li>\n\t<li><strong>非数组对象的属性不能保证以特定的顺序出现在序列化后的字符串中。</strong></li>\n\t<li>布尔值、数字、字符串的包装对象在序列化过程中会自动转换成对应的原始值。</li>\n\t<li><code>undefined</code>、任意的函数以及 symbol 值，在序列化过程中会被忽略（出现在非数组对象的属性值中时）或者被转换成&nbsp;<code>null</code>（出现在数组中时）。函数、undefined 被单独转换时，会返回 undefined，如<code>JSON.stringify(function(){})</code>&nbsp;or&nbsp;<code>JSON.stringify(undefined)</code>.</li>\n\t<li>对包含循环引用的对象（对象之间相互引用，形成无限循环）执行此方法，会抛出错误。</li>\n\t<li>所有以 symbol 为属性键的属性都会被完全忽略掉，即便&nbsp;<code>replacer</code>&nbsp;参数中强制指定包含了它们。</li>\n\t<li>Date 日期调用了 toJSON() 将其转换为了 string 字符串（同Date.toISOString()），因此会被当做字符串处理。</li>\n\t<li>NaN 和 Infinity 格式的数值及 null 都会被当做 null。</li>\n\t<li>其他类型的对象，包括 Map/Set/WeakMap/WeakSet，仅会序列化可枚举的属性。</li>\n</ul>\n\n<p><strong>对象的属性是无序的，Chrome浏览器会按属性顺序执行，但是不需要关注顺序，重要的是每个属性都会执行一次。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">        var obj = {\n            a:1,\n            d:{\n                m:10,\n                n: function(){\n\n                }\n            },\n            b: 2,\n            c: 3\n        }\n        var jsonstr = JSON.stringify(obj, function(k, v){\n            console.log(k,v);\n            return v;\n        })</code></pre>\n\n<p><img alt=\"\" height=\"173\" src=\"https://img-blog.csdnimg.cn/20200828111513478.png\" width=\"234\" /></p>\n\n<h3>注意：</h3>\n\n<p>（1）只有对象的可枚举属性，才会被JSON处理</p>\n\n<p>（2）不能用 replacer 方法，从数组中移除值（values），如若返回 undefined 或者一个函数，将会被 null 取代。</p>\n\n<p>（3）JSON处理的值是数组时，第二个参数是数组来选择属性是无效的。</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "javascript,JSON对象",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/108221971"
        },
        js_internal_date: {
            "article_id": "108201728",
            "title": "内置日期对象Date",
            "description": "unix时间戳\n\n是从1970年1月1日（UTC/GMT的午夜）开始所经过的秒数，不考虑闰秒。在大多数的UNIX系统中UNIX时间戳存储为32位，这样会引发2038年问题或Y2038。\n\nUTC协调世界时\n\n协调世界时，又称世界统一时间、世界标准时间、国际协调时间。由于英文（CUT）和法文（TUC）的缩写不同，作为妥协，简称UTC。\n\n原子时：IAT（international atomic time），以物质的原子内部发射的电磁振荡频率为基准的时间计量系统。\n\n世界时UT[1]即格林尼治[1]...",
            "content": "<h3>unix时间戳</h3>\n\n<p>是从1970年1月1日（UTC/GMT的午夜）开始所经过的秒数，不考虑闰秒。在大多数的UNIX系统中UNIX时间戳存储为32位，这样会引发2038年问题或Y2038。</p>\n\n<h3>UTC协调世界时</h3>\n\n<p>协调世界时，又称世界统一时间、世界标准时间、国际协调时间。由于英文（CUT）和法文（TUC）的缩写不同，作为妥协，简称UTC。</p>\n\n<p>原子时：IAT（international atomic time），以物质的原子内部发射的电磁振荡频率为基准的时间计量系统。</p>\n\n<p><a href=\"https://baike.baidu.com/item/%E4%B8%96%E7%95%8C%E6%97%B6\">世界时</a><a href=\"https://baike.baidu.com/item/UT/4044923\">UT</a>&nbsp;[1]<a name=\"ref_[1]_37429\">&nbsp;</a>&nbsp;即<a href=\"https://baike.baidu.com/item/%E6%A0%BC%E6%9E%97%E5%B0%BC%E6%B2%BB/3065623\">格林尼治</a>&nbsp;[1]<a>&nbsp;</a>&nbsp;平太阳时间，是指格林尼治所在地的标准时间，也是表示地球自转速率的一种形式。</p>\n\n<p>协调世界时是以<a href=\"https://baike.baidu.com/item/%E5%8E%9F%E5%AD%90%E6%97%B6/692466\">原子时</a>秒长为基础，在时刻上尽量接近于<a href=\"https://baike.baidu.com/item/%E4%B8%96%E7%95%8C%E6%97%B6/692237\">世界时</a>的一种时间计量系统。</p>\n\n<h3>Date对象</h3>\n\n<p>new操作符的优先级最高！！！</p>\n\n<pre>\n<code class=\"language-javascript\">console.log( new Date().getFullYear())</code></pre>\n\n<p>创建一个 JavaScript&nbsp;<code>Date</code>&nbsp;实例，该实例呈现时间中的某个时刻。<code>Date</code>&nbsp;对象则基于&nbsp;<a href=\"http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap04.html#tag_04_16\">Unix Time Stamp</a>，即自1970年1月1日（UTC）起经过的毫秒数。</p>\n\n<pre>\n<code class=\"language-javascript\">new Date();   Mon Aug 24 2020 16:45:42 GMT+0800 (中国标准时间)\n\nnew Date(value);\nnew Date(1234567891011)  Sat Feb 14 2009 07:31:31 GMT+0800 (中国标准时间)\n\nnew Date(dateString);\nnew Date(\"Sat Feb 14 2020 GMT+0800 (中国标准时间)\")\nFri Feb 14 2020 00:00:00 GMT+0800 (中国标准时间)\n\nnew Date(year, monthIndex [, day [, hours [, minutes [, seconds [, milliseconds]]]]]);\nnew Date(2020,8,25,09)\nFri Sep 25 2020 09:00:00 GMT+0800 (中国标准时间)</code></pre>\n\n<p><strong>Date对象是一个标准时间！！</strong>&nbsp;</p>\n\n<ul>\n\t<li>JavaScript的时间由世界标准时间（UTC）1970年1月1日开始，用毫秒计时，一天由 86,400,000 毫秒组成。<code>Date</code>&nbsp;对象的范围是 -100,000,000 天至 100,000,000 天（等效的毫秒值）。</li>\n\t<li><code>Date</code>&nbsp;对象为跨平台提供了统一的行为。时间属性可以在不同的系统中表示相同的时刻，而如果使用了本地时间对象，则反映当地的时间。</li>\n\t<li><code>Date</code>&nbsp;对象支持多个处理 UTC 时间的方法，也相应地提供了应对当地时间的方法。UTC，也就是我们所说的格林威治时间，指的是time中的世界时间标准。而当地时间则是指执行JavaScript的客户端电脑所设置的时间。</li>\n\t<li>如果没有输入任何参数，则Date的构造器会依据系统设置的<strong>当前时间</strong>来创建一个Date对象。</li>\n\t<li>&nbsp;</li>\n\t<li>以一个函数的形式来调用&nbsp;<code>Date</code>&nbsp;对象（即不使用&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/new\"><code>new</code></a>&nbsp;操作符）会返回一个代表当前日期和时间的字符串。</li>\n\t<li>可以传入一个毫秒数，来生成一个日期对象。</li>\n\t<li>可以传入一个日期，来生成日期对象。</li>\n\t<li>如果提供了至少两个参数，其余的参数均会默认设置为 1（如果没有指定 day 参数）或者 0（如果没有指定 day 以外的参数）。</li>\n</ul>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date/prototype\"><code>Date.prototype</code></a>&nbsp; 允许为&nbsp;<code>Date</code>&nbsp;对象添加属性。所有的&nbsp;<code>Date</code>&nbsp;实例都继承自&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date/prototype\"><code>Date.prototype</code></a>。修改&nbsp;<code>Date&nbsp;</code>构造函数的原型对象会影响到所有的&nbsp;<code>Date</code>&nbsp;实例。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date/now\"><code>Date.now()</code></a>返回自 1970-1-1 00:00:00 &nbsp;UTC（世界标准时间）至今所经过的毫秒数。</p>\n\n<p>Date.now()获取当前时间的毫秒数，而getTime()和valueOf()获取的是日期对象的毫秒数。我们可以通过new Date().getTime() 来获取当前日期对象的毫秒数，等同于Date.now()。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date/parse\"><code>Date.parse()</code></a>解析一个表示日期的字符串，并返回从 1970-1-1 00:00:00 所经过的毫秒数。</p>\n\n<pre>\n<code class=\"language-javascript\">Date.parse(dateString)  ===  new Date(dateString).getTime()</code></pre>\n\n<p><strong>注意:</strong>&nbsp;由于浏览器差异和不一致，强烈建议不要使用<code>Date.parse</code>解析字符串。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date/UTC\"><code>Date.UTC()</code></a>接受和构造函数最长形式的参数相同的参数（从2到7），并返回从 1970-01-01 00:00:00 UTC 开始所经过的毫秒数。</p>\n\n<h3>&nbsp;以下是原型Date.prototype上的方法</h3>\n\n<p><code><strong>getTime()</strong></code>&nbsp;方法返回一个时间对象的数值。<code>getTime</code>&nbsp;方法的返回值一个数值，表示从1970年1月1日0时0分0秒（UTC，即协调世界时）距离该日期对象所代表时间的毫秒数。这个方法的功能和&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date/valueof\"><code>valueOf()</code></a>&nbsp;方法一样。</p>\n\n<p><code><strong>setTime()</strong></code>&nbsp;方法以一个表示从1970-1-1 00:00:00 UTC计时的毫秒数为来为&nbsp;<code>Date</code>&nbsp;对象设置时间。</p>\n\n<h3><strong>获取和设置&mdash;&mdash;年</strong></h3>\n\n<p><strong><code>getFullYear()</code></strong>&nbsp;方法根据本地时间返回指定日期的年份。<code>getYear</code>&nbsp;不返回千禧年[full years] (&quot;year 2000 problem&quot;)，所以这个方法不再被使用。</p>\n\n<p><code><strong>setFullYear()</strong></code>&nbsp;方法根据本地时间为一个日期对象设置年份。</p>\n\n<pre>\n<code>dateObj.setFullYear(yearValue[, monthValue[, dayValue]])</code></pre>\n\n<h3><strong>&nbsp;获取和设置&mdash;&mdash;月</strong></h3>\n\n<p>getMonth() 根据本地时间，返回一个指定的日期对象的月份，为基于0的值（0表示一年中的第一月）。返回一个0 到 11的整数值： 0 代表一月份，1 代表二月份， 2 代表三月份，依次类推。&nbsp;</p>\n\n<p><code><strong>setMonth()</strong></code>&nbsp;方法根据本地时间为一个设置年份的日期对象设置月份。</p>\n\n<pre>\n<code>dateObj.setMonth(monthValue[, dayValue])</code></pre>\n\n<h3><strong>获取和设置&mdash;&mdash;日</strong></h3>\n\n<p><code>getDate()&nbsp;</code>返回一个1 到 31的整数值。</p>\n\n<p><code><strong>setDate()</strong></code>&nbsp;方法根据本地时间来指定一个日期对象的天数。</p>\n\n<pre>\n<code>dateObj.setDate(<em>dayValue</em>)</code></pre>\n\n<h3>获取&mdash;&mdash;周</h3>\n\n<p><strong><code>getDay()</code></strong>&nbsp;方法根据本地时间，返回一个具体日期中一周的第几天，0 表示星期天。</p>\n\n<p>不能设置周！</p>\n\n<h3>获取和设置&mdash;&mdash;时</h3>\n\n<p><strong><code>getHours()</code></strong>&nbsp;方法根据本地时间，返回一个指定的日期对象的小时。<code>getHours</code>返回一个0 到 23之间的整数值。</p>\n\n<p><code><strong>setHours()</strong></code>&nbsp;方法根据本地时间为一个日期对象设置小时数，返回从1970-01-01 00:00:00 UTC 到更新后的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Date\"><code>日期</code></a>&nbsp;对象实例所表示时间的毫秒数。</p>\n\n<pre>\n<code class=\"language-html\">dateObj.setHours(hoursValue[, minutesValue[, secondsValue[, msValue]]])</code></pre>\n\n<h3>获取和设置&mdash;&mdash;分</h3>\n\n<p><strong><code>getMinutes()</code></strong>&nbsp;方法根据本地时间，返回一个指定的日期对象的分钟数。<code>getMinutes&nbsp;</code>返回一个0 到 59的整数值。</p>\n\n<p><code><strong>setMinutes()</strong></code>&nbsp;方法根据本地时间为一个日期对象设置分钟数。</p>\n\n<pre>\n<code class=\"language-html\">dateObj.setMinutes(minutesValue[, secondsValue[, msValue]])</code></pre>\n\n<h3><strong>获取和设置&mdash;&mdash;秒</strong></h3>\n\n<p><strong><code>getSeconds()</code></strong>&nbsp;方法根据本地时间，返回一个指定的日期对象的秒数。该方法返回一个 0 到 59 的整数值。</p>\n\n<p><code><strong>setSeconds()</strong></code>&nbsp;方法根据本地时间设置一个日期对象的秒数。</p>\n\n<pre>\n<code class=\"language-html\">dateObj.setSeconds(secondsValue[, msValue])</code></pre>\n\n<p><code>参数：secondsValue</code>&nbsp;一个 0 到 59 的整数。<code>msValue</code>&nbsp; 一个 0 到 999 的数字，表示微秒数。</p>\n\n<p>获取和设置&mdash;&mdash;毫秒</p>\n\n<p>getMilliseconds() 方法，根据本地时间，返回一个指定的日期对象的毫秒数。<code>getMilliseconds()&nbsp;</code>方法返回一个0 到 999的整数。</p>\n\n<p><code><strong>setMilliseconds()</strong></code>&nbsp;方法会根据本地时间设置一个日期对象的豪秒数。</p>\n\n<pre>\n<code class=\"language-html\">dateObj.setMilliseconds(millisecondsValue)</code></pre>\n\n<p>参数：<code>millisecondsValue</code>&nbsp; 一个 0 到 999 的数字，表示豪秒数。</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "日期对象Date",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/108201728"
        },
        js_string_funcs: {
            "article_id": "108101273",
            "title": "String字符串方法",
            "description": "字符串的方法都不会改变原字符串\n\n1、charAt()方法从一个字符串中返回指定的字符。\n\n\nstr.charAt(index)\n\n字符串中的字符从左向右索引，第一个字符的索引值为 0，最后一个字符（假设该字符位于字符串 stringName 中）的索引值为stringName.length - 1。如果指定的 index 值超出了该范围，则返回一个空字符串。\n\n\n...",
            "content": "<h3><strong>字符串的方法都不会改变原字符串，因为JavaScript中字符串的值是不可改变的。</strong></h3>\n\n<p><strong><code>0、String</code></strong>&nbsp;全局对象是一个用于字符串或一个字符序列的构造函数。</p>\n\n<p>三个知识点至关重要：</p>\n\n<p>（1）构造字符串的形式，构造函数或字面量</p>\n\n<pre>\n<code class=\"language-javascript\">var str1 = new String(\"abcd\");\nvar str2 = \"abcd\";</code></pre>\n\n<p><img alt=\"\" height=\"155\" src=\"https://img-blog.csdnimg.cn/20200821194548285.png\" width=\"231\" /></p>\n\n<p>ES6的模板字面量：</p>\n\n<pre>\n<code class=\"language-javascript\">`hello world` `hello! world!` `hello ${who}` escape `&lt;a&gt;${who}&lt;/a&gt;`</code></pre>\n\n<p>（2）String()转为字符串比toSting()方法更好</p>\n\n<p>因为不同类型的数值在调用toSting方法的时候，都是调用他们自己重写的toString方法，而undefined和null没有该方法，不可以通过toString方法转为字符串。但是String()不一样，将数据传入String()方法中，都可以转为字符串，甚至是undefined和null。</p>\n\n<p>（3）转义字符在字符串中代表特殊含义<strong>（字符串中，谨慎使用哦）</strong></p>\n\n<table>\n\t<thead>\n\t\t<tr>\n\t\t\t<th scope=\"col\">Code</th>\n\t\t\t<th scope=\"col\">Output</th>\n\t\t</tr>\n\t</thead>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td><code>\\0</code></td>\n\t\t\t<td>空字符</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong><code>\\&#39;</code></strong></td>\n\t\t\t<td><strong>单引号</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong><code>\\&quot;</code></strong></td>\n\t\t\t<td><strong><code>双引号</code></strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong><code>\\\\</code></strong></td>\n\t\t\t<td><strong>反斜杠</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>\\n</code></td>\n\t\t\t<td>换行</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>\\r</code></td>\n\t\t\t<td><code>回车</code></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>\\v</code></td>\n\t\t\t<td>垂直制表符</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>\\t</code></td>\n\t\t\t<td>水平制表符</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>\\b</code></td>\n\t\t\t<td>退格</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>\\f</code></td>\n\t\t\t<td>换页</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>\\uXXXX</code></td>\n\t\t\t<td>unicode 码</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p><strong>1、charAt()&nbsp;方法从一个字符串中返回指定索引的字符。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">str.charAt(index)</code></pre>\n\n<p>字符串中的字符从左向右索引，第一个字符的索引值为 0，最后一个字符（假设该字符位于字符串 stringName 中）的索引值为&nbsp;<code>stringName.length - 1</code>。&nbsp;如果指定的 index 值超出了该范围，则返回一个空字符串。</p>\n\n<p><strong>2、<code>charCodeAt()</code>&nbsp;方法返回&nbsp;<code>0</code>&nbsp;到&nbsp;<code>65535</code>&nbsp;之间的整数，表示给定索引处的 UTF-16 代码单元</strong></p>\n\n<p>UTF-16 编码单元匹配能用一个 UTF-16 编码单元表示的 Unicode 码点。如果 Unicode 码点不能用一个&nbsp;UTF-16&nbsp;编码单元表示（因为它的值大于<code>0xFFFF</code>），则所返回的编码单元会是这个码点代理对的第一个编码单元) 。</p>\n\n<p><strong>注意：\\u不能和变量拼接，要直接和码值拼接</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"我是谁\";\nvar code10 = str.charCodeAt(0); //25105\nvar code10to16 = code10.toString(16); //6211\nconsole.log(\"\\u6211\") //我</code></pre>\n\n<h3><strong>关于进制的转换</strong></h3>\n\n<p><strong>（1）数字的toString方法，</strong>可以接收一个参数，表明转为多少进制的字符串。不传参数，默认把数字转为10进制的字符串，就是把数字类型转为同样值的字符串类型</p>\n\n<p>数字默认转为同等值的字符串：</p>\n\n<pre>\n<code class=\"language-javascript\">console.log((65535).toString())  65535</code></pre>\n\n<p>数字转为指定进制的字符串：</p>\n\n<pre>\n<code class=\"language-javascript\">console.log((65535).toString(16)) ffff</code></pre>\n\n<p><strong>（2）parseInt方法：输出十进制整数</strong>，可以指定该数字是多少进制的数字，即将多少进制的数字转为十进制数字</p>\n\n<pre>\n<code class=\"language-javascript\">console.log(parseInt(\"FFFF\",16))  65535</code></pre>\n\n<p>如果该数字自身有指定类型，不必再指定，如果再指定，将会进行两次转换</p>\n\n<pre>\n<code class=\"language-javascript\">console.log(parseInt(0xffff))  65535</code></pre>\n\n<p>如果再指定进制，0xffff转为十进制的数字，再按指定进制表明该数字是多少进制数字，再转为10进制</p>\n\n<p>比如：0xffff是十进制的65535，而又指明65535为32进制，那么32进制的65535输出的10进制数是6460517</p>\n\n<pre>\n<code class=\"language-javascript\">console.log(parseInt(0xffff,32))  6460517</code></pre>\n\n<p><strong>3、<code>concat()</code>&nbsp;方法将一个或多个字符串与原字符串连接合并，形成一个新的字符串并返回。如果参数不是字符串类型，它们在连接之前将会被转换成字符串。</strong></p>\n\n<p><strong>字符串在处理每项值的时候，都会调用String(），该项值转为字符串</strong></p>\n\n<p>关于性能：强烈建议使用<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Assignment_Operators\">赋值操作符</a>（<code>+</code>,&nbsp;<code>+=</code>）代替&nbsp;<code>concat</code>&nbsp;方法。</p>\n\n<pre>\n<code class=\"language-javascript\">console.log(undefined + \"\" + null); //undefinednull</code></pre>\n\n<p>如果要把每项转为字符串后再进行拼接，那么每项都会被String()方法转为字符串，包括undefined和null值</p>\n\n<pre>\n<code class=\"language-javascript\">String([\"a\",[\"b\"]]) //a,b\nString(undefined); //undefined</code></pre>\n\n<p>由于undefined和null没有toString方法，所以此处不是调用自己的toString方法（不同类型的值实现的toString方法不同）</p>\n\n<pre>\n<code class=\"language-javascript\">console.log([\"a\",[\"b\"]].toString())  //a,b\nconsole.log(undefined.toString())  //TypeError: Cannot read property 'toString' of undefined</code></pre>\n\n<p>回到concat方法，例如：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"a\";\nconsole.log(str.concat(1, [\"a\",[\"b\"]], true, undefined,null));\na1a,btrueundefinednull</code></pre>\n\n<p>&nbsp;拼接时，大数组中的每项会按字符串的特性处理，但是每项中的项，按该项值的特性处理：</p>\n\n<p><strong>数组在处理undefined和null为字符串时，将其转换为空字符。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">console.log(\"\".concat(1, [\"a\",undefined,[\"b\"]], true, undefined,null));\n\n1a,,btrueundefinednull</code></pre>\n\n<p><strong>拼接字符串，实际上是对每项执行String方法，不管是concat方法拼接还是 + 拼接。&nbsp;</strong></p>\n\n<p><strong><code>4、indexOf()</code>&nbsp;方法返：回字符串中，指定片段的第一次出现的index索引值。从&nbsp;<code>fromIndex</code>&nbsp;处进行搜索。如果未找到该值，则返回 -1。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.indexOf(\"ab\")) //0</code></pre>\n\n<p>&nbsp;可以接收第二个参数，index值，表明从字符串的index位置开始查找</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.indexOf(\"ab\",2)) //-1</code></pre>\n\n<p>&nbsp;<strong><code>lastIndexOf()</code></strong>&nbsp;方法返回调用<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/String\"><code>String</code></a>&nbsp;对象的指定值<strong>最后的索引</strong>，在一个字符串中的指定位置&nbsp;<code>fromIndex</code>处<strong>从后向前搜索。</strong>如果没找到这个特定值则返回-1 。</p>\n\n<p>&nbsp;<strong><code>lastIndexOf()</code></strong>该方法将从尾到头地检索字符串&nbsp;<em>str</em>，看它是否含有子串&nbsp;<em>searchValue</em>。开始检索的位置在字符串的&nbsp;<em>fromIndex</em>&nbsp;处或字符串的结尾（没有指定&nbsp;<em>fromIndex</em>&nbsp;时）。返回检索到的index值</p>\n\n<p><strong>从结尾开始检索，或者从指定的index处开始往头检索</strong></p>\n\n<p>&nbsp;</p>\n\n<p><strong>&nbsp;5、<code>localeCompare()</code>&nbsp;方法返回一个数字来比较两个字符串的第一项字符在本地排序中是在之前还是在之后。</strong></p>\n\n<p><strong><code>localeCompare()方法的兼容性比较好，但是该方法里面的还可以接收两个参数，参数的兼容性是IE11</code></strong></p>\n\n<p>返回一个数字表示是否&nbsp;<strong>引用字符串</strong>&nbsp;在排序中位于&nbsp;<strong>比较字符串</strong>&nbsp;的前面，后面，或者二者相同。</p>\n\n<ul>\n\t<li>当&nbsp;<strong>引用字符串&nbsp;</strong>在&nbsp;<strong>比较字符串&nbsp;</strong>前面时返回 -1</li>\n\t<li>当&nbsp;<strong>引用字符串</strong>&nbsp;在&nbsp;<strong>比较字符串&nbsp;</strong>后面时返回 1</li>\n\t<li>相同位置时返回 0</li>\n</ul>\n\n<p><strong>切勿依赖于 -1 或 1 这样特定的返回值。</strong>不同浏览器之间（以及不同浏览器版本之间）<strong>&nbsp;</strong>返回的正负数的值各有不同，因为W3C规范中只要求返回值是正值和负值，而没有规定具体的值。一些浏览器可能返回-2或2或其他一些负的、正的值</p>\n\n<p>比如：字符串&ldquo;abc&rdquo;和字符串&ldquo;xyz&rdquo;相比，a在x前面，所以返回-1&nbsp;</p>\n\n<pre>\n<code class=\"language-javascript\">var str1 = \"abc\";\nvar str2 = \"xyz\";\nconsole.log(str1.localeCompare(str2))   -1</code></pre>\n\n<p>&nbsp;比如：汉字的对比，第一个字符的拼音首字母</p>\n\n<pre>\n<code class=\"language-javascript\">var str1 = \"我wo\";\nvar str2 = \"你ni\";\nconsole.log(str1.localeCompare(str2))  1</code></pre>\n\n<p>&nbsp;比如：数字的对比，字符串形式的数字进行对比，也是获取数字的第一个字符 12中的1 在 4的前面</p>\n\n<p>注意：数字不可以直接调用localeCompare方法，因为这是字符串方法</p>\n\n<pre>\n<code class=\"language-javascript\">var str1 = \"12\";\nvar str2 = \"4\";\nconsole.log(str1.localeCompare(str2))  -1</code></pre>\n\n<p>&nbsp;刚好可以结合数组的sort方法，进行排序：</p>\n\n<pre>\n<code class=\"language-javascript\">var arr = [\"a\",\"c\",\"b\",1,12,31,3,4,51,7,\"啊\",\"等\",\"好\"];\narr.sort((a,b) =&gt; {\n    if(typeof a == \"number\" &amp;&amp; typeof b == \"number\"){\n        return a - b;//升序\n    }else {\n        return String(a).localeCompare(b); //升序\n    }\n})\nconsole.log(arr); //[1, 3, 4, 7, 12, 31, 51, \"啊\", \"等\", \"好\", \"a\", \"b\", \"c\"]</code></pre>\n\n<p><strong><a href=\"https://blog.csdn.net/zyz00000000/article/details/106924219\">此处也有对字符串的方法总结，尤其是localeCompare方法的使用</a></strong></p>\n\n<h3>6、还有几个与正则表达式相关的方法：</h3>\n\n<p><a href=\"https://blog.csdn.net/zyz00000000/article/details/107957777\">search()、split()、replace()、match()方法，参考该文章</a></p>\n\n<p><strong><code>7、slice()</code></strong>&nbsp;方法提取某个字符串的一部分，并返回一个新的字符串，且不会改动原字符串。</p>\n\n<pre>\n<code class=\"language-html\">str.slice(beginIndex[, endIndex])。注意提取的范围：[beginIndex,endIndex) 左闭右开！</code></pre>\n\n<p><code>beginIndex</code>：从该索引（以 0 为基数）处开始提取原字符串中的字符。如果值为负数，会被当做&nbsp;<code>strLength + beginIndex</code>&nbsp;看待，这里的<code>strLength</code>&nbsp;是字符串的长度。</p>\n\n<p><code>endIndex</code>：可选。在该索引（以 0 为基数）处结束提取字符串。如果省略该参数，<code>slice()</code>&nbsp;会一直提取到字符串末尾。如果该参数为负数，则被看作是 strLength + endIndex，这里的 strLength 就是字符串的长度。</p>\n\n<p>正数就是index的值，负数是 +str.length的==index的值</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.slice(0,1)) //a\nconsole.log(str.slice(4)) //efg\nconsole.log(str.slice(-3, -1))// 倒数第三个到倒数第一个  ef </code></pre>\n\n<p><strong>&nbsp;如果是取字符串的后五个字符：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">str.slice(-5)</code></pre>\n\n<p><strong><code>substring()&nbsp;</code></strong>方法返回一个字符串在开始索引到结束索引之间的一个子集, 或从开始索引直到字符串的末尾的一个子集。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.substring(0,1)) //a\nconsole.log(str.substring(4,6)) //ef</code></pre>\n\n<p><strong>&nbsp;8、大小写的转换：</strong></p>\n\n<p><strong><code>toLocaleUpperCase()</code></strong>&nbsp;方法根据本地主机语言环境把字符串转换为大写格式，并返回转换后的字符串。</p>\n\n<p><strong><code>toLocaleLowerCase()</code></strong>方法根据任何指定区域语言环境设置的大小写映射，返回调用字符串被转换为小写的格式。</p>\n\n<p><strong><code>toUpperCase()</code></strong>&nbsp;方法将调用该方法的字符串转为大写形式并返回（如果调用该方法的值不是字符串类型会被强制转换）。</p>\n\n<p><code><strong>toLowerCase()</strong></code>&nbsp;会将调用该方法的字符串值转为小写形式，并返回。</p>\n\n<p><strong><code>9、trim()</code>&nbsp;</strong>方法会从一个字符串的两端删除空白字符。兼容性IE9</p>\n\n<p><strong><code>10、valueOf()</code></strong>&nbsp;方法返回&nbsp;&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/String\"><code>String</code></a>&nbsp; 对象的原始值。此方法通常由JavaScript在内部调用，而不是在代码中显式调用。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.valueOf()) //abcdefg</code></pre>\n\n<p><code><strong>toString()</strong></code>&nbsp;方法返回指定对象的字符串形式。</p>\n\n<p><code>String</code>&nbsp;对象覆盖了<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object\"><code>Object</code></a>&nbsp;对象的&nbsp;<code>toString</code>&nbsp;方法；并没有继承&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/toString\"><code>Object.toString()</code></a>。对于&nbsp;<code>String</code>&nbsp;对象，<code>toString</code>&nbsp;方法返回该对象的字符串形式，和&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/valueOf\"><code>String.prototype.valueOf()</code></a>&nbsp;方法返回值一样。&nbsp;</p>\n\n<p><code><strong>字符串的toString()</strong></code>&nbsp;方法，返回字符串的字符串形式，是不是有点鸡肋，字符串本身就是字符串，但是调用了toString方法后还是字符串值。每个类型的数据都实现了自己的toSting方法，当然对象的toString根本不用重写，因为本来就是所有的数据类型继承的对象toString方法。</p>\n\n<p>以下实例，<strong>字符串的toString方法的作用：</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = new String(\"abcd\");\nconsole.log(str, typeof str);\nconsole.log(str.toString(), typeof str.toString())</code></pre>\n\n<p>结果：</p>\n\n<p><img alt=\"\" height=\"56\" src=\"https://img-blog.csdnimg.cn/2020082119212180.png\" width=\"226\" /></p>\n\n<p>&nbsp;此文章对toSting进行了深入探讨！</p>\n\n<h3>11、ES6新增方法以及Polyfill</h3>\n\n<p><code><strong>endsWith()</strong></code>方法用来判断当前字符串是否是以另外一个给定的子字符串&ldquo;结尾&rdquo;的，根据判断结果返回&nbsp;<code>true</code>&nbsp;或&nbsp;<code>false</code>。</p>\n\n<p>比如以下实例：是否是以fg结尾的</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.endsWith(\"fg\")) //true</code></pre>\n\n<p>还可以接收第二个参数，表明从index的位置截掉。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.endsWith(\"de\",5)) //true</code></pre>\n\n<p><code><strong>startsWith()</strong></code>&nbsp;方法用来判断当前字符串是否以另外一个给定的子字符串开头，并根据判断结果返回&nbsp;<code>true</code>&nbsp;或&nbsp;<code>false</code>。</p>\n\n<p>第二个参数表明：从index的位置开始</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.startsWith(\"abc\")) //true\nconsole.log(str.startsWith(\"def\",3)) //true</code></pre>\n\n<p><strong><code>includes()</code>&nbsp;</strong>方法用于判断一个字符串是否包含在另一个字符串，根据情况返回 true 或 false。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.includes(\"ab\")) //true</code></pre>\n\n<p>可以指定第二个参数，从字符串的第index位开始查找：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdefg\";\nconsole.log(str.includes(\"ab\",1)) //false</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "JS字符串的方法",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/108101273"
        },
        js_bitwise_perators: {
            "article_id": "107174287",
            "title": "JavaScript的位操作符",
            "description": "按位与（ AND）\n\t\t\ta &amp; b\n\t\t\t对于每一个比特位，只有两个操作数相应的比特位都是1时，结果才为1，否则为0。\n\t\t按位或（OR）\n\t\t\ta | b\n\t\t\t对于每一个比特位，当两个操作数相应的比特位至少有一个1时，结果为1，否则为0。\n\t\t按位异或（XOR）\n\t\t\ta ^ b\n\t\t\t对于每一个比特位，当两个操作数相应的比特位有且只有一个1时，结果为1，否则为0。\n\t\t按位非（NOT）\n\t\t\t~ a\n\t\t\t反转操作数的比特位，即0变成1，1变成0。\n\t\t左移（Left shift）\n\t\t\ta",
            "content": "<table>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_AND\">按位与（ AND）</a></td>\n\t\t\t<td><code>a &amp; b</code></td>\n\t\t\t<td>对于每一个比特位，只有两个操作数相应的比特位都是1时，结果才为1，否则为0。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_OR\">按位或（OR）</a></td>\n\t\t\t<td><code>a | b</code></td>\n\t\t\t<td>对于每一个比特位，当两个操作数相应的比特位至少有一个1时，结果为1，否则为0。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_XOR\">按位异或（XOR）</a></td>\n\t\t\t<td><code>a ^ b</code></td>\n\t\t\t<td>对于每一个比特位，当两个操作数相应的比特位有且只有一个1时，结果为1，否则为0。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_NOT\">按位非（NOT）</a></td>\n\t\t\t<td><code>~ a</code></td>\n\t\t\t<td>反转操作数的比特位，即0变成1，1变成0。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Left_shift\">左移（L</a><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Left_shift\">eft shift）</a></td>\n\t\t\t<td><code>a &lt;&lt; b</code></td>\n\t\t\t<td>将&nbsp;<code>a</code>&nbsp;的二进制形式向左移&nbsp;<code>b</code>&nbsp;(&lt; 32) 比特位，右边用0填充。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Right_shift\">有符号右移</a></td>\n\t\t\t<td><code>a &gt;&gt; b</code></td>\n\t\t\t<td>将 a 的二进制表示向右移<code>&nbsp;b&nbsp;</code>(&lt; 32) 位，丢弃被移出的位。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Unsigned_right_shift\">无符号右移</a></td>\n\t\t\t<td><code>a &gt;&gt;&gt; b</code></td>\n\t\t\t<td>将 a 的二进制表示向右移<code>&nbsp;b&nbsp;</code>(&lt; 32) 位，丢弃被移出的位，并使用 0 在左侧填充。</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "JavaScript位操作符",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/107174287"
        },
        js_math :{
            "article_id": "107610841",
            "title": "内置对象Math",
            "description": "Math内置对象\n\nMath是一个内置对象，它拥有一些数学常数属性和数学函数方法。Math不是一个函数对象。\n\nMath用于Number类型。它不支持BigInt。\n\n注意\n\n很多Math函数都有一个精度，而且这个精度在不同实现中也是不相同的。这意味着不同的浏览器会给出不同的结果，甚至，在不同的系统或架构下，相同的 JS 引擎也会给出不同的结果！\n\n\n\n常用方法\n\nMath.abs(x)\n\n返回一个数的绝对值。\n\nMath.ceil(x)\n\n返回大于一个数的最小整数，即一个数向上取整后的值...",
            "content": "<h3>Math内置对象</h3>\n\n<p>&nbsp;<strong><code>Math</code></strong>&nbsp;是一个内置对象，它拥有一些数学常数属性和数学函数方法。<code>Math</code>&nbsp;不是一个函数对象。</p>\n\n<p><code>Math</code>&nbsp;用于&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Number\"><code>Number</code></a>&nbsp;类型。它不支持&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/BigInt\"><code>BigInt</code></a>。</p>\n\n<h3>注意</h3>\n\n<p>很多&nbsp;<code>Math</code>&nbsp;函数都有一个精度，而且这个精度在不同实现中也是不相同的。这意味着不同的浏览器会给出不同的结果，甚至，在不同的系统或架构下，相同的 JS 引擎也会给出不同的结果！</p>\n\n<p><img alt=\"\" height=\"742\" src=\"https://img-blog.csdnimg.cn/20200727141940632.png\" width=\"395\" /></p>\n\n<h3>常用方法</h3>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/abs\"><code>Math.abs(x)</code></a></p>\n\n<p>返回一个数的绝对值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/ceil\"><code>Math.ceil(x)</code></a></p>\n\n<p>返回大于一个数的最小整数，即一个数向上取整后的值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/floor\"><code>Math.floor(x)</code></a></p>\n\n<p>返回小于一个数的最大整数，即一个数向下取整后的值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/random\"><code>Math.random()</code></a></p>\n\n<p>返回一个 0 到 1 之间的伪随机数。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/round\"><code>Math.round(x)</code></a></p>\n\n<p>返回四舍五入后的整数。</p>\n\n<p><code><strong>Math.max()</strong></code>&nbsp;函数返回一组数中的最大值。如果给定的参数中至少有一个参数无法被转换成数字，则会返回&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a>。</p>\n\n<p><code><strong>Math.min()</strong></code>&nbsp;返回一组数据中的最小值。如果给定的参数中至少有一个参数无法被转换成数字，则会返回&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/NaN\"><code>NaN</code></a>。</p>\n\n<h3>不常用方法</h3>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/acos\"><code>Math.acos(x)</code></a></p>\n\n<p>返回一个数的反余弦值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/acosh\"><code>Math.acosh(x)</code></a></p>\n\n<p>返回一个数的反双曲余弦值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/asin\"><code>Math.asin(x)</code></a></p>\n\n<p>返回一个数的反正弦值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/asinh\"><code>Math.asinh(x)</code></a></p>\n\n<p>返回一个数的反双曲正弦值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/atan\"><code>Math.atan(x)</code></a></p>\n\n<p>返回一个数的反正切值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/atanh\"><code>Math.atanh(x)</code></a></p>\n\n<p>返回一个数的反双曲正切值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/atan2\"><code>Math.atan2(y,&nbsp;x)</code></a></p>\n\n<p>返回&nbsp;<code>y/x</code>&nbsp;的反正切值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/cbrt\"><code>Math.cbrt(x)</code></a></p>\n\n<p>返回一个数的立方根。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/clz32\"><code>Math.clz32(x)</code></a></p>\n\n<p>返回一个 32 位整数的前导零的数量。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/cos\"><code>Math.cos(x)</code></a></p>\n\n<p>返回一个数的余弦值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/cosh\"><code>Math.cosh(x)</code></a></p>\n\n<p>返回一个数的双曲余弦值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/exp\"><code>Math.exp(x)</code></a></p>\n\n<p>返回欧拉常数的参数次方，<code>Ex</code>，其中&nbsp;<code>x</code>&nbsp;为参数，<code>E</code>&nbsp;是欧拉常数（2.718...，自然对数的底数）。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/expm1\"><code>Math.expm1(x)</code></a></p>\n\n<p>返回&nbsp;<code>exp(x) - 1</code>&nbsp;的值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/fround\"><code>Math.fround(x)</code></a></p>\n\n<p>返回最接近一个数的<a href=\"https://zh.wikipedia.org/wiki/%E5%96%AE%E7%B2%BE%E5%BA%A6%E6%B5%AE%E9%BB%9E%E6%95%B8\">单精度浮点型</a>表示。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/hypot\"><code>Math.hypot([x[,&nbsp;y[, &hellip;]]])</code></a></p>\n\n<p>返回其所有参数平方和的平方根。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/imul\"><code>Math.imul(x,&nbsp;y)</code></a></p>\n\n<p>返回 32 位整数乘法的结果。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/log\"><code>Math.log(x)</code></a></p>\n\n<p>返回一个数的自然对数（㏒e，即 ㏑）。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/log1p\"><code>Math.log1p(x)</code></a></p>\n\n<p>返回一个数加 1 的和的自然对数（㏒e，即 ㏑）。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/log10\"><code>Math.log10(x)</code></a></p>\n\n<p>返回一个数以 10 为底数的对数。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/log2\"><code>Math.log2(x)</code></a></p>\n\n<p>返回一个数以 2 为底数的对数。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/max\"><code>Math.max([x[,&nbsp;y[, &hellip;]]])</code></a></p>\n\n<p>返回零到多个数值中最大值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/min\"><code>Math.min([x[,&nbsp;y[, &hellip;]]])</code></a></p>\n\n<p>返回零到多个数值中最小值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/pow\"><code>Math.pow(x,&nbsp;y)</code></a></p>\n\n<p>返回一个数的 y 次幂。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/sign\"><code>Math.sign(x)</code></a></p>\n\n<p>返回一个数的符号，得知一个数是正数、负数还是 0。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/sin\"><code>Math.sin(x)</code></a></p>\n\n<p>返回一个数的正弦值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/sinh\"><code>Math.sinh(x)</code></a></p>\n\n<p>返回一个数的双曲正弦值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/sqrt\"><code>Math.sqrt(x)</code></a></p>\n\n<p>返回一个数的平方根。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/tan\"><code>Math.tan(x)</code></a></p>\n\n<p>返回一个数的正切值。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/tanh\"><code>Math.tanh(x)</code></a></p>\n\n<p>返回一个数的双曲正切值。</p>\n\n<p><code>Math.toSource()</code></p>\n\n<p>返回字符串&nbsp;<code>&quot;Math&quot;</code>。</p>\n\n<p><a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Math/trunc\"><code>Math.trunc(x)</code></a></p>\n\n<p>返回一个数的整数部分，直接去除其小数点及之后的部分。</p>\n",
            "markdowncontent": "",
            "tags": "javascript,Math对象方法",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/107610841"
        },
        js_boolean: {
            "article_id": "106994724",
            "title": "布尔值Boolean",
            "description": "1、Boolean()方法和!!的结果是一样的，都可以来判断某个值是truth或\n\n2、== 比较的时候，会调用值的toString方法，根据值的类型调用该值原型上的toString方法，进行比较。toSting不会修改原值。\n\n\n",
            "content": "<p>1、Boolean()方法和!!的结果是一样的，都可以来判断某个值是truth或</p>\n\n<p>2、== 比较的时候，会调用值的toString方法，根据值的类型调用该值原型上的toString方法，进行比较。toSting不会修改原值。</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "javascript",
            "categories": "JavaScript",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106994724"
        },
        js_regular_expression : {
            "article_id": "106768089",
            "title": "js正则表达式",
            "description": "正则表达式的创建方式；常用的字符；子表达式；优先级",
            "content": "<h1>正则表达式的两种创建方式</h1>\n\n<p><strong>（1）字面量</strong></p>\n\n<p><strong>（2）RegExp构造函数</strong></p>\n\n<p>构造函数内传入字符串，</p>\n\n<p><strong>注意：双\\&nbsp; 代表 \\，</strong><strong>第二个参数是匹配标志</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var reg = new RegExp(\"[a]\\\\w(\\\\d)\\\\1\");\nconsole.log(reg);</code></pre>\n\n<h1>字符的用法</h1>\n\n<h3>1、反斜杠 \\</h3>\n\n<p><strong>（1）\\w \\d \\s \\b</strong></p>\n\n<p><strong>\\w</strong>：单字字符&nbsp;字母数字下划线&nbsp;&nbsp;===&nbsp;&nbsp;[a-Z0-9_]&nbsp; &nbsp; &nbsp;<strong>\\W：</strong>&nbsp;非单字字符&nbsp;等价于&nbsp;[^A-Za-z0-9_]</p>\n\n<p><strong>是否发现：</strong>exec方法的执行结果有点像Generator对象，exec方法下面会具体的探讨</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"aB1_@\";\nvar reg = /\\w/g\nconsole.log(reg.exec(str))//[\"a\", index: 0, input: \"aB1_@\", groups: undefined]\nconsole.log(reg.exec(str))//[\"B\", index: 1, input: \"aB1_@\", groups: undefined]\nconsole.log(reg.exec(str))//[\"1\", index: 2, input: \"aB1_@\", groups: undefined]\nconsole.log(reg.exec(str))//[\"1\", index: 2, input: \"aB1_@\", groups: undefined]\nconsole.log(reg.exec(str))//null</code></pre>\n\n<p><strong>&nbsp;\\d</strong>： 数字&nbsp;===&nbsp;[0-9]&nbsp; &nbsp;&nbsp;<strong>\\D：</strong>&nbsp;非数字&nbsp;&nbsp;===&nbsp;[^0-9]</p>\n\n<p><strong>提醒：</strong>此处的数字，必须提醒一下，数字是指字符串中的数字。</p>\n\n<p>正则表达式的方法可以传入字符串或数字，其实会把数字转为字符串；但是字符串的方法，必须有字符串进行调用。</p>\n\n<p>正则表达式匹配的是字符串中的字符、数字、符号等。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"123\" || 123;\nvar reg = /\\d+/;\nconsole.log(reg.exec(str));//[\"123\", index: 0, input: \"123\", groups: undefined]</code></pre>\n\n<p><strong>\\s：</strong>&nbsp;空白字符&nbsp; 包括：空格、\\f换页、<strong>\\n换行</strong>、<strong>\\r回车</strong>、\\t水平制表符、\\v垂直制表符&nbsp;&nbsp;&nbsp;<strong>\\S：</strong>&nbsp;非空白字符&nbsp;&nbsp;</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1 a\";\nvar reg = /\\d\\s\\w/;\nconsole.log(reg.exec(str));//[\"1 a\", index: 0, input: \"1 a\", groups: undefined]</code></pre>\n\n<p><strong>注意：</strong>除了空格外，其他几个都是通过\\n\\f\\r\\t\\v这种写法才可以在代码中表现出来。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1\\na\";\nvar reg = /\\d\\s\\w/;\nconsole.log(reg.exec(str));//[\"1↵a\", index: 0, input: \"1↵a\", groups: undefined]</code></pre>\n\n<p>&nbsp;<strong>\\b：</strong>&nbsp;单词边界&nbsp;&nbsp;<strong>\\B：</strong>&nbsp;非单词边界&nbsp;</p>\n\n<p><strong>注意：</strong>单词编辑\\b，要么修饰前面的词，要么修饰后面的词。</p>\n\n<p>比如下面例子：\\d数字后面是有边界的；\\s的前面是有边界的。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1 a\";\nvar reg = /\\d\\b\\b\\s\\w/;\nconsole.log(reg.exec(str));//[\"1 a\", index: 0, input: \"1 a\", groups: undefined]</code></pre>\n\n<p><strong>（2）转义字符（Escape）</strong></p>\n\n<p>所有的特殊字符，想要匹配到他们，都需要通过反斜杠进行转义。等等</p>\n\n<pre>\n<code class=\"language-javascript\">\\ * ^ $ ! ? . + ( ) [ ] {  } |</code></pre>\n\n<h3><strong>&nbsp;（3）子表达式</strong></h3>\n\n<p>捕获括号，匹配括号内容，并记住匹配项。可以通过子表达式代替匹配到的内容。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12123\";\nvar reg = /(\\d)(\\d)\\1\\2/\nconsole.log(reg.exec(str))//[\"1212\", \"1\", \"2\", index: 0, input: \"12123\", groups: undefined]</code></pre>\n\n<p>&nbsp;特别注意：子表达式代表的是匹配到的内容</p>\n\n<p>重点是捕获括号，下面会对捕获括号进行探讨</p>\n\n<p>括号内可以是或语句：也是子表达式</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12112\";\nvar reg = /(\\d|\\w)(\\d)(\\d)\\1\\2/\nconsole.log(reg.exec(str))//[\"12112\", \"1\", \"2\", \"1\", index: 0, input: \"12112\", groups: undefined]</code></pre>\n\n<p>子表达式配到到的不止一个字符，可以是多个字符，作为一个子表达式</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12112\";\nvar reg = /(\\d\\d|\\w\\w)(\\d)\\1/\nconsole.log(reg.exec(str))//[\"12112\", \"12\", \"1\", index: 0, input: \"12112\", groups: undefined]</code></pre>\n\n<p>&nbsp;如此看来，只有特定的括号才是非捕获，而一般的都是捕获，可以使用子表达式</p>\n\n<h3>2、中括号 [&nbsp; ]</h3>\n\n<p>中括号[ ]内可以设置匹配哪些字符中的一个。</p>\n\n<pre>\n<code class=\"language-javascript\">[abc]    匹配abc其中的一个字符\n[a-z]    匹配a-z其中的一个字符\n[^a-z]   匹配非a-z的其中一个字符</code></pre>\n\n<p><strong>&nbsp;[点] 匹配点</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"3.1415926\";\nvar reg = /[.]/;\nconsole.log(reg.exec(str)); //[\".\", index: 1, input: \"3.1415926\", groups: undefined]</code></pre>\n\n<p><strong>&nbsp;[$] 匹配$</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"100,000$\";\nvar reg = /[$]/;\nconsole.log(reg.exec(str)); //[\"$\", index: 7, input: \"100,000$\", groups: undefined]</code></pre>\n\n<p>&nbsp;</p>\n\n<h3>&nbsp;3、大括号{ }、量词、非贪婪匹配</h3>\n\n<p>*&nbsp; 0个或多个</p>\n\n<p>+&nbsp; 1个或多个</p>\n\n<p>?&nbsp; 0个或1个</p>\n\n<p>{0，}&nbsp; 0个或多个</p>\n\n<p>{2,3} 2个或三个</p>\n\n<p>{2}&nbsp; 2个</p>\n\n<p>量词后面跟？采用非贪婪匹配模式</p>\n\n<p>下面实例实现拆分字符串成为数组：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1234\";\nvar reg = /\\d{1,4}?/g\nconsole.log(str.match(reg));\n\n[\"1\", \"2\", \"3\", \"4\"]</code></pre>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1234\";\nvar reg = /\\d+?/g\nconsole.log(str.match(reg)); //[\"1\", \"2\", \"3\", \"4\"]</code></pre>\n\n<h3>4、其他特殊字符</h3>\n\n<p><strong>（1）.</strong>&nbsp; &nbsp;&nbsp;匹配除了\\n换行以外的所有字符；如果匹配模式是<strong>s</strong>，那么 <strong>. </strong>匹配所有的字符</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a\\!@#\\$%\";\nvar reg = /.+/g\nconsole.log(reg.exec(str))//[\"1a!@#$%\", index: 0, input: \"1a!@#$%\", groups: undefined]</code></pre>\n\n<p>&nbsp;匹配所有字符，包括换行\\n</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a\\!@\\n#\\$%\";\nvar reg = /.+/gs\nconsole.log(reg.exec(str))//[\"1a!@↵#$%\", index: 0, input: \"1a!@↵#$%\", groups: undefined]</code></pre>\n\n<p>&nbsp;<strong>（2）*&nbsp;</strong> &nbsp;匹配前一个表达式 0 次或多次。等价于&nbsp;<code>{0,}</code>。</p>\n\n<p>&nbsp;</p>\n\n<p><strong>（3）^</strong>&nbsp; 匹配以什么开头的，如果在[^ ]中，标示<strong>非。正则匹配好像只有此处可以标示&ldquo;非&rdquo;和非捕获括号中x(?!y)、(?&lt;!y)x标示非。</strong></p>\n\n<p>以下实例：全局匹配，寻找以数字开始的\\d</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a2\";\nvar reg = /^\\d/gs\nconsole.log(reg.exec(str))//[\"1\", index: 0, input: \"1a2\", groups: undefined]</code></pre>\n\n<p>&nbsp;以下匹配：全局匹配，寻找一个非数字的字符</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a2\";\nvar reg = /[^\\d]/g\nconsole.log(reg.exec(str))//[\"a\", index: 1, input: \"1a2\", groups: undefined]</code></pre>\n\n<p><strong>（4）$</strong>&nbsp; 匹配以什么结尾；在正则表达式的替换replace方法中，而在换环节，则要使用像&nbsp;<code>$1</code>、<code>$2</code>、...、<code>$n</code>&nbsp;这样的语法，<code>$&amp;</code>&nbsp;表示整个用于匹配的原字符串。</p>\n\n<p>用法1：以什么结尾，以下实例：全局匹配，寻找以数字的结尾的\\d</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a2\";\nvar reg = /\\d$/g\nconsole.log(reg.exec(str))//[\"2\", index: 2, input: \"1a2\", groups: undefined]</code></pre>\n\n<p>用法2： replace方法中，以下的含义：</p>\n\n<table>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>$1、$2、...、$99</td>\n\t\t\t<td>与 regexp 中的第 1 到第 99 个子表达式相匹配的文本。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>$&amp;</td>\n\t\t\t<td>与 regexp 相匹配的子串。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>$`</td>\n\t\t\t<td>位于匹配子串左侧的文本。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>$&#39;</td>\n\t\t\t<td>位于匹配子串右侧的文本。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>$$</td>\n\t\t\t<td>直接量符号。</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p><strong>（5）|&nbsp; &nbsp;或</strong></p>\n\n<p>或在正则表达式中的优先级最低！下面会讨论优先级</p>\n\n<p><strong>会将 或 | 左右两侧分隔开，因为或|的优先级最低，所以或|左侧的先结合，然后或|右侧的再结合</strong></p>\n\n<p>比如：下面代码，匹配的是f或者mood</p>\n\n<pre>\n<code class=\"language-javascript\">var reg = /f|mood/</code></pre>\n\n<p>在jquery源码中，<strong>表示空字符的三种方式，以多个空字符开头或者以多个空字符结尾！</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var rtrim = /^[\\s\\uFEFF\\xA0]+|[\\s\\uFEFF\\xA0]+$/g;</code></pre>\n\n<p>&nbsp;</p>\n\n<h3>非捕获括号</h3>\n\n<p>(?:x)&nbsp; 匹配 &#39;x&#39; 但是不记住匹配项</p>\n\n<p><strong>切记子表达式\\1代表匹配到的内容，而不是\\d</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a2335\";\nvar reg = /(?:\\d)(\\d)\\1/g\nvar result = str.match(reg)\nconsole.log(result);   //[\"233\"]</code></pre>\n\n<p>(?=y)&nbsp; 后面是y</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a2335\";\nvar reg = /\\d(?=33)/g\nvar result = str.match(reg)\nconsole.log(result);      //[\"2\"]</code></pre>\n\n<p>(?&lt;=y)&nbsp; 前面是y</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a2335\";\nvar reg = /(?&lt;=1a)\\d/g\nvar result = str.match(reg)\nconsole.log(result);  //[\"2\"]</code></pre>\n\n<p>(?!y)&nbsp; &nbsp;后面不是y</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a2335\";\nvar reg = /\\d(?!a)/g\nvar result = str.match(reg)\nconsole.log(result);  //[\"2\", \"3\", \"3\", \"5\"]</code></pre>\n\n<p>(?&lt;!y) 前面不是y</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1a2335\";\nvar reg = /(?&lt;!a)\\d/g\nvar result = str.match(reg)\nconsole.log(result);   //[\"1\", \"3\", \"3\", \"5\"]</code></pre>\n\n<p><strong>总结</strong>：都是<strong>用?开头</strong>， <strong>是=</strong>，<strong>不是！</strong>，<strong>区分前面：？后面跟&lt;</strong></p>\n\n<h3>优先级</h3>\n\n<p><strong>正则表达式从左到右进行计算，并遵循优先级顺序，这与算术表达式非常类似。</strong></p>\n\n<p><strong>相同优先级的从左到右进行运算，不同优先级的运算先高后低。</strong></p>\n\n<p>下表从最高到最低说明了各种正则表达式运算符的优先级顺序：</p>\n\n<table>\n\t<tbody>\n\t\t<tr>\n\t\t\t<th>运算符</th>\n\t\t\t<th>描述</th>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>\\</td>\n\t\t\t<td>转义符</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>(), (?:), (?=), []</td>\n\t\t\t<td>圆括号和方括号</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>*, +, ?, {n}, {n,}, {n,m}</td>\n\t\t\t<td>限定符</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>^, $, 任何字符</td>\n\t\t\t<td>定位点和序列（即：位置和顺序）</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>|</td>\n\t\t\t<td>替换，&quot;或&quot;操作<br />\n\t\t\t字符具有高于替换运算符的优先级，使得&quot;m|food&quot;匹配&quot;m&quot;或&quot;food&quot;。若要匹配&quot;mood&quot;或&quot;food&quot;，请使用括号创建子表达式，从而产生&quot;(m|f)ood&quot;。</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p><strong>&nbsp;对优先级的理解：</strong></p>\n\n<p>（1）\\的优先级最高</p>\n\n<p>（2）() (?:) (?=) [] 的优先级</p>\n\n<p>(?:)代表非捕获，不管后面跟上面内容</p>\n\n<pre>\n<code>var reg = /(?:a|b)/</code></pre>\n\n<h3>相关方法</h3>\n\n<p><img alt=\"\" height=\"155\" src=\"https://img-blog.csdnimg.cn/20200811192404636.png\" width=\"708\" /></p>\n\n<p><strong>正则表达式两个方法：</strong></p>\n\n<p><strong>（1）test方法</strong></p>\n\n<p><code><strong>test()</strong></code>&nbsp;方法执行一个检索，<strong>用来查看正则表达式与指定的字符串是否匹配</strong>。返回&nbsp;<code>true</code>&nbsp;或&nbsp;<code>false</code>。</p>\n\n<p><strong>（2）exec方法</strong></p>\n\n<p>在设置了&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/global\"><code>global</code></a>&nbsp;或&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/sticky\"><code>sticky</code></a>&nbsp;标志位的情况下（如&nbsp;<code>/foo/g</code>&nbsp;or&nbsp;<code>/foo/y</code>），JavaScript&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/RegExp\"><code>RegExp</code></a>&nbsp;对象是<strong>有状态</strong>的。他们会将上次成功匹配后的位置记录在&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/lastIndex\"><code>lastIndex</code></a>&nbsp;属性中。使用此特性，<code>exec()</code>&nbsp;可用来对单个字符串中的多次匹配结果进行逐条的遍历（包括捕获到的匹配），而相比之下，&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/match\"><code>String.prototype.match()</code></a>&nbsp;只会返回匹配到的结果。</p>\n\n<p>如果你只是为了判断是否匹配（true或 false），可以使用&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test\"><code>RegExp.test()</code></a>&nbsp;方法，或者&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/search\"><code>String.search()</code></a>&nbsp;方法。</p>\n\n<p><strong>非全局匹配</strong>，reg.lastIndex一直是0，所以匹配的结果一直都是第一项的结果</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345\";\nvar reg = /\\d/;\nconsole.log(reg.exec(str), reg.lastIndex)\nconsole.log(reg.exec(str), reg.lastIndex)\n\n[\"1\", index: 0, input: \"12345\", groups: undefined] 0</code></pre>\n\n<p><strong>&nbsp;全局匹配，</strong>每次匹配后，会记录lastIndex的值，从而每次执行的结果，是下一个符合条件的匹配结果</p>\n\n<h3>根据该特性，实现一个简单的generator函数</h3>\n\n<pre>\n<code class=\"language-javascript\">var obj = {\n    0: \"a\",\n    1: true,\n    2: [1,2,3],\n    3: function(){console.log(3)}\n}\nvar str = \"0123\";\nvar reg = /\\d/g;\nfunction imitateGenetator(o, r, s){//只有g模式下，才会有generator的效果\n    var key = null;\n    var reg = r.exec(s);//[\"0\", index: 0, input: \"01234567\", groups: undefined]\n    if(reg){\n       key = reg[0]\n       console.log(o[key])\n        return o[key];\n    }else{\n        console.log(undefined)\n        return undefined;\n    }\n    \n}\nimitateGenetator(obj, reg, str);//a\nimitateGenetator(obj, reg, str);//true\nimitateGenetator(obj, reg, str);//[1, 2, 3]\nimitateGenetator(obj, reg, str);//ƒ (){console.log(3)}\nimitateGenetator(obj, reg, str);//undefined</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "javascript",
            "categories": "JavaScript,正则表达式",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/106768089"
        },
        js_regular_expression_methods: {
            "article_id": "107957777",
            "title": "正则表达式的相关方法",
            "description": "一、正则对象的方法\n\n讨论是否是全局匹配，因为全局匹配，会修改该正则对象的lastIndex的值，其他正则实例对象的lastIndex值不被修改，使得下一次用正则方法匹配的时候，从lastIndex的位置开始。\n\nlastIndex是正则表达式的一个可读可写的整数型属性，用来指定下一次匹配的起始索引。\n\nRegExp.lastIndex属性的属性特性：\n\t\twritable\n\t\t\ttrue\n\t\tenumerable\n\t\t\tfalse\n\t\tconfigurable\n\t\t\tfalse\n\t\t只有正则表达式使...",
            "content": "<h3>一、正则对象的方法</h3>\n\n<p>讨论是否是全局匹配，因为全局匹配，会修改<strong>该正则对象的lastIndex的值</strong>，其他正则实例对象的lastIndex值不被修改，使得下一次用正则方法匹配的时候，从lastIndex的位置开始。</p>\n\n<p><code><strong>lastIndex</strong></code>&nbsp;是正则表达式的一个<strong>可读可写</strong>的整数型属性，用来指定下一次匹配的起始索引。</p>\n\n<table>\n\t<thead>\n\t\t<tr>\n\t\t\t<th colspan=\"2\"><code>RegExp.lastIndex</code>&nbsp;属性的属性特性：</th>\n\t\t</tr>\n\t</thead>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td>writable</td>\n\t\t\t<td>true</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>enumerable</td>\n\t\t\t<td>false</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td>configurable</td>\n\t\t\t<td>false</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>&nbsp;只有正则表达式使用了表示<strong>全局检索的 &quot;<code>g</code>&quot; 标志时，该属性才会起作用</strong>。此时应用下面的规则：</p>\n\n<ul>\n\t<li>lastIndex大于等于字符串长度时，<code>regexp.test</code>&nbsp;和&nbsp;<code>regexp.exec</code>&nbsp;将会匹配失败，然后&nbsp;<code>lastIndex</code>&nbsp;被设置为 0。除非regexp匹配空字符串。</li>\n\t<li>否则，<code>lastIndex</code>&nbsp;被设置为紧随<strong>最近一次成功匹配的索引值的下一个位置</strong>。下一次匹配会从最新的lastIndex值开始。</li>\n</ul>\n\n<p><strong>需要注意的是</strong>：某个正则实例对象，对所有传入的字符串生效，尤其注意lastIndex值的变化。&nbsp;</p>\n\n<p><strong>&nbsp;1、RegExp.prototype.test()</strong></p>\n\n<p><code><strong>test()</strong></code>&nbsp;方法执行一个检索，用来查看正则表达式与指定的字符串是否匹配。返回&nbsp;<code>true</code>&nbsp;或&nbsp;<code>false</code>。</p>\n\n<p>非全局匹配</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcabcabc\";\nvar reg = /b/;\nconsole.log(reg.test(str), reg.lastIndex) //true 0\nconsole.log(reg.test(str), reg.lastIndex) //true 0</code></pre>\n\n<p>全局匹配</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcabcabc\";\nvar reg = /b/g;\nconsole.log(reg.test(str), reg.lastIndex)//true 2\nconsole.log(reg.test(str), reg.lastIndex)//true 5</code></pre>\n\n<p><strong>2、RegExp.prototype.exec()</strong></p>\n\n<p><code><strong>exec()&nbsp;</strong></code>方法在一个指定字符串中执行一个搜索匹配。返回一个结果数组或&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null\"><code>null</code></a>。</p>\n\n<p>如果匹配成功，<code>exec()</code>&nbsp;方法返回一个数组（包含额外的属性&nbsp;<strong><code>index</code>&nbsp;匹配项的索引值</strong>和&nbsp;<strong><code>input</code>&nbsp;输入到正则的字符串，因为该正则对象还可以匹配别的字符串</strong>），并更新正则表达式对象的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/lastIndex\"><code>lastIndex</code></a>&nbsp;属性。完全匹配成功的文本将作为返回数组的第一项，从第二项起，后续每项都对应正则表达式内捕获括号里匹配成功的文本。</p>\n\n<p>如果匹配失败，<code>exec()</code>&nbsp;方法返回&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null\"><code>null</code></a>，并将&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/lastIndex\"><code>lastIndex</code></a>&nbsp;重置为 0 。</p>\n\n<p>非全局匹配</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcabcabc\";\nvar reg = /b/;\nconsole.log(reg.exec(str), reg.lastIndex) \n//[\"b\", index: 1, input: \"abcabcabc\", groups: undefined] 0\nconsole.log(reg.exec(str), reg.lastIndex) \n//[\"b\", index: 1, input: \"abcabcabc\", groups: undefined] 0</code></pre>\n\n<p>&nbsp;全局匹配</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcabcabc\";\nvar reg = /b/g;\nconsole.log(reg.exec(str), reg.lastIndex) \n//[\"b\", index: 1, input: \"abcabcabc\", groups: undefined] 2\nconsole.log(reg.exec(str), reg.lastIndex) \n//[\"b\", index: 4, input: \"abcabcabc\", groups: undefined] 5</code></pre>\n\n<p>&nbsp;<strong>手动修改lastIndex值</strong></p>\n\n<p>非全局匹配下，修改lastIndex。非全局匹配不受lastIndex值影响，所以不管怎么修改lastIndex值，匹配的还是第一项符合的字段。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"ab1ab2ab3\";\nvar reg = /b\\d/;\nreg.lastIndex = 10;\nconsole.log(reg.exec(str), reg.lastIndex) \n//[\"b\", index: 1, input: \"abcabcabc\", groups: undefined] 10\nconsole.log(reg.exec(str), reg.lastIndex) \n//[\"b\", index: 1, input: \"abcabcabc\", groups: undefined] 10</code></pre>\n\n<p>全局匹配下，受lastIndex值影响，谨慎使用lastIndex值。超出字符串长度，会匹配失败</p>\n\n<p>&nbsp;下面实例，从字符串的index值为3的位置开始匹配</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"ab1ab2ab3\";\nvar reg = /b\\d/g;\nreg.lastIndex = 3;\nconsole.log(reg.exec(str), reg.lastIndex) //[\"b2\", index: 4, input: \"ab1ab2ab3\", groups: undefined] 6\nconsole.log(reg.exec(str), reg.lastIndex) //[\"b3\", index: 7, input: \"ab1ab2ab3\", groups: undefined] 9</code></pre>\n\n<h3>二、字符串的方法</h3>\n\n<p><strong>1、String.prototype.search()</strong></p>\n\n<p>字符串去搜索该正则所匹配的片段，返回首次匹配到的索引值，否则返回-1。</p>\n\n<p>如果匹配成功，则&nbsp;<code>search()</code>&nbsp;返回正则表达式在字符串中首次匹配项的索引;否则，返回&nbsp;<code>-1</code>。</p>\n\n<p>当你想要知道字符串中是否存在某个模式（pattern）时可使用&nbsp;<code>search()</code>，类似于正则表达式的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test\"><code>test()</code></a>&nbsp;方法。当要了解更多匹配信息时，可使用&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/match\"><code>match()</code></a>（但会更慢一些），该方法类似于正则表达式的&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/exec\"><code>exec()</code></a>&nbsp;方法。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"hey ^$JudE2\";\nvar re = /[A-Z]/g;\nvar re2 = /[\\^]/g;\nconsole.log(str.search(re));  //6\nconsole.log(str.search(re2)); //4</code></pre>\n\n<p><strong>2、String.prototype.split()</strong></p>\n\n<p>拆分字符串为数组，不改变原字符串</p>\n\n<p>（1）不传参数，返回一个长度为1的数组，数组的第一项是该字符串</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdef\";\nconsole.log(str.split(), str); //[\"abcdef\"] \"abcdef\"</code></pre>\n\n<p>（2）接收一个空字符串，会把每个字符拆分出来</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abcdef\";\nconsole.log(str.split(\"\"), str);\n[\"a\", \"b\", \"c\", \"d\", \"e\", \"f\"] \"abcdef\"</code></pre>\n\n<p>&nbsp;（3）可接受一个字符串或一个正则表达式作为分隔符</p>\n\n<p>字符串2</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"ab2cd2ef\";\nconsole.log(str.split(\"2\"), str);//[\"ab\", \"cd\", \"ef\"] \"ab2cd2ef\"</code></pre>\n\n<p>&nbsp;正则表达式</p>\n\n<pre>\n<code class=\"language-javascript\">var reg = /\\d/;\nvar str = \"ab2cd2ef\";\nconsole.log(str.split(reg), str); // [\"ab\", \"cd\", \"ef\"] \"ab2cd2ef\"</code></pre>\n\n<p>&nbsp;（4）可以接受第二个参数，一个整数，限定返回的分割片段数量</p>\n\n<pre>\n<code class=\"language-javascript\">var reg = /\\d/;\nvar str = \"abcdef\";\nconsole.log(str.split(\"\",3), str); //[\"a\", \"b\", \"c\"] \"abcdef\"</code></pre>\n\n<p><strong>3、String.prototype.replace()</strong></p>\n\n<p>原字符串不会改变。</p>\n\n<p><strong>特别注意是多次匹配还是匹配一次！</strong></p>\n\n<p>接收两个参数，模式pattern和替换值replacement，模式可以是字符串片段也可以是正则表达式，替换值可以是字符串或函数，字符串内可以有特殊的$符。</p>\n\n<p>关于替换值是字符串时，字符串内可以是特殊$符：</p>\n\n<table>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td><code>$$</code></td>\n\t\t\t<td>插入一个 &quot;$&quot;。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>$&amp;</code></td>\n\t\t\t<td>插入匹配的子串。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>$`</code></td>\n\t\t\t<td>插入当前匹配的子串左边的内容。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>$&#39;</code></td>\n\t\t\t<td>插入当前匹配的子串右边的内容。</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><code>$<em>n</em></code></td>\n\t\t\t<td>\n\t\t\t<p>假如第一个参数是&nbsp;<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/RegExp\"><code>RegExp</code></a>对象，并且 n 是个小于100的非负整数，那么插入第 n 个括号匹配的字符串。提示：索引是从1开始</p>\n\t\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>&nbsp;替换值是<strong>函数，一个用来创建新子字符串的函数，该函数的返回值将作为替换值</strong>。函数参数的意义：</p>\n\n<p><strong>&nbsp;（函数参数的每项和正则的exec()或非全局匹配的match方法得到的结果类似）</strong></p>\n\n<table>\n\t<tbody>\n\t\t<tr>\n\t\t\t<td style=\"width:70px\"><code>match</code></td>\n\t\t\t<td>匹配的子串。（对应于上述的$&amp;。）</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"width:70px\"><code>p1,p2, ...</code></td>\n\t\t\t<td>\n\t\t\t<p>假如replace()方法的第一个参数是一个<a href=\"https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/RegExp\"><code>RegExp</code></a>&nbsp;对象，则代表第n个括号匹配的字符串。（对应于上述的$1，$2等。）例如，如果是用&nbsp;<code>/(\\a+)(\\b+)/</code>&nbsp;这个来匹配，<code>p1</code>&nbsp;就是匹配的&nbsp;<code>\\a+</code>，<code>p2</code>&nbsp;就是匹配的&nbsp;<code>\\b+</code>。</p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"width:70px\"><code>index</code></td>\n\t\t\t<td>\n\t\t\t<p>匹配到的子字符串在原字符串中的index值。（比如，如果原字符串是&nbsp;<code>&#39;abcd&#39;</code>，匹配到的子字符串是&nbsp;<code>&#39;bc&#39;</code>，那么这个参数将会是 1）</p>\n\t\t\t</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td style=\"width:70px\"><code>string</code></td>\n\t\t\t<td>被匹配的原字符串。</td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p>（1）模式是字符串：意味着<strong>字符串仅第一个匹配项会被替换</strong>。只进行一次匹配和替换。</p>\n\n<p>1）模式是字符串，替换值是字符串：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abacadaebf\";\nvar replaceStr = str.replace(\"a\",\"1\");\nconsole.log(replaceStr); //1bacadaebf</code></pre>\n\n<p>&nbsp;2）模式是字符串，替换值是字符串，内有特殊$符：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abacadaebf\";\nvar replaceStr = str.replace(\"a\",\"$'1\");\nconsole.log(replaceStr); //bacadaebf1bacadaebf</code></pre>\n\n<p>&nbsp;3）模式是字符串，替换值是函数：</p>\n\n<p>我们知道接收的第二个参数如果是函数， 进行了多少次匹配替换，函数就会执行对应的次数。</p>\n\n<p>模式是字符串仅会进行一次匹配，所以替换值的函数也只会执行一次。</p>\n\n<p>我们看下打印结果：不是正则表达式，函数没有p1 p2等于子表达式对应的值。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abacadaebf\";\nvar replaceStr = str.replace(\"a\",function(){\n    console.log(arguments);//[\"a\", 0, \"abacadaebf\", callee: ƒ, Symbol(Symbol.iterator): ƒ]\n});</code></pre>\n\n<p>（2） 模式是正则表达式，如果是全局匹配，可能会进行多次匹配和替换；如果非全局匹配，也只能匹配到一项</p>\n\n<p>&nbsp;非全局匹配：仅会匹配一项，仅替换第一个匹配项</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abacadaebf\";\nvar reg = /(a)/;\nvar replaceStr = str.replace(reg,\"@\"); \nconsole.log(replaceStr);//@bacadaebf</code></pre>\n\n<p>&nbsp;全局匹配：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abacadaebf\";\nvar reg = /(a)/g;\nvar replaceStr = str.replace(reg,\"@\");\nconsole.log(replaceStr);  // @b@c@d@ebf</code></pre>\n\n<p>&nbsp;如果替换值是函数，匹配到多少次，函数就会执行对应的次数，注意每次执行时的参数。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abacadaebf\";\nvar reg = /(a)[bcde]/g;\nvar replaceStr = str.replace(reg,function(){\n    console.log(arguments);\n    return \"k\";\n});\nconsole.log(replaceStr);</code></pre>\n\n<p>&nbsp;结果：</p>\n\n<pre>\n<code class=\"language-javascript\">Arguments(4) [\"ab\", \"a\", 0, \"abacadaebf\", callee: ƒ, Symbol(Symbol.iterator): ƒ]\nArguments(4) [\"ac\", \"a\", 2, \"abacadaebf\", callee: ƒ, Symbol(Symbol.iterator): ƒ]\nArguments(4) [\"ad\", \"a\", 4, \"abacadaebf\", callee: ƒ, Symbol(Symbol.iterator): ƒ]\nArguments(4) [\"ae\", \"a\", 6, \"abacadaebf\", callee: ƒ, Symbol(Symbol.iterator): ƒ]\nkkkkbf\n</code></pre>\n\n<p>注意：匹配子串match中，&nbsp;</p>\n\n<p><strong>&nbsp;例题：</strong></p>\n\n<p>（1）字符串的replace方法可以替换第一个匹配项，或所有的匹配项，那么匹配任意匹配项时，该如何实现呢？</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abacadaebf\";\nvar reg = /(a)[bcde]/g;\n//替换值是函数时，要自己判断有几个子表达式，如果判断不清楚，就打印arguments查看\nvar replaceStr = str.replace(reg,function(match, p1, index, str){\n    if(index == 4){//index是原字符串中该项的index值，自己判断第几个匹配项对应的index值。\n        //比如index为4，是第三个匹配项\n    }\n    return index;\n});\nconsole.log(replaceStr); //0246bf\n</code></pre>\n\n<p>&nbsp;（2）交换字符串中的两个单词</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"hello world\";\nvar reg = /(\\w+)\\s(\\w+)/g\nconsole.log(str.replace(reg, \"$2 $1\")); // world hello</code></pre>\n\n<p>&nbsp;如果原字符串较长：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"This is a string hello world\";\nvar reg = /(hello)\\s(world)/g\nconsole.log(str.replace(reg, \"$2 $1\"));\n\nThis is a string world hello</code></pre>\n\n<p>&nbsp;如果将a全部替换为b，将b全部替换为a：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"abababab\";\nvar reg = /(a)|(b)/g\nconsole.log(str.replace(reg, function(match, p1, p2, index, str){\n    console.log(arguments);\n    if(match === \"a\"){\n        return \"b\";\n    }else if(match === \"b\"){\n        return \"a\";\n    }\n})); // babababa</code></pre>\n\n<p>&nbsp;或 | ，p1 或 p2可能为undefined，什么原因呢？</p>\n\n<p><img alt=\"\" height=\"204\" src=\"https://img-blog.csdnimg.cn/20200817172117135.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"622\" /></p>\n\n<p>&nbsp;</p>\n\n<p><strong>4、String.prototype.match()</strong></p>\n\n<p>如果是全局匹配，返回一个包含所有匹配项的数组；如果是非全局匹配，返回的结果和reg.exec()的结果相同；如果没有匹配到，返回null。</p>\n\n<p>全局匹配：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"a1b1c1d1\";\nvar reg = /[a-z]/g;\nconsole.log(str.match(reg)); //[\"a\", \"b\", \"c\", \"d\"]</code></pre>\n\n<p>&nbsp;非全局匹配：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"a1b1c1d1\";\nvar reg = /[a-z]/;\nconsole.log(str.match(reg)); //[\"a\", index: 0, input: \"a1b1c1d1\", groups: undefined]</code></pre>\n\n<p>&nbsp;未匹配到：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"a1b1c1d1\";\nvar reg = /m/;\nconsole.log(str.match(reg)); //null</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "正则表达式方法",
            "categories": "正则表达式",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1,
            "csdn_article_url": "https://blog.csdn.net/zyz00000000/article/details/107957777"
        },
        js_limit_number_length: {
            "article_id": "89002939",
            "title": "js正则表达式——限制数字长度",
            "description": "1. 比如：限制字数长度为4\n\nvar reg = /^\\d{4}$/\n\n解释：以四个数字开头并且也是以这四个数字结尾，所以就限制了数字长度，只能为四。\n\n注意：也是以这四个数\n\n\n\n如果是：以四个数字开头，且以四个数字结尾的写法又是什么呢？那么字数长度就没有受到限制了，当然了，至少是四个。\n\n不会写！\n\n...",
            "content": "<h3>1. 比如：限制字数长度为4</h3>\n\n<pre>\n<code class=\"language-javascript\">var reg = /^\\d{4}$/g</code></pre>\n\n<p><strong>解释：</strong>以四个数字开头并且<span style=\"color:#f33b45\"><strong>也是以这四个数</strong></span>字结尾，所以就限制了数字长度，只能为四。</p>\n\n<p><strong>注意：</strong><span style=\"color:#f33b45\"><strong>也是以这四个数</strong></span></p>\n\n<p>&nbsp;</p>\n\n<p><strong>如果是：以四个数字开头，且以四个数字结尾的写法又是什么呢？那么字数长度就没有受到限制了，当然了，至少是四个。</strong></p>\n\n<p><strong>不会写！</strong></p>\n\n<p><strong>&mdash;&mdash;&mdash;&mdash;&mdash;分割线&mdash;&mdash;&mdash;&mdash;&mdash;</strong></p>\n\n<p><strong>时隔多年，看到了这篇文章，早就知道答案的我，今天（2020.8.3）来写一下这个答案，要告诫自己，不管平时工作多忙，还是要每天坚持看博客，更新博客。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var reg = /^\\d{4}((\\w|\\W)*\\d{4}$|\\d{0,3}$)/g\nvar str = \"1234asfwerqgrwg~!@#$%^&amp;*()_+reqqg5678\";\nvar str1 = \"12345\";\nvar str2 = \"1234a567\";\nconsole.log(str.match(reg))//[\"1234asfwerqgrwg~!@#$%^&amp;*()_+reqqg5678\"]\nconsole.log(str1.match(reg))//[\"12345\"]\nconsole.log(str2.match(reg))//null</code></pre>\n\n<p><strong>分析一下：</strong></p>\n\n<p>必须是要有四个数字开头：^\\d{4}</p>\n\n<p>两种情况：（1）字符串的长度大于等于8，（2）和字符串的长度大于等于4，小于7</p>\n\n<p>情况（1）必须以四个数字结尾，中间可以有任意个任意字符，（\\w|\\W）*，大家注意表示任意字符的方法，就是匹配全部字符嘛</p>\n\n<p>你可以用\\d|\\D，即数字和非数字的交集；也可以用\\w|\\W，单字字符和非单字字符的交集，就表示全部任意字符。</p>\n\n<p>情况（2）必须是0~3个数字结尾，即&ldquo;1234&rdquo;四个数字、&ldquo;12345&rdquo;五个数字、六个数字、七个数字，这几种情况<br />\n&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "正则表达式限制数字长度",
            "categories": "正则表达式",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1
        },
        reg_interview_question: {
            "article_id": "108062090",
            "title": "正则表达式面试题",
            "description": "1、给一个连字符串例如：get-element-by-id转化成驼峰形式。\n\n\nvar str = \"get-element-by-id\";\nvar reg = /-(\\w)/g;\nconsole.log(str.replace(reg, function(match, p1){\n    return p1.toString().toUpperCase();\n})); //getElementById\n\n字符串的转为大写：toUpperCase()；转为小写：toLowerCase()\n\n\n\n2、匹配一..",
            "content": "<p><strong>切记：正则表达式匹配的是片段，是符合条件的某段字符串或几段字符串！</strong></p>\n\n<p><strong>1、&nbsp;给一个连字符串例如：get-element-by-id转化成驼峰形式。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"get-element-by-id\";\nvar reg = /-(\\w)/g;\nconsole.log(str.replace(reg, function(match, p1){\n    return p1.toString().toUpperCase();\n})); //getElementById</code></pre>\n\n<p>字符串的转为大写：toUpperCase()；转为小写：toLowerCase()</p>\n\n<p>&nbsp;</p>\n\n<p><strong>2、匹配一个[1-255]的数字</strong></p>\n\n<p>括号内匹配符合条件的值都可以，但是要以匹配的括号内容开头且结尾。这样就不会匹配345中的34和5了。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"248\";\nvar reg = /^([1]\\d\\d|[2][0-4]\\d|[2]5[0-5]|[1-9]\\d|[1-9])$/\nconsole.log(str.match(reg));</code></pre>\n\n<p><strong>3、匹配ip地址</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"248.89.31.26\";\nvar reg = /^(([1]\\d\\d|[2][0-4]\\d|[2]5[0-5]|[1-9]\\d|[1-9])\\.){3}([1]\\d\\d|[2][0-4]\\d|[2]5[0-5]|[1-9]\\d|[1-9])$/\nconsole.log(str.match(reg));</code></pre>\n\n<p>打印结果：</p>\n\n<p><img alt=\"\" height=\"183\" src=\"https://img-blog.csdnimg.cn/20200817200423780.png\" width=\"668\" /></p>\n\n<p><strong>4、关于开头和结尾</strong></p>\n\n<p>开头^和结尾$运算符 的优先级只大于 或 |&nbsp;&nbsp;</p>\n\n<p>开头或结尾运算符修饰的是整个正则，除非有或</p>\n\n<p>比如：匹配以多个数字和m开头的片段</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12346m\";\nvar reg = /^\\d+m/g;\nconsole.log(str.match(reg)); //[\"12346m\"]</code></pre>\n\n<p>比如：匹配以多个数字和m结尾的片段</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12346m\";\nvar reg = /\\d+m$/g;\nconsole.log(str.match(reg)); //[\"12346m\"]</code></pre>\n\n<p>&nbsp;再比如：匹配以多个数字和m开头，且以这个几个数字和这个m结尾！因为^$同时修饰的是匹配到的该片段！</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12346m\";\nvar reg = /^\\d+m$/g;\nconsole.log(str.match(reg)); //[\"12346m\"]</code></pre>\n\n<p>如果有或|，匹配以多个数字开头的片段 或者 以m结尾的片段</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12346m\";\nvar reg = /^\\d+|m$/g;\nconsole.log(str.match(reg)); //[\"12346\", \"m\"]</code></pre>\n\n<p><strong>5、量词之间要用()，连着两个量词的正则表达式是错误的！</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var reg = /\\d{1,3}*/g</code></pre>\n\n<p>&nbsp;量词{1,3}后面再跟量词*将会报错！！！</p>\n\n<p><strong>6、子表达式嵌套子表达式，他们的先后顺序，谁是$1，谁又是$2</strong></p>\n\n<p>下面表达式reg1，<strong>(\\d{1,3})</strong>是第一个子表达式；<strong>((\\d{3})*)</strong>第二个子表达式；<strong>(\\d{3})</strong>第三个子表达式。</p>\n\n<p><strong>特点：正则中，括号从左到右，从外到内</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1234567890\";\nvar reg1 = /^(\\d{1,3})((\\d{3})*)$/\nvar reg2 = /\\d{3}/g\nconsole.log(str.match(reg1))\nvar replaceStr = str.replace(reg1, function (match, p1, p2, p3) {\n    return p1 + p2.replace(reg2, \",$&amp;\")\n})\nconsole.log(replaceStr);</code></pre>\n\n<p><strong>7、子表达式后面跟有量词，最后一次匹配到的内容，作为子表达式的值</strong></p>\n\n<p>我们知道非全局匹配下的match方法的结果和exec方法的结果一样，会得到所有的匹配信息，包括子表达式的内容。</p>\n\n<p>以下例子中，子表达式的值是456，匹配多个3数字，最后一个3数字的内容。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345678\";\nvar reg = /(\\d{3})*/\nconsole.log(str.match(reg))\n[\"123456\", \"456\", index: 0, input: \"12345678\", groups: undefined]</code></pre>\n\n<p><strong>&nbsp;8、| 或情况下的子表达式可能是undefined</strong></p>\n\n<p>在或 | 的情况下，有的子表达式未匹配到内容，其值是undefined。</p>\n\n<p>结果会有第二个子表达式的值，但值是undefined。如果继续往后面匹配，比如用exec多次执行往后匹配，当匹配b1的时候，第一个子表达式是undefined，而第二个子表达式是b1。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"a1b1a2b2\";\nvar reg = /(a\\d)|(b\\d)/\nconsole.log(str.match(reg))\n\n[\"a1\", \"a1\", undefined, index: 0, input: \"a1b1a2b2\", groups: undefined]</code></pre>\n\n<p><strong>9、如何让正则表达式匹配先匹配后面的片段，再匹配前面的片段</strong></p>\n\n<p>我们知道正则表达式是从字符串的前面开始匹配，即从左到右进行匹配寻找符合条件的片段，比如：</p>\n\n<p>下面字符串&ldquo;12345&rdquo;，匹配连续的两个数字，会匹配到12 和 34；而不是 23 和45</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345\";\nvar reg = /\\d{2}/g\nconsole.log(str.match(reg)); //[\"12\", \"34\"]</code></pre>\n\n<p>如果就是想匹配23和45呢？我们必须使用$符，因为我们无法从右往左匹配，只能先把2345匹配出来，再进行匹配拆分：</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345\";\nvar reg1 = /(\\d{2})+$/g\nvar reg2 = /\\d{2}/g;\nconsole.log(str.match(reg1)[0].match(reg2)) // [\"23\", \"45\"]</code></pre>\n\n<p><strong>10、关于$符，结尾的作用时的匹配匹配规则</strong></p>\n\n<p>没有$符时，\\d{1,3}会先匹配三个数字，(\\d{3})*再匹配三个数字。然后\\d{1,3}再匹配剩下的78</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345678\";\nvar reg = /\\d{1,3}(\\d{3})*/g\nconsole.log(str.match(reg))  // [\"123456\", \"78\"]</code></pre>\n\n<p>&nbsp;如果非全局匹配：也先匹配3个数字</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345678\";\nvar reg = /\\d{1,3}(\\d{3})*/\nconsole.log(str.match(reg))\n // [\"123456\", \"456\", index: 0, input: \"12345678\", groups: undefined]</code></pre>\n\n<p>如果有开头符号，也是先匹配3个数字</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345678\";\nvar reg = /^\\d{1,3}(\\d{3})*/g\nconsole.log(str.match(reg)) // [\"123456\"]</code></pre>\n\n<p>我们再来看看有$符时，<strong>\\d{1,3}</strong>就会优先让<strong>(\\d{3})*</strong>先匹配符合的数字，剩下的再由<strong>\\d{1,3}</strong>匹配，这样<strong>\\d{1,3}就不会先匹配了。</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345678\";\nvar reg = /\\d{1,3}(\\d{3})*$/g\nconsole.log(str.match(reg)) // [\"12345678\"]</code></pre>\n\n<p>匹配结果：[&quot;12345678&quot;]，345和678是<strong>(\\d{3})*匹配到的，12是\\d{1,3}匹配到的。</strong></p>\n\n<p><strong>如果同时有开头符^和结束符$:还是先倒着匹配，即先看&nbsp;(\\d{3})* 再看\\d{1,3}。这就是$符的特性</strong></p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"12345678\";\nvar reg = /^\\d{1,3}(\\d{3})*$/g\nconsole.log(str.match(reg)) //[\"12345678\"]</code></pre>\n\n<p><strong>11、重点例题</strong></p>\n\n<p>给一串数字添加千位分号</p>\n\n<p>比如：</p>\n\n<p>1234567 &mdash;&mdash;》 1,234,567&nbsp;</p>\n\n<p>12 &mdash;&mdash;》 12</p>\n\n<p>123456 &mdash;&mdash;》 123,456</p>\n\n<p>实现：通过reg1的$符的特性和^$符锁定，得到两部分，第一部分是前面剩余的数字，第二部分是后面长度为3的倍数部分。</p>\n\n<p>当执行replace方法时，通过子表达式得到第二部分的内容，在用新的正则reg2添加逗号，从而实现</p>\n\n<pre>\n<code class=\"language-javascript\">var str = \"1234567890\";\nvar reg1 = /^(\\d{1,3})((\\d{3})*)$/\nvar reg2 = /\\d{3}/g\nconsole.log(str.match(reg1))\nvar replaceStr = str.replace(reg1, function (match, p1, p2, p3) {\n    return p1 + p2.replace(reg2, \",$&amp;\")\n})\nconsole.log(replaceStr);</code></pre>\n\n<p><strong>12、gulp打包的时候，需要对html htm jsp文件中的js、css的版本号进行修改。我们要匹配文件中的css和js</strong></p>\n\n<p>以css为例：注意myselfCss是我自己实现的，匹配注意点：</p>\n\n<p>（1）有些位置是可以有空格的，可以适当的进行匹配；但是href属性的地址中不允许有空格。</p>\n\n<p>（2）.*来匹配不确定的部分，比如rel=&quot;stylesheet&quot;，link标签中不仅有ref属性，重要的是必须有href属性。</p>\n\n<p>（3）版本的标示不一定非要用字符t，可以是任意字符，但是为了规范，项目写的时候，就字符来标示版本变量吧。</p>\n\n<p>这里必须吐槽一下：regCss是他们写项目的时候写的，用了很多没必要的()，是对正则匹配的优先级不了解造成的。</p>\n\n<pre>\n<code class=\"language-javascript\">var str = '&lt;link rel=\"stylesheet\" href=\" https://m/aaf_da-adas.css?t=12341234\"&gt;';\nvar regCss = /&lt;link.*href=(\"|')([^\\\"\\']+.css((\\?[a-z]|\\_|\\.)=([0-9a-zA-Z\\.\\_]+))*)(\"|')(.*)?\\/?&gt;/g;\nvar myselfCss = /&lt;link.*href\\s*=\\s*(?:\"|')\\s*[^ ]*\\.css\\?[a-z]=[\\w.]*\\s*(?:\"|')\\s*&gt;/g\nconsole.log(str.match(myselfCss))\nconsole.log(str.match(regCss))\n\n[\"&lt;link rel=\"stylesheet\" href=\" https://m/aaf_da-adas.css?t=12341234\"&gt;\"]\n[\"&lt;link rel=\"stylesheet\" href=\" https://m/aaf_da-adas.css?t=12341234\"&gt;\"]</code></pre>\n\n<p><strong>13、相关的一些练习题</strong></p>\n\n<pre>\n<code class=\"language-javascript\">    //清除\"数字\"和\".\"以外的字符\n    var str = \"123@ da.2.99h\";\n    var reg = /[^\\d.]/g\n    console.log(str.replace(reg,\"\"))\n\n    //验证第一个字符是数字\n    var str = \"123@ da.2.99h\";\n    var reg = /^\\d/;\n    console.log(str.match(reg))\n\n    //只保留第一个字符, 清除多余的\n    var str = \"safsfw1231\";\n    var reg = /(.).*/g;//匹配整个str\n    console.log(str.replace(reg,\"$1\"))//用第一个字符替换整个str\n\n    //只能输入两个小数\n    var str = \"0.12\";\n    var reg = /^\\d+\\.\\d{2}$/g\n    console.log(str.match(reg))\n    &lt;/script&gt;</code></pre>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "正则表达式练习题",
            "categories": "正则表达式",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1
        },
        js_recursion: {
            "article_id": "82930758",
            "title": "JavaScript递归例子",
            "description": "1. 递归的特点：找规律、找出口\n\n（1）求一个数的阶乘：\n\n\n        function factorial(n){\n            if(n == 0 || n == 1){\n                return 1;\n            }\n            return n * factorial(n-1);\n        }\n\n（2）求斐波拉数列第n项的...",
            "content": "<p>1. 递归的特点：找规律、找出口</p>\n\n<p>（1）求一个数的阶乘：</p>\n\n<pre class=\"has\">\n<code class=\"language-javascript\">        function factorial(n){\n            if(n == 0 || n == 1){\n                return 1;\n            }\n            return n * factorial(n-1);\n        }</code></pre>\n\n<p>（2）求斐波拉数列第n项的值（1 1 2 3 5 8 13...）</p>\n\n<pre class=\"has\">\n<code class=\"language-javascript\">        function fabona(n){\n            if(n == 1 || n == 2){\n                return 1;\n            }\n            return fabona(n-1) + fabona(n-2);\n        }</code></pre>\n\n<p>（3）求斐波拉数列的前n项值</p>\n\n<pre class=\"has\">\n<code class=\"language-javascript\">        function fabona(n){\n            if(n == 1 || n == 2){\n                return 1;\n            }\n            return fabona(n-1) + fabona(n-2);\n        }\n        function fabonaN(n){\n            var str = '';\n            for(var i = 1; i &lt;= n; i++){\n                str = str + ' ' + fabona(i);\n            }\n            return str;\n        }</code></pre>\n\n<p>（4）在jQuery中，extend方法用到了递归</p>\n\n<p>&nbsp;</p>\n",
            "markdowncontent": "",
            "tags": "web前端,JavaScript,递归,递归题目",
            "categories": "前端web,JavaScript问题摘要",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1
        },
        js_basic_knowledge: {
            "article_id": "82930156",
            "title": "JavaScript运算符和判断语句",
            "description": "1. &amp;gt;   &amp;lt;   \n\n规则：数字比大小；字符串（内容是字母、数字）比的是第一位的ascll码值\n\n2. ==   !=   ===   !==\n\n主要比较：0 null undefined NaN \n\n结果为true：null == null , null === null ; undefined == undefined , undefined === undefined; nu...",
            "content": "<p><strong>1. &gt;&nbsp; &nbsp;&lt;&nbsp; &nbsp;</strong></p>\n\n<p>规则：数字比大小；字符串（内容是字母、数字）比的是第一位的ascll码值</p>\n\n<p><strong>2. ==&nbsp; &nbsp;!=&nbsp; &nbsp;===&nbsp; &nbsp;!==</strong></p>\n\n<p>主要比较：0 null undefined NaN&nbsp;</p>\n\n<p>结果为true：<strong>null == null</strong> , <strong>null === null</strong> ; <strong>undefined == undefined</strong> , <strong>undefined === undefined</strong>; <strong>null == undefined</strong></p>\n\n<p>结果为false：<strong>null &gt; 0 ,null &lt; 0 ,null == 0</strong>; <strong>undefined &gt; 0, undefined &lt; 0 ,undefined == 0</strong>;&nbsp; <strong>null != undefined</strong>; <strong>NaN == NaN</strong></p>\n\n<p><strong>3. 运算符优先级</strong></p>\n\n<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\n\t<tbody>\n\t\t<tr>\n\t\t\t<td><strong>运算符</strong></td>\n\t\t\t<td><strong>描述</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>.&nbsp; [ ]&nbsp; ()&nbsp;</strong></td>\n\t\t\t<td>&nbsp;</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>++&nbsp; --&nbsp; !&nbsp; delete&nbsp; new&nbsp; typyof&nbsp; void</strong></td>\n\t\t\t<td><strong>自增&nbsp; 自减 非&nbsp; 删除 创建对象 数据类型</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>*&nbsp; &nbsp;/&nbsp; &nbsp;%</strong></td>\n\t\t\t<td><strong>乘&nbsp; 除&nbsp; 取余</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>+&nbsp; &nbsp;-</strong></td>\n\t\t\t<td>加 减</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>&gt;&nbsp; &nbsp; &lt;&nbsp; &nbsp; &gt;=&nbsp; &nbsp; &lt;=&nbsp; &nbsp; instanceof</strong></td>\n\t\t\t<td>大于，小于，大于等于，小于等于</td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>==&nbsp; &nbsp;!=&nbsp; &nbsp; ===&nbsp; &nbsp; !==</strong></td>\n\t\t\t<td><strong>等于，不等于，绝对等于，绝对不等于</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>&amp;&amp;</strong></td>\n\t\t\t<td><strong>逻辑与</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>||</strong></td>\n\t\t\t<td><strong>逻辑或</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>?:</strong></td>\n\t\t\t<td><strong>三目运算符</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>=</strong></td>\n\t\t\t<td><strong>赋值</strong></td>\n\t\t</tr>\n\t\t<tr>\n\t\t\t<td><strong>,</strong></td>\n\t\t\t<td><strong>多重求值</strong></td>\n\t\t</tr>\n\t</tbody>\n</table>\n\n<p><strong>4. JavaScript 的 typeof 返回哪些数据类型？</strong></p>\n\n<ul>\n\t<li>基础类型包括：Number、String、Boolean、Null、Undefined、Symbol（该类型位 ES2015（ES6） 中新增类型）</li>\n\t<li>引用类型包括：Object typeof 运算符把类型信息以字符串形式返回，需要注意的是 typeof 返回的类型和 JavaScript 定义的类型有细微的差异。</li>\n\t<li>typeof 返回七种可能的值：&ldquo;number&rdquo;、&ldquo;string&rdquo;、&ldquo;boolean&rdquo;、&ldquo;object&rdquo;、&quot;symbol&quot;、&ldquo;function&rdquo;和&ldquo;undefined&rdquo;。</li>\n\t<li>\n\t<p>Symbol类型的特点：</p>\n\n\t<p>Symbol(&#39;key&#39;) !==&nbsp;Symbol(&#39;key&#39;) //true</p>\n\n\t<p>Symbol类型的对象永远不相等，即便创建它们的时候传入了相同的值，因此，可借助此特性解决属性名的冲突问题（比如适用于多人编码的时候），这也是该数据类型存在的主要用途，意为标记</p>\n\n\t<p>例子：</p>\n\n\t<p>var sym = Symbol(&#39;foo&#39;);</p>\n\n\t<p>var obj = {[sym] : 1};</p>\n\n\t<p>obj[sym] === 1 //true</p>\n\t</li>\n</ul>\n\n<p><strong>5.强制类型转换：</strong></p>\n\n<p>（1）Number()：会将数据类型转换为number，但是值：除了<strong>数字和null</strong>会转换为对应值，其他的值全是&ldquo;NaN&rdquo;。</p>\n\n<p>（2）parseInt()：会将数据类型转换为number，但是值：除了<strong>数字和数字开头的字符串</strong>转换对应的数字，其他的值全是&ldquo;NaN&rdquo;</p>\n\n<p>注意：<strong>若</strong>parseInt内有基数，该基数指向parseInt内的值，比如：parseInt(&#39;abcd&#39;, 16)，即&lsquo;abcd&rsquo;是16进制的，parseInt的结果是10进制的</p>\n\n<p>（3）parseFloat()： 会将数据类型转换为number，但是值：除了数字和数字开头的字符串转换为对应的数字，其他值全是&ldquo;NaN&rdquo;</p>\n\n<p>（4）String()：会将数据类型转换为string，值：会得到字符串</p>\n\n<p>（5）.toString()：<strong>这是个方法</strong>，会将数据类型转换为string，<strong>若</strong>有基数，会将10进制的数字转换为指定进制的字符串，比如：</p>\n\n<p>var n = 12; n.toString(8) 的结果：字符串类型的：14</p>\n\n<p>（6）Boolean()：会将数据类型转换为boolean，值：除了6个false值，其他的值全是true</p>\n\n<p>6个false值：false 0 null undefined NaN &#39;&#39;</p>\n\n<p><strong>6. 隐式类型转换</strong></p>\n\n<p>会转换为number类型的：<strong>++&nbsp; --&nbsp; -&nbsp; *&nbsp; /&nbsp; %&nbsp; isNaN()</strong></p>\n\n<p>会转换为boolean类型的：<strong>&gt;&nbsp; &lt;&nbsp; &gt;=&nbsp; &lt;=&nbsp; ==&nbsp; !</strong></p>\n\n<p><img alt=\"\" class=\"has\" height=\"189\" src=\"https://img-blog.csdn.net/20181003111143562?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70\" width=\"316\" /></p>\n",
            "markdowncontent": "",
            "tags": "web前端,JavaScript,运算符,判断语句",
            "categories": "前端web,JavaScript,JavaScript知识总结",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1
        },
        browser_render: {
            "article_id": "82911888",
            "title": "浏览器渲染原理和性能优化",
            "description": "\n\n1. 渲染原理：\n\n(1)处理HTML生成DOM树\n\n(2)处理CSS生成CSSDOM树\n\n(3 )将两树合并成render树\n\n(4 )对render树进行布局计算\n\n(5)将render树中的每一个节点绘制到屏幕上\n\n细化分析:\n1.浏览器把获取到的html代码解析成1个Dom树，html中的每个标签都是Dom树中的1个节点，根节点就是我们常用的document对象(&amp;lt;html&amp;gt...",
            "content": "<p>&nbsp;</p>\n\n<p><strong>1. 渲染原理：</strong></p>\n\n<p>(1)处理HTML生成DOM树</p>\n\n<p>(2)处理CSS生成CSSDOM树</p>\n\n<p>(3&nbsp;)将两树合并成render树</p>\n\n<p>(4&nbsp;)对render树进行布局计算</p>\n\n<p>(5)将render树中的每一个节点绘制到屏幕上</p>\n\n<p><strong>细化分析:</strong><br />\n1.浏览器把获取到的html代码解析成1个Dom树，html中的每个标签都是Dom树中的1个节点，根节点就是我们常用的document对象(&lt;html&gt;&nbsp;tag)，当然这里包含用js动态创建的dom节点</p>\n\n<p><br />\n2浏览器把所有样式(主要包括Css和浏览器的默认样式设置)解析成样式结构体，在解析的过程中会去掉浏览器不能识别的样式,生成CSSDOM树</p>\n\n<p><br />\n3.DOM&nbsp;tree和CSSDOM&nbsp;tree合并成render&nbsp;tree，render&nbsp;tree中每个node都有自己的style，而且render&nbsp;tree不包含隐藏的节点(比如display:none的节点，还有无样式head节点)，因为这些节点不会用于呈现，而且不会影响呈现的，注意visillty:hiddedn隐藏的元素还是会包含到render&nbsp;tree中的，因为vsibilt:hidden会影响布局(ayout)，会占有空间。</p>\n\n<p><br />\n4.render&nbsp;tree构建完毕之后根据样式计算布局，布局阶段的输出结果称为&ldquo;盒模型&rdquo;(&nbsp;box&nbsp;model)。盒模型精确表达了窗口中每个元素的位置和大小，而且所有的相对的度量单位都被转化成了屏幕上的绝对像索位置(根据css2的标准，render&nbsp;tree中的每个节点都称为box(Box&nbsp;dimensions---盒子模型)&nbsp;,&nbsp;box所有属性:width,height,margin,padding,left,top,border等。)</p>\n\n<p><br />\n5.将这些信息渲染为屏幕上每个真实的像素点了。这个阶段称为&quot;绘制&rdquo;，&nbsp;或者&ldquo;栅格化<br />\n<br />\n<strong>2.&nbsp;重绘、重排</strong><br />\n(1&nbsp;)我们计算它们在当前设备中准确的位置和尺寸。这正是布局阶段要做的的工作，该阶段在英语中也被称为&ldquo;回流&rdquo;(&nbsp;reflow)，当render&nbsp;tree中的一部分(或全部)因为元素的规模尺寸,布局，隐藏等改变而需要重新构建。也会回流(其实我觉得叫重新布局更简单明了些)。每个页面至少需要一-次回流，就是在页面第一-次加载的时候。</p>\n\n<p>(2)重绘(repaints&nbsp;)当render&nbsp;tree中的一些元素需要更新属性，而这些属性只是影响元素的外观，风格，而不会影响布局的，比如background-color.则就叫称为重绘。<br />\n(3)重绘，重排会影响性能，&nbsp;在浏览器的performance（性能）下，刷新看执行时间</p>\n\n<p>蓝色:网络通信和HTML解析</p>\n\n<p>黄色:&nbsp;JavaScript执行</p>\n\n<p>紫色:样式计算和布局，即重排</p>\n\n<p>绿色:重绘</p>\n\n<p>(4)触发重排的方法:</p>\n\n<p>以下这些属性和方法需要返回最新的布局信息，重新计算渲染树，就会造成回流，能发重排以返回正确的值。建议将他们合并到一起操作，可以减少回流的次数。</p>\n\n<p>这些属性包括:&nbsp;</p>\n\n<p>offsetTop.&nbsp;offsetLeft.&nbsp;offsetWidth,&nbsp;offsetHeight&nbsp;;&nbsp;scrolTopscrolleft.&nbsp;scrollWidth.&nbsp;scrollHeight&nbsp;;&nbsp;dientTop.&nbsp;clientLeft.&nbsp;clientWidth、dientHeight&nbsp;;&nbsp;getComputedStyleO、currentStyle(&nbsp;).</p>\n\n<p>提高网页性能，就是要降低&quot;重排&quot;和&quot;重绘&quot;的频率和成本，尽量少触发重新渲染。</p>\n\n<p>DOM变动和样式变动，都会触发重新渲染。但是，浏览器已经很智能了，会尽量把所有的变动集中在一起，&nbsp;排成一一个队列&nbsp;</p>\n\n<p>然后一次性执行，尽量避免多次重新渲染div.style.color&nbsp;=&nbsp;&#39;red&#39;;<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;div.style.background=&#39;yellow&#39;;div.style.left&nbsp;=&nbsp;&#39;10px&#39;;div.style.width&nbsp;=&nbsp;&#39;20px&#39;;<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;但是浏览器只会触发一次重排和重绘。<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;般来说，样式的写操作之后,如果有上面那些属性的读操作，都会引发浏览器立即重排，这种重排，不会形成之前队列优化div.style.color=&nbsp;&#39;red&#39;;<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var&nbsp;height&nbsp;=&nbsp;div.offseHeight;div.style.height&nbsp;=&nbsp;height&nbsp;+&nbsp;&#39;px&#39;;</p>\n\n<p>Bad&nbsp;:<br />\n&nbsp; &nbsp; &nbsp; div.style.left&nbsp;=&nbsp;div.offsetLeft&nbsp;+&nbsp;&#39;px&#39;;div.style.top&nbsp;=&nbsp;div.offsetTop+&nbsp;&#39;px&#39;;</p>\n\n<p>重排重绘两次</p>\n\n<p>Good:<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Var&nbsp;left&nbsp;=&nbsp;div.offsetLeft&nbsp;+&nbsp;&#39;px&#39;;Var&nbsp;top&nbsp;=&nbsp;div.offsetTop+&nbsp;&#39;px&#39;;div.style.left&nbsp;=&nbsp;left;div.style.top&nbsp;=&nbsp;top;<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;放到队列一次执行重排重绘一次<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我们来测试一下js动态添加10000个li不同颜色而且设置宽度所耗费renderpainting的时间吧<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(5)理论上的解决优化办法:<br />\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.DOM的多个读操作(或多个写操作)，&nbsp;应该放在一起。&nbsp;不要两个读操作之间，加入一个写操作。<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.离线操作DOM如使用隐藏元素documentcreateDocumentFagment()&nbsp; &nbsp;cloneNode();<br />\n&nbsp; &nbsp; &nbsp; 3.修改样式的时候添加类名，或次性添加到dom.style.cssText上等</p>\n\n<p>3. 3D属性可以提高性能</p>\n\n<p>（1）尽可能的多用硬件能力，使用3D属性来开启GPU加速，前提是用户电脑性能较好，因为3D效果会消耗更多的内存。</p>\n\n<pre class=\"has\">\n<code class=\"language-html\">-webkit-transform: translate3d(0,0,0);\n-moz-transform: translate3d(0,0,0);\n-ms-transform: translate3d(0,0,0);\ntransform: translate3d(0,0,0);</code></pre>\n\n<pre class=\"has\">\n<code class=\"language-html\">-webkit-transform: rotate3d(1,1,1,30deg);\n-moz-transform: rotate3d(1,1,1,30deg);\n-ms-transform: rotate3d(1,1,1,30deg);\ntransform: rotate3d(1,1,1,30deg);</code></pre>\n\n<p>（2）注意：如果动画过程中有闪烁现象，常出现在动画开始的时候，可以尝试下面的hack方法：测试出来的方法</p>\n\n<pre class=\"has\">\n<code class=\"language-html\">            -webkit-backface-visibility: hidden;\n            -moz-backface-visibility: hidden;\n            -ms-backface-bisibility: hidden;\n            backface-visibility: hidden;\n            -webkit-perspective: 1000;\n            -moz-perspective: 1000;\n            -ms-perspective: 1000;\n            perspective: 1000;</code></pre>\n\n<p>（3）还有比如：transform：translate3d（）移动效果的流畅度，远高于任性形式下的left属性值变化（animation，transition，JavaScript定时器等）</p>\n\n<p>（4）尽可能的避免使用box-shadow阴影和gradients渐变，这个两个属性是页面性能杀手</p>\n\n<p>（5）尽可能的让动画元素不在文档流中，及减少重排</p>\n\n<p>position：fixed；</p>\n\n<p>position：absolute；</p>\n\n<p>（6）优化DOM reflow</p>\n",
            "markdowncontent": "",
            "tags": "web前端,渲染机制,重排重绘,3D性能优化",
            "categories": "前端web",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1
        },
        browser_work_principle: {
            "article_id": "105695064",
            "title": "浏览器工作原理",
            "description": "一、浏览器的组成部分，组件\n\n每个组件的作用是什么\n\n\n\n二、输入url的时候获取到服务器数据发生了什么\n\n\n\n三、对获取的数据进行渲染\n",
            "content": "<h1>一、浏览器的组成部分及工作原理</h1>\n\n<p>&nbsp;</p>\n\n<h3>1. 浏览器主要组成部分</h3>\n\n<p>参考文章：<a href=\"https://segmentfault.com/a/1190000018277184\">https://segmentfault.com/a/1190000018277184</a></p>\n\n<p><strong>（1）The User Interface用户界面</strong></p>\n\n<p>为Browser Engine提供接口，地址栏、书签、前进后退按钮等等，出了document文档，其他的都属于User Interpreter</p>\n\n<p><strong>（2）The Browser Engine 浏览器引擎、</strong></p>\n\n<p>为The Rendering Engine提供接口，如刷新、向前、退后等操作，让Rendering Engine重新渲染。</p>\n\n<p>也为User Interface提供各种与错误、加载进度相关的消息</p>\n\n<p><strong>（3）The Render Engine 渲染引擎、</strong></p>\n\n<p>浏览器的渲染引擎就是浏览器的内核，用来解析HTML、XHTML、svg、css等文件，进行页面的渲染的。</p>\n\n<p>js引擎和浏览器内核</p>\n\n<p><img alt=\"\" height=\"373\" src=\"https://img-blog.csdnimg.cn/20200429223632557.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"678\" /></p>\n\n<p>浏览器是如何渲染页面的？</p>\n\n<p>从服务器获取HTML文件，自上而下的对html文件进行解析，同时构建dom tree。</p>\n\n<p>解析的过程中，遇到link标签，从服务器加载对应的css文件，同时构建css rule tree。</p>\n\n<p>当遇到script标签时，由于JavaScript会对dom进行修改，所以停止dom tree和css rule tree的构建，从服务器加载该js文件，下载完成后，浏览器开启<strong>js引擎组件</strong>，直接对该js文件进行解析执行。所以的script标签都会执行该操作。</p>\n\n<p>然后继续构建dom tree和css rule tree，构建完成之后，合并为render tree，这些操作是在<strong>Rendering Engine</strong>组件中完成的。</p>\n\n<p>计算标签的尺寸位置大小等等属性，浏览器进行layout布局，paint绘制页面（调用的是<strong>UI Backend组件</strong>，操作系统）</p>\n\n<p><strong>（4）The Networking 网络、</strong></p>\n\n<p>基于互联网HTTP和FTP协议，处理网络请求。</p>\n\n<p>网络模块负责Internet communication and security，character set translations and MIME type resolution。</p>\n\n<p>另外网络模块还提供获得到文档的缓存，以减少网络传输。</p>\n\n<p><strong>（5）The JavaScript Interpreter： js解析器</strong></p>\n\n<p>解释和运行网站上的js代码，得到的结果传输到Rendering Engine来展示。</p>\n\n<p><img alt=\"\" height=\"239\" src=\"https://img-blog.csdnimg.cn/20200430132630476.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70\" width=\"478\" /></p>\n\n<p>事件循环：</p>\n\n<p><a href=\"https://segmentfault.com/a/1190000018134157\">https://segmentfault.com/a/1190000018134157</a></p>\n\n<p><a href=\"https://zhuanlan.zhihu.com/p/33058983\">https://zhuanlan.zhihu.com/p/33058983</a></p>\n\n<p>阮一峰对event loop的理解：<a href=\"http://www.ruanyifeng.com/blog/2014/10/event-loop.html\">http://www.ruanyifeng.com/blog/2014/10/event-loop.html</a></p>\n\n<p>js定时器机制：<a href=\"https://segmentfault.com/a/1190000015353521\">https://segmentfault.com/a/1190000015353521</a></p>\n\n<p>setInterval调用有可能会被废弃：主线程的任务执行时间短时间内又定义了相同的setInterval，导致原本的setInterval还在事件队列中未执行，又被重新定义，原本的setInterval会被替代。<br />\nsetInterval的连续执行：定时器的任务执行时间&gt;定时器的时间。</p>\n\n<p>可以这样理解setInterval，连续执行是因为他自己，被废弃执行是因为主线程又重新定义。</p>\n\n<p>setTimeout和setInterval的执行时间都会受到宏任务的影响。</p>\n\n<p><strong>（6）The UI Backend： UI后端</strong></p>\n\n<p>一些底层操作系统的接口</p>\n\n<p><strong>（7）The Data Storage 数据储存</strong></p>\n\n<p>管理用户数据，浏览器需要在硬盘中保存类似cookie的各种数据。</p>\n\n<p><img alt=\"img\" src=\"https://imgconvert.csdnimg.cn/aHR0cHM6Ly9zZWdtZW50ZmF1bHQuY29tL2ltZy9yZW1vdGUvMTQ2MDAwMDAxODI3NzE4Nw?x-oss-process=image/format,png\" /></p>\n\n<p><strong>还需要注意的是：搜索引擎是指：百度、搜狗、Google、雅虎。&nbsp;</strong></p>\n",
            "markdowncontent": "",
            "tags": "javascript,html",
            "categories": "前端web",
            "type": "original",
            "status": 0,
            "read_type": "public",
            "reason": "",
            "resource_url": "",
            "original_link": "",
            "authorized_status": false,
            "check_original": false,
            "editor_type": 0,
            "not_auto_saved": 1
        }
    },
    treeData: [
        //基础知识：
        {
            parent: "基础知识",
            parentFlag: "basic",
            children: [
                {
                    childTitle: "基础知识(一)",
                    childFlag: "js_basic"
                },
                {
                    childTitle: "基础知识(二)",
                    childFlag: "js_basic_knowledge"
                },
                {
                    childTitle: "运算符优先级",
                    childFlag: "js_operational_character_priority"
                },{
                    childTitle: "JavaScript的位操作符",
                    childFlag: "js_bitwise_perators"
                },
                {
                    childTitle: "keyCode码",
                    childFlag: "js_keyCode"
                }
            ]
        },
        //高级知识
        {
            parent: "高级知识",
            parentFlag: "advancedKnowledge",
            children: [
                {
                    childTitle: "数据类型",
                    childFlag: "js_dataType"
                },
                {
                    childTitle: "typeOf",
                    childFlag: "js_typeOf"
                },
                {
                    childTitle: "instanceof",
                    childFlag: "js_instanceof"
                },
                {
                    childTitle: "toString",
                    childFlag: "js_toString"
                },
                {
                    childTitle: "constructor判断数据类型",
                    childFlag: "js_constructor"
                },
                {
                    childTitle: "操作符--隐式类型转换",
                    childFlag: "js_dataTypeTransform"
                },
                {
                    childTitle: "包装类对象",
                    childFlag: "js_primitiveObject"
                },{
                    childTitle: "with",
                    childFlag: "js_with"
                },
                {
                    childTitle: "递归",
                    childFlag: "js_recursion"
                }
            ]
        },

        //布尔值
        {
            parent: "布尔值",
            parentFlag: "",
            children: [
                {
                    childTitle: "Boolean",
                    childFlag: "js_boolean"
                }
            ]
        },
        //数字
        {
            parent: "数字",
            parentFlag: "",
            children: [
                {
                    childTitle: "Number",
                    childFlag: "js_number"
                }
            ]
        },
        //字符串
        {
            parent: "字符串",
            parentFlag: "",
            children: [
                {
                    childTitle: "String",
                    childFlag: "js_string"
                },
                {
                    childTitle: "String字符串方法",
                    childFlag: "js_string_funcs"
                }
            ]
        },
        //数组
        {
            parent: "数组",
            parentFlag: "",
            children: [
                {
                    childTitle: "js数组",
                    childFlag: "js_array"
                },
                {
                    childTitle: "js数组方法",
                    childFlag: "js_array_methods"
                },
                {
                    childTitle: "数组练习题",
                    childFlag: "js_array_problem"
                }
            ]
        },
        //对象
        {
            parent: "对象",
            parentFlag: "js_object",
            children: [
                {
                    childTitle: "new操作符",
                    childFlag: "object_new"
                },
                {
                    childTitle: "获取对象属性的方法",
                    childFlag: "js_object_getProperty_funcs"
                },
                {
                    childTitle: "数据属性和访问器属性",
                    childFlag: "object_property"
                },
                {
                    childTitle: "Object构造函数方法(一)",
                    childFlag: "object_method1"
                },
                {
                    childTitle: "Object构造函数方法(二)",
                    childFlag: "object_method2"
                },
                {
                    childTitle: "Object构造函数方法(三)",
                    childFlag: "object_method3"
                },
                {
                    childTitle: "继承和原型链",
                    childFlag: "object_inherit"
                }
            ]
        },
        //函数
        {
            parent: "函数",
            parentFlag: "function",
            children: [
                {
                    childTitle: "Function类型",
                    childFlag: "js_function"
                },
                {
                    childTitle: "内置构造函数原型",
                    childFlag: "js_prototype"
                },
                {
                    childTitle: "函数原型",
                    childFlag: "js_proto__"
                },
                {
                    childTitle: "arguments",
                    childFlag: "js_arguments"
                },
                {
                    childTitle: "闭包",
                    childFlag: "js_closure"
                },
            ]
        },
        //内置对象
        {
            parent: "内置对象",
            parentFlag: "",
            children: [
                {
                    childTitle: "日期对象Date",
                    childFlag: "js_internal_date"
                },
                {
                    childTitle: "Math对象 ",
                    childFlag: "js_math"
                }
            ]
        },
        {
            parent: "JOSN对象",
            parentFlag: "js_JSON",
            children: [ 
                {
                    childTitle: "JSON对象",
                    childFlag: "js_json"
                }
             ]
        },
        {
            parent: "正则表达式",
            parentFlag: "RegularExpression",
            children: [ 
                {
                    childTitle: "js正则表达式",
                    childFlag: "js_regular_expression"
                },{
                    childTitle: "正则表达式相关方法",
                    childFlag: "js_regular_expression_methods"
                },{
                    childTitle: "练习题一",
                    childFlag: "js_limit_number_length"
                },{
                    childTitle: "面试题",
                    childFlag: "reg_interview_question"
                }
             ]
        },
        //额外补充
        {
            parent: "额外补充",
            parentFlag: "js_garbageColletion",
            children: [
                {
                    childTitle: "垃圾回收机制",
                    childFlag: "js_garbageColletion"
                }, {
                    childTitle: "事件循环和任务队列",
                    childFlag: "js_EventLoop"
                },{
                    childTitle: "浏览器渲染和性能优化",
                    childFlag: "browser_render"
                },{
                    childTitle: "浏览器工作原理",
                    childFlag: "browser_work_principle"
                }
            ]
        },
    ]
}