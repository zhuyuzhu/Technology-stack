$.namespace('Zhu.jqueryData');
Zhu.jqueryData = {
    init: function () {
        console.log("Zhu.jqueryData.init执行")
    },
    data: {
        jquery_basic: `<div class="htmledit_views" id="content_views">
    
        <h3>前沿</h3>

        <p>1.&nbsp;文章收录自己在开发过程中，学习到的jquery的知识，将从以下三个方向总结；</p>

        <p>2.&nbsp;文章的总结学习将从2019.11.19日正式开始，每周至少写5篇内容；</p>

        <p>3.&nbsp;一年的时间，专注于javascript和jquery的学习，让自己精通js和jq；</p>

        <p>4.&nbsp;该目录收录的所有文章，参考借鉴整理他人优秀的知识，自己所写文章会标注参考文献</p>

        <p>5.&nbsp;一些重要的知识点，自己会实践开发，通过实例详细的介绍某个方法、知识点或jquery&nbsp;ui等知识的使用。</p>

        <p>6.&nbsp;所写的知识，作为一种笔记方式，成为自己的开发字典，使用时可以用来查找，开发过程中也会不断完善某个知识点，以及整个开发目录。</p>

        <h1>一、方法</h1>

        <p><a href="https://blog.csdn.net/zyz00000000/article/details/100210633">1.jquery的$.extend()</a>
        </p>

        <p>（1）extend({},item1,item2,&hellip;&hellip;)</p>

        <p>（2）<a
                href="https://www.cnblogs.com/luyuan/p/3940116.html">https://www.cnblogs.com/luyuan/p/3940116.html</a>
        </p>

        <p><a href="https://www.cnblogs.com/agansj/p/8780220.html">2.
                $(selector).each(function(index,element))和$.each(dataresource,function(index,element))</a>
        </p>

        <p>看源码一切就明白了：$.each()方法，callback回调函数，调用的是obj</p>

        <pre>
        <code class="language-javascript">		/**
                 * jQuery上拓展的静态方法，遍历对象或数据
                 * @param {*} obj 
                 * @param {*} callback 
                 */
                each: function(obj, callback) {
                    var length, i = 0;
        
                    if (isArrayLike(obj)) {
                        length = obj.length;
                        for (; i &lt; length; i++) {
                            if (callback.call(obj[i], i, obj[i]) === false) {
                                break;
                            }
                        }
                    } else {
                        for (i in obj) {
                            if (callback.call(obj[i], i, obj[i]) === false) {
                                break;
                            }
                        }
                    }
        
                    return obj;
                },</code></pre>

        <p>&nbsp;再来看看$(selector).each()</p>

        <pre>
        <code class="language-javascript">		/**
                 * 实例方法，this指向调用者本身
                 * @param {*} callback 
                 */
                each: function(callback) {
                    return jQuery.each(this, callback);
                },</code></pre>

        <p><a
                href="https://www.cnblogs.com/jiqing9006/archive/2012/07/03/2575141.html">3.jQuery中的join方法</a>
        </p>

        <p>4.resize() 方法</p>

        <p>5.animate() 方法</p>

        <p><a href="https://www.runoob.com/jquery/sel-gt.html">6.&nbsp;jQuery&nbsp;:gt()&nbsp;选择器</a>
        </p>

        <p><a href="https://www.runoob.com/jquery/sel-lt.html">7.&nbsp;jQuery&nbsp;:lt()&nbsp;选择器</a>
        </p>

        <p><a href="https://blog.csdn.net/zyz00000000/article/details/103418305">8.
                jquery的$.type()方法和javascript的toString
                方法以及Object.prototype.toString.call()</a></p>

        <p><a href="https://blog.csdn.net/zyz00000000/article/details/103459270">9.
                ES5的数组新方法forEach、filter、map、every、some；indexOf和lastIndexOf</a></p>

        <p><a href="https://blog.csdn.net/zyz00000000/article/details/103461109">10.
                数组方法：push、pop、unshift、shift、splice、sort；改变原数组的方法</a></p>

        <p><a href="https://blog.csdn.net/zyz00000000/article/details/103461899">11.
                数组方法：concat、slice、reverse；只复制出副本，不改变原数组；</a></p>

        <p><a
                href="https://blog.csdn.net/zyz00000000/article/details/103479478">12.&nbsp;数组转字符串，字符串转数组</a>
        </p>

        <p><a
                href="https://blog.csdn.net/zyz00000000/article/details/103557802">13.&nbsp;jquery给标签添加属性或获取属性值attr方法，移除属性removeAttr方法</a>、
        </p>

        <p><a
                href="https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify">14.
                JSON.stringify()方法的使用</a></p>

        <p><a href="https://blog.csdn.net/zyz00000000/article/details/103800984">15.
                jquery的trigger和triggerHandler方法</a></p>

        <p>16. jquery中的data的使用，data方法绑定的属性在哪里存着</p>

        <p>17. 事件绑定的类型，具体区别，live、on、$(document).on(&#39;&#39;)，绑定事件的类型，新插入的html也会被绑定事件。</p>

        <p>18. show和hide方法的权重，与行间样式相比，他们的权重如何？</p>

        <p>19. $()[0]，jquery类数组中的某一个是dom对象，不是jquery对象</p>

        <p>20. 背景图片的设置：backgrou-image，url()</p>

        <p>21. 杂项index方法</p>

        <p>22. closest方法，事件委托</p>

        <p>23. 属性特性的获取和赋值</p>

        <p>24.&nbsp;delegate</p>

        <p>（1）delegate() 方法为指定的元素（属于被选元素的子元素）添加一个或多个事件处理程序，并规定当这些事件发生时运行的函数。</p>

        <p>使用 delegate() 方法的事件处理程序适用于当前或未来的元素</p>

        <p>（2）on() 方法在被选元素及子元素上添加一个或多个事件处理程序。</p>

        <p>自 jQuery 版本 1.7 起，<strong>on() 方法是 bind()、live() 和 delegate() 方法的新的替代品</strong>。该方法给 API
            带来很多便利，我们推荐使用该方法，它简化了
            jQuery 代码库。</p>

        <p><strong>注意：</strong>使用 on() 方法添加的事件处理程序适用于当前及未来的元素（比如由脚本创建的新元素）。</p>

        <p><strong>提示：</strong>如需移除事件处理程序，请使用&nbsp;<a
                href="https://www.runoob.com/jquery/event-off.html">off()</a>&nbsp;方法。
        </p>

        <p><strong>提示：</strong>如需添加只运行一次的事件然后移除，请使用&nbsp;<a
                href="https://www.runoob.com/jquery/event-one.html">one()</a>&nbsp;方法。</p>

        <p>25.&nbsp;<strong>$.fn</strong>.tabs=function、$.fn.extend({})</p>

        <p>26.&nbsp;$(window).resize</p>

        <p>27.&nbsp;ajax请求后的回调函数的作用域</p>

        <p>28. jquery的静态方法和实例方法</p>

        <p><a href="https://www.jb51.net/article/85773.htm">https://www.jb51.net/article/85773.htm</a>
        </p>

        <h1>二、知识</h1>

        <p><a
                href="https://blog.csdn.net/zyz00000000/article/details/97131595">1.jQuery源码解读&mdash;&mdash;严格模式</a>
        </p>

        <h1>三、jquery-ui</h1>

        <p><a href="https://blog.csdn.net/tycoding/article/details/82904750">1.JQuery-zTree.js使用范例</a>
        </p>

        <p><a href="https://blog.csdn.net/wangchaoqi1985/article/details/80382350">2.jQuery UI dialog
                的使用</a></p>

        <p><a href="https://www.jianshu.com/p/51863ae82723">3. jquery chosen插件使用及select常用方法</a></p>

        <p><a href="https://blog.csdn.net/wangxiuyan0228/article/details/79419937">3.1 chosen插件的使用</a>
        </p>

        <p><a href="https://blog.csdn.net/c11073138/article/details/79462156">3.2chosen插件使用及优化</a></p>

        <p>（1）chosen设置宽度width：select标签css中设置的width值会作用到对应的chosen生成的div标签的元素上，所以：</p>

        <p>修改select标签的width值，即可修改chosen对应的div标签的width值</p>

        <p>比如：</p>

        <p><img alt="" class="has" height="269"
                src="https://img-blog.csdnimg.cn/20191012094553699.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3p5ejAwMDAwMDAw,size_16,color_FFFFFF,t_70"
                width="735" /></p>

        <p>（2）select内容居中、居左、居右等位置的变化由chosen生成的a标签的padding决定的</p>

        <p>可以通过在css样式设置a标签的padding值来修改select内容的位置</p>

        <p>比如：</p>

        <p><img alt="" class="has" height="239" src="https://img-blog.csdnimg.cn/20191012101143211.png"
                width="262" /></p>

        <p>（3）设置显示个数后的出现chosen滚动条</p>

        <p>&nbsp;与设置css样式一样，chosen生成的div，设置height和overflow-y:auto，超出部分滚动条展示</p>

        <p>比如：设置高度是100px，纵向超出部分出现滚动条</p>

        <p><img alt="" class="has" height="112" src="https://img-blog.csdnimg.cn/20191012134117447.png"
                width="425" /></p>

        <p>效果：</p>

        <p><img alt="" class="has" height="119" src="https://img-blog.csdnimg.cn/20191012134234832.png"
                width="209" /></p>

        <p><a href="https://www.jb51.net/article/121912.htm">4.详解EasyUi控件中的Datagrid</a></p>

        <p><a href="https://blog.csdn.net/cdnight/article/details/41351505">5.jQuery 自定义网页滚动条样式插件
                mCustomScrollbar&nbsp;</a>
        </p>

        <p><a href="https://www.runoob.com/jqueryui/api-sortable.html">6.&nbsp;可排序小部件（Sortable
                Widget）</a></p>

        <p>可排序（Sortable）插件让被选元素通过鼠标拖拽进行排序</p>
        
    </div>`,
        jquery_$data: `<p>&nbsp;</p>

        <p>注意：获取的dom对象</p>
        
        <pre>
        <code>        var elem = document.getElementsByClassName('test')
        
                typeof elem   ===  "object"
        
                Object.prototype.toString.call(elem) === "[object HTMLCollection]"</code></pre>
        
        <p>给标签元素的dom对象绑定值 或者 获取值</p>
        
        <p>将值进行缓存的时候，我们用到是随机数，是jquery在初始化的时候，产生的随机数。</p>
        
        <pre>
        <code class="language-javascript">"use strict";
        var version = "3.4.1";
        jQuery.extend({
          expando: "jQuery" + (version + Math.random()).replace(/\D/g, "")
        })
        function Data() {
          // expando 随机生成  比如：jQuery34102265498103020911
          this.expando = jQuery.expando + Data.uid++;
        }
        Data.uid = 1;
        Data.prototype = {
          cache: function (elem) {
            return  elem[this.expando] =  elem[this.expando] || {};
          },
          set: function (elem, key, value) {
            var cache = this.cache(elem);//对象是引用类型，操作cache 就是在操作elem[this.expando]对象
            if(key &amp;&amp; typeof key === "string" &amp;&amp; value){
              cache[key] = value;//实际上是添加或修改key属性
            }else if(key &amp;&amp; typeof key === "object" &amp;&amp; value === undefined){
              for(var prop in key){
                cache[prop] = key[prop];
              }
            }
            return cache;
          },
          get: function (elem, key) {
            var cache = this.cache(elem);
            if(key &amp;&amp; typeof key === "string"){
              return cache[key];//获取该属性值，若该属性不存在，结果为undefined
            }
          },
          
          access: function (elem, key, value) {
            if(key &amp;&amp; typeof key === "string" &amp;&amp; value === undefined){
              return this.get(elem, key);//取值的
            }else if(key &amp;&amp; value || Object.prototype.toString.call(key)  === "[object Object]" &amp;&amp; value === undefined){
              this.set(elem, key, value);//设置值的
              return value !== undefined ? value : key;
            }
          },
          remove: function (elem, key) {
            if(key &amp;&amp; typeof key === "string"){
              var cache = this.cache(elem);
              delete cache[key];
            }
          }
        }
        var dataPriv = new Data();//构建私有对象
        
        var dataUser = new Data();//构建公有对象
        
        jQuery.extend({
         
          //可以是一对值，也可以是一个对象内包括多个键值对
          data: function (elem, key, value) {
            return dataUser.access(elem, key, value);
          },
          removeData: function (elem, key) {
            dataUser.remove(elem, key);
          },
          //私有方法
          _data: function (elem, key, value) {
            return dataUser.access(elem, key, value);
          }, _removeData: function () {
            return dataPriv.access(elem, name, data);
          }
        })</code></pre>
        
        <p>&nbsp;</p>`,
        jquery_dataType: `<p>1. 隐式的转换为字符串</p>

        <pre>
        <code class="language-javascript">var variate = ["a","b","c"];
        //转换为字符串
        variate + ""</code></pre>
        
        <p>2. 隐式的转换为布尔类型</p>
        
        <pre>
        <code class="language-javascript">var variate = ["a","b","c"];
        //转换为Boolean类型
        !!variate</code></pre>
        
        <p>3. 查看字符串类型</p>
        
        <p>typeof 数组、对象、null&nbsp; === &ldquo;object&rdquo;</p>
        
        <p>typeof 函数 === &quot;functiong&quot;</p>
        
        <p>typeof 字符串 === &quot;string&quot;</p>
        
        <pre>
        <code class="language-javascript">typeof null
        "object"
        
        typeof {}
        "object"
        
        typeof []
        "object"
        
        typeof function(){}
        "function"
        
        typeof "aa"
        "string"
        
        typeof 11
        "number"
        
        typeof true
        "boolean</code></pre>
        
        <p>4.&nbsp;Object.prototype.toString.call（）方法</p>
        
        <pre>
        <code class="language-javascript">Object.prototype.toString.call(null)
        "[object Null]"
        
        Object.prototype.toString.call([])
        "[object Array]"
        
        Object.prototype.toString.call({})
        "[object Object]"
        
        Object.prototype.toString.call("")
        "[object String]"
        
        Object.prototype.toString.call(111)
        "[object Number]"
        
        Object.prototype.toString.call(false)
        "[object Boolean]"
        
        Object.prototype.toString.call(undefined)
        "[object Undefined]"
        
        Object.prototype.toString.call(NaN)
        "[object Number]"
        
        Object.prototype.toString.call(function(){})
        "[object Function]"</code></pre>
        
        <p>&nbsp;</p>`
        ,
        jquery_type: `<p>$.type()：</p>

        <p>使用许多技术来确定对象的确切返回值。&nbsp;[[Class]]&nbsp;的确定如下:</p>
        
        <ul>
          <li>jQuery.type( undefined ) === &quot;undefined&quot;</li>
          <li>jQuery.type() === &quot;undefined&quot;</li>
          <li>jQuery.type( window.notDefined ) === &quot;undefined&quot;</li>
          <li>jQuery.type( null ) === &quot;null&quot;</li>
          <li>jQuery.type( true ) === &quot;boolean&quot;</li>
          <li>jQuery.type( new Boolean() ) === &quot;boolean&quot;</li>
          <li>jQuery.type( 3 ) === &quot;number&quot;</li>
          <li>jQuery.type( new Number(3) ) === &quot;number&quot;</li>
          <li>jQuery.type( &quot;test&quot; ) === &quot;string&quot;</li>
          <li>jQuery.type( new String(&quot;test&quot;) ) === &quot;string&quot;</li>
          <li>jQuery.type( function(){} ) === &quot;function&quot;</li>
          <li>jQuery.type( [] ) === &quot;array&quot;</li>
          <li>jQuery.type( new Array() ) === &quot;array&quot;</li>
          <li>jQuery.type( new Date() ) === &quot;date&quot;</li>
          <li>jQuery.type( new Error() ) === &quot;error&quot; //&nbsp;<strong>as of jQuery 1.9</strong></li>
          <li>jQuery.type( Symbol() ) === &quot;symbol&quot; //&nbsp;<strong>as of jQuery 1.9</strong></li>
          <li>jQuery.type( Object(Symbol()) ) === &quot;symbol&quot; //&nbsp;<strong>as of jQuery 1.12</strong></li>
          <li>jQuery.type( /test/ ) === &quot;regexp&quot;</li>
        </ul>`
        ,
        jquery_trigger: `<a href="https://segmentfault.com/a/1190000021615755" target="_blank">
                        jquery的trigger()和triggerHandler()的使用和深入理解</a>`
        ,
    },
    treeData: [
        {
            parent: "基础知识",
            parentFlag: "basic",
            chirlden: [
                {
                    childTitle: "jquery基础知识",
                    childFlag: "jquery_basic"
                },
                {
                    childTitle: "$.data()原理",
                    childFlag: "jquery_$data"
                },
                {
                    childTitle: "$.type()原理",
                    childFlag: "jquery_type"
                },
                {
                    childTitle: "trigger和triggerHandler",
                    childFlag: "jquery_trigger"
                }
            ]
        },
        {
            parent: "知识2",
            parentFlag: "parent2",
            chirlden: [
                {
                    childTitle: "item1",
                    childFlag: "item1"
                },
                {
                    childTitle: "item2",
                    childFlag: "item2"
                },
                {
                    childTitle: "item3",
                    childFlag: "item3"
                }
            ]
        }
    ]
}
