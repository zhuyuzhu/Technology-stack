$.namespace('Zhu.gulpData');
Zhu.gulpData = {
    init: function () {
        console.log("Zhu.gulpData.init执行")
    },
    data: {
        gulp_install: `<p>一、安装</p>

        <p>1. 全局安装</p>
        
        <pre>
        <code class="language-javascript">npm install gulp --global
        
        或者
        
        npm install gulp -g</code></pre>
        
        <p>2. 本地安装（项目的开发依赖devDependencies安装）</p>
        
        <pre>
        <code class="language-javascript">npm install gulp --save-dev
        
        或
        
        npm install gulp -D</code></pre>
        
        <p>3. 查看安装是否成功</p>
        
        <pre>
        <code class="language-javascript">gulp --version
        
        或者
        
        gulp -v
        
        结果：
        
        CLI version: 2.2.0
        Local version: 4.0.2
        
        </code></pre>
        
        <p>&nbsp;</p>`,
        gulp_glob: '<h2>Glob使用详解</h2>',
        gulp_less:`<h2>gulp-less插件：less转css</h2>`,
        gulp_gulify: `<h2>gulp-uglify插件：压缩js</h2>`,
        gulp_babel: `<h3>一、安装</h3>

        <p>1、首先要安装gulp-babel、babel-core、babel-preset-es2015这三个插件</p>
        
        <p>下面这种安装方法，安装的不是最新的插件，往下读，会明白原因，以及如何安装最新的。</p>
        
        <pre>
        <code>npm install gulp-babel@7 --save-dev
        
        npm install babel-core --save-dev
        
        npm install babel-preset-es2015 --save-dev </code></pre>
        
        <p><strong>2、gulp-babel要指定版本号</strong></p>
        
        <p>npm上查看：<a href="https://www.npmjs.com/package/gulp-babel" target="_blank">gulp-babel的使用和版本号</a>，文档会介绍不同的版本依赖安装</p>
        
        <p>7.0的gulp-bable依赖6.0的babel-core；</p>
        
        <p>8.0的gulp-babel依赖7.0的@babel/core；</p>
        
        <p>因为npm上gulp-babel的最新版本为8.0.0，不指定版本号，默认会安装最新版的。</p>
        
        <p>&nbsp;</p>
        
        <p><strong>3、babel-core和@babel/core的区别</strong></p>
        
        <p>babel-core在7.0 之后，包名升级为 @babel/core，所以在我们没有指定版本号的时候下</p>
        
        <p>安装babel-core的实际上是安装的最后版本的babel-core，版本为6.26.3</p>
        
        <p>安装的 @babel/core实际上是最新版本的，版本号为7.9.6</p>
        
        <p>npm上查看：<a href="https://www.npmjs.com/package/@babel/core" target="_blank">最新版的@babel/core</a></p>
        
        <p>npm上查看：<a href="https://www.npmjs.com/package/babel-core" target="_blank">最新版的babel-core</a></p>
        
        <p>二、配置</p>
        
        <p>在项目根目录下，即与.gulpfile文件同一目录，创建.babelrc文件</p>
        
        <p>设置内容为：</p>
        
        <pre>
        <code>{
            "presets": ["es2015"]
        }</code></pre>
        
        <p>三、使用</p>
        
        <p>1、在gulpfile中，引入插件</p>
        
        <pre>
        <code>var babel =require("gulp-babel")</code></pre>
        
        <p>2、创建task任务</p>
        
        <p>.pipe(babel())就是转换es6为es5</p>
        
        <pre>
        <code>gulp.task('uglify', function(done){
            gulp.src('./src/js/**/*.js')
                .pipe(babel())
                .pipe(uglify())
                .pipe(gulp.dest('./dist/js'))
            done()
        })</code></pre>
        
        <p>&nbsp;</p>
        
        <p>&nbsp;</p>
        
        <p>&nbsp;</p>`,
    },
    treeData: [
        {
            parent: "基础知识",
            parentFlag: "basic",
            chirlden: [
                {
                    childTitle: "gulp安装",
                    childFlag: "gulp_install"
                },
                {
                    childTitle: "Glob详解",
                    childFlag: "gulp_glob"
                },
                {
                    childTitle: "item3",
                    childFlag: "item3"
                }
            ]
        },
        {
            parent: "gulp插件",
            parentFlag: "plugins",
            chirlden: [
                {
                    childTitle: "Less转CSS",
                    childFlag: "gulp_less"
                },
                {
                    childTitle: "压缩js",
                    childFlag: "gulp_gulify"
                },
                {
                    childTitle: "es6转es5",
                    childFlag: "gulp_babel"
                }
            ]
        }
    ]
}

