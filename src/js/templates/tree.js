/**
 * 构建左侧树结构
 */
;(function($){

    var Tree = function(target, options){
        this.childflag = "zhu",
        this.defaults = [
            {
                parent: "知识1",
                parentFlag: "parent1",
                children: [
                    {
                        childTitle: "item1",
                        childFlag: "item1"
                    },
                    {
                        childTitle: "item2",
                        childFlag: "item2"
                    },
                    {
                        childTitle: "item3",
                        childFlag: "item3"
                    }
                ]
            },
            {
                parent: "知识2",
                parentFlag: "parent2",
                children: [
                    {
                        childTitle: "item1",
                        childFlag: "item1"
                    },
                    {
                        childTitle: "item2",
                        childFlag: "item2"
                    },
                    {
                        childTitle: "item3",
                        childFlag: "item3"
                    }
                ]
            }
        ];
    }
    Tree.prototype = {
        init: function(target,options,contentData){
            var that = this;
            var treeData = $.extend(that.defaults, options);
            var treeHtml = that.creatTree(treeData);
            $(target).append(treeHtml);
            that.initDom();
            that.initEvent(treeData,contentData)
        },
        initDom: function(){
            var that = this;
            that.$parent = $('.item .parent');
            that.$child = $('.item .ul-wrap li');
        },
        initEvent: function(treeData, contentData){
            var that = this;
            that.$parent.on('click', function(event){
                var hasActiveClass = $(this).siblings('.ul-wrap').hasClass('active');
                if(!hasActiveClass){//展开
                    $('.item ul.active').slideUp("fast");//原本的ul合上
                    $('.item ul.active').removeClass('active');//并且移除它的active

                    $('.icon-arrow-down').addClass('hide');
                    $('.icon-arrow-up').removeClass('hide');

                    $(this).siblings('.ul-wrap').slideDown('fast');//当前的ul展开
                    $(this).siblings('.ul-wrap').addClass('active');//添加active

                    $(this).find('.icon-arrow-up').addClass('hide')
                    $(this).find('.icon-arrow-down').removeClass('hide');
                    
                }else {//合并
                    $(this).siblings('.ul-wrap').slideUp('fast');//当前的ul合上
                    $(this).siblings('.ul-wrap').removeClass('active');;//移除它的active

                    $(this).find('.icon-arrow-up').removeClass('hide')
                    $(this).find('.icon-arrow-down').addClass('hide');
                }
                event.stopPropagation();
            })
            that.$child.on('click',contentData, function(){
                
                var childflag = $(this).attr('childflag');
                if(childflag != that.childflag){
                    // $('#content_views').empty();
                    if(typeof contentData[childflag] == "string"){
                        $('#content_views').html(contentData[childflag]);//此处的childflag是变量，不能用.调用
                    }else if(typeof contentData[childflag] == "object"){
                        var articleUrl = "";
                        var myArticleUrl = "https://blog.csdn.net/zyz00000000/article/details/"
                        if(contentData[childflag].csdn_article_url){//内容前拼接一个文章地址
                            articleUrl = `<h3><a href="${contentData[childflag].csdn_article_url}" target="_blank">文章地址</a></h3>\n`
                        }else{
                            articleUrl = `<h3><a href="${myArticleUrl}+${contentData[childflag].article_id}" target="_blank">文章地址</a></h3>\n`
                        }
                        $('#content_views').html(articleUrl + contentData[childflag].content);
                    }
                   
                    that.childflag = childflag;
                    // location.href = location.pathname + "#" + childflag;
                    $('.item .ul-wrap li').removeClass('active');
                    $(this).addClass('active');
                }
                
            })
        },
        /**
         * 根据数据创建html标签
         * @param {*} data 
         */
        creatTree : function(data){
            var treeHtml = ''
            for(var i = 0; i < data.length; i ++){
                treeHtml += '<div class="item"><div class="parent"><span>'+data[i].parent+'</span><span class="iconfont icon-arrow-up">&#xe661;</span><span class="iconfont icon-arrow-down hide">&#xe613;</span></div><ul class="ul-wrap" style="display:none;">';
                for(var j = 0; j < data[i].children.length; j ++){
                    treeHtml +=  '<li id="'+data[i].children[j].childFlag+'" childFlag="'+data[i].children[j].childFlag+'">'+ data[i].children[j].childTitle + '</li>'
                }
                treeHtml += '</ul></div>'
            }
            return treeHtml;
        },

    }
    $.fn.tree = function(options, contentData){
        
        var target = this;
        if(Object.prototype.toString.call(options) == "[object Array]"){
            var theTree = new Tree(target, options, contentData);
            theTree.init(target,options,contentData);
        }
    }
})(jQuery);