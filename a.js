import {a, arr} from "./b.js";
console.log(a,arr);
if (!Function.prototype.bind) (function(){
    var slice = Array.prototype.slice;
    Function.prototype.bind = function() {
      var thatFunc = this, thatArg = arguments[0];
      var args = slice.call(arguments, 1);
      if (typeof thatFunc !== 'function') {
        // closest thing possible to the ECMAScript 5
        // internal IsCallable function
        throw new TypeError('Function.prototype.bind - ' +
               'what is trying to be bound is not callable');
      }
      return function(){
        var funcArgs = args.concat(slice.call(arguments))
        return thatFunc.apply(thatArg, funcArgs);
      };
    };
  })();

  //  Yes, it does work with `new (funcA.bind(thisArg, args))`
if (!Function.prototype.mybind) (function(){
    var ArrayPrototypeSlice = Array.prototype.slice;
    Function.prototype.mybind = function(otherThis) {
      if (typeof this !== 'function') {
        // closest thing possible to the ECMAScript 5
        // internal IsCallable function
        throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
      }
  
      var baseArgs= ArrayPrototypeSlice.call(arguments, 1),
          baseArgsLength = baseArgs.length,
          fToBind = this,
          fNOP    = function() {},
          fBound  = function() {
            //改变函数this后的函数
            //如果是fBound函数执行，this是函数执行时的上下文，比如window
            //如果是new fBound()，this是构造函数的实例对象
            baseArgs.length = baseArgsLength; // reset to default base arguments
            baseArgs.push.apply(baseArgs, arguments);
            return fToBind.apply( //执行fBound时执行原函数
                   fNOP.prototype.isPrototypeOf(this) ? this : otherThis, baseArgs //如果是实例对象，this
            );
          };
  
      if (this.prototype) {//此处的this是mybind的this，是个函数
        // Function.prototype doesn't have a prototype property
        fNOP.prototype = this.prototype; 
      }
      fBound.prototype = new fNOP();
  
      return fBound; //执行bind返回的函数
    };
  })();

var obj = {
  name: "obj",
  sayName: function(){
    console.log(this.name);
  }
}
var obj2 = {
  name: "obj2"
}
var func = obj.sayName.mybind(obj2);

func();

function Foo() {}
function Bar() {}
function Baz() {}

Bar.prototype = Object.create(Foo.prototype);// Bar.prototype ——> Foo.prototype ——> Object.prototype
Baz.prototype = Object.create(Bar.prototype);// Baz.prototype ——> Bar.prototype ——> Foo.prototype ——> Object.prototype

var foo = new Foo();
var bar = new Bar();
var baz = new Baz();
//是在该对象的原型链上
console.log(Baz.prototype.isPrototypeOf(foo)); // false
console.log(Bar.prototype.isPrototypeOf(foo)); // false
console.log(Foo.prototype.isPrototypeOf(foo)); // true
console.log(Object.prototype.isPrototypeOf(foo)); // true
